/*
 * manage total working days for current month
 */

$(document).ready(function (fn) {
    disableSave();
    $("table").find("td").addClass("col-sm-1");
    $("#days-count-error").removeClass("hidden").hide();
    $("input[id^=th-]").each(function (fn) {
        this.checked = true;
    });

    totalDays = 0;
    $("td .checkbox-inline").each(function (fn) {
        if ($(this).find("input").is(':checked')) {
            totalDays++;
        }
    });
    $("#working_days").html(totalDays);


    $("td .checkbox-inline").find("input").each(function () {
        this.checked = true;
    });

    $(document).click(function (fn) {
        totalDays = 0;
        $("td .checkbox-inline").each(function (fn) {
            if (!$(this).parent("td").find("label").hasClass("color-red")) {
                if ($(this).find("input").is(':checked')) {
                    totalDays++;
                }
            }
        });
        $("#working_days").html(totalDays);
    });

    $("td input[id^=checkboxes-]").click(function (fn) {
        id = fn.target.id;
//        console.log(id);
        if (!$(this).parent("td").find("label").hasClass("color-red")) {
            if ($(this).is(':checked')) {
                checkCheckBox(id);
            } else {
                uncheckCheckBox(id);
            }
        }

    });

});


/*
 * 
 * @param {string} id
 * @returns {void}
 * checks the checked bos in body table
 */
function checkCheckBox(id) {
    $("#" + id).closest("input").attr("value", "1");
    $("#" + id).closest("td").find(".note-text").remove();
    $("#" + id).closest("td").find("br").remove();
}

/*
 * 
 * @param {string} id
 * @returns {void}
 * unchecked checkbox in tbody
 */
function uncheckCheckBox(id) {
    $("#" + id).closest("input").attr("value", "");
    $("#" + id).closest("td").find(".note-text").remove();
    $("#" + id).closest("td").find("br").remove();
    if (!$("#" + id).closest("span").hasClass("hidden")) {
        $("#" + id).closest("td").append(getInputBox(id));
    }
}

/*
 * return input form for adding note in the holidays7
 */
function getInputBox(id) {
    formElem = "";
    formElem += "<br><label class=\"col-sm-12 note-text\">";
    //prepare name for the input
    note_text_name = "note-" + id;
    formElem += "<input id = \"textinput\" name=" + note_text_name + " placeholder=\"Note\" class=\"form-control input-sm\" type=\"text\"></label>";
    return formElem;
}

/*
 * disable save button
 */
function disableSave() {
    $("#save").addClass("disabled");
}

/*
 * enable save button
 */
function enableSave() {
    $("#save").removeClass("disabled");
}

/*
 * disable check boxes under the 
 */
function disableCheckBoxes(headTdId) {
    headArr = headTdId.split("-");
    //find out the tds under the thead which is checked or unchecked
    //go through each tds
    $(".td-" + headArr[1]).each(function (fn) {

        //uncheck check box
        $(this).find("label").find('span').removeClass('checked');
        $(this).find("label").find('span').find("input").attr('checked', false);
        $(this).find('input').checked = false;
        //hide check box
        $(this).find("label").find('span').addClass("hidden");
        //re paint label
        $(this).find("label").addClass("color-red");

//set values to the checkbox
        $(this).find("label").closest("input").attr("value", "No");
        $(this).closest("td").find(".note-text").remove();
        $(this).closest("td").find("br").remove();
    });
}

function enableCheckBoxes(headTdId) {
    headArr = headTdId.split("-");
    $(".td-" + headArr[1]).each(function (fn) {
        $(this).find("label").find('span').addClass('checked');
        $(this).find("label").find('span').removeClass("hidden");
        $(this).find("label").removeClass("color-red");
    });
}

$(document).ready(function (fn) {
    $("th .checkbox-inline").click(function (fn) {
        id = fn.target.id;
        actionCalculateWeekDays();
        if ($(this).find("input").is(':checked')) {
            $(this).removeClass("color-red");
            enableCheckBoxes(id);
        } else {
            $(this).addClass("color-red");
            disableCheckBoxes(id);
        }

    });
});

/*
 * calculate num of woksrking days
 */
function actionCalculateWeekDays() {
    days = 0;
    $("th .checkbox-inline").each(function (fn) {
        if ($(this).find("input").is(':checked')) {
            days++;
        }
    });
    if (days < 5 || days >= 7) {
        $("#days-count-error").show();
        $(".days-count").addClass("hidden");
        disableSave();
    }
    else {
        $("#days-count-error").hide();
        $("#count").html(days);
        $(".days-count").removeClass("hidden");
        enableSave();
    }
}