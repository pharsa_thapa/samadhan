/* 
 * cloning form elements
 */

$(document).ready(function (fn) {
    $("#remove-last-clone").addClass("disabled");
    /*
     * cloning form elements as a whole
     * button id 'clone-button' triggers event
     */
    $("#clone-button").click(function (f) {
        //display remove button
        $("#remove-last-clone").removeClass("disabled");
        //clone the initial form
        // id 'clone-0' must be the initial parent clone
        clone = $("#clone-0").clone(true, true);
        clones = $(".clone-content").length;

        //manipulate ids and names of the form elements
        inputs = clone.find(":input");

//        radioCount = $(document).find("input[type='radio']").length;

        //iterate over the total inputs field inside initial base component to be cloned
        $.each(inputs, function (index) {

            element = inputs[index];

            //capturing initial states of the form elements
            inputType = $(element).attr("type");
            initialName = $(element).attr("name");

            pieces = initialName.split("_");
            //find out the index of cloned component
            nextCount = parseInt($(".clone-content").length);
            //preparing index for the cloned component
            pieces.pop();
            pieces.push(nextCount);

            //prepare attributes for the cloned components
            finalName = pieces.join("_");
            finalId = finalName;

            $(clone).find(element).attr("name", finalName);

            //attaching id for the new element
            if (inputType === "text") {
                $(clone).find(element).attr("id", finalId);
                $(clone).find(element).val('');
            }
            //check if the input is radio
            /*
             * not applicable in this version
             * due to some technical diffirulties 
             */
            if (inputType === "radio") {
                $(clone).find(element).closest("label").attr("for", "radio-" + radioCount).find("div").attr("id", "uniform-radios-" + radioCount);
                radioId = $(clone).find(element).attr("id");

                idPieces = radioId.split("-");

                idPieces.pop();
                idPieces.push(radioCount);
                finalId = idPieces.join("-");
                $(clone).find(element).attr("id", finalId);
                radioCount++;
            }


        });

        // id for newly cloned component 
        nextCloneId = "clone-" + clones;
        $(clone).closest(".clone-content").attr("id", nextCloneId);
//        //count number of form clones
        //append hr for segmentation
        $("<hr>").appendTo("#clone-wrapper");
        //attach cloned form in the main wrapper 
        //wrapper id 'clone-wrapper'
        $(clone).appendTo("#clone-wrapper");
    });

    /*
     * removing lastly added clone
     */
    $("#remove-last-clone").click(function (fn) {
        //find out the total clones
        clones = $(".clone-content").length;
        //if clones (other than base component) are available 
        if (clones > 1) {
            //remove the last clone
            removeId = clones - 1;
            $("#clone-" + removeId).remove();
            $("#clone-wrapper").find("hr:last").remove();
        }
        //if clones not are available
        if (clones - 2 == 0) {
            //disable the remove button
            $("#remove-last-clone").addClass("disabled");
        }
    });

});
