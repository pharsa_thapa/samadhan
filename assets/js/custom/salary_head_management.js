
$(document).ready(function (fn) {
//add action attribute in the form
    $(document).find("form").attr("action", "http://localhost/metropayroll/salary_heads/procesSalaryHeadAllocation");

    //track event on check box under applicable table head
    $(".applicable_check").click(function (fn) {
        //find out the id of the clicked check box
        id = $(this).attr("id");
        splitId = id.split("_");
        recordId = splitId[splitId.length - 1];

        //check if the box is checked or not
        if ($(this).is(':checked')) {
            //highlight the table which contains the check box
            $(this).closest("tr").addClass("highlight");
            //show hidden text box for amount input
            $("#amount_div_" + recordId).removeClass("hidden");
        }
        else
        {
            //checkbox is unchecked
            //un highlight the table row
            $(this).closest("tr").removeClass("highlight");
            $('#uniform-percent_rule_' + recordId).find("span").removeClass('checked');
            $("#percent_entry_" + recordId).addClass("hidden");
            $("#amount_div_" + recordId).addClass("hidden");
        }
    });

    //event triggered in percent rule  table head check box
    $(".percent_rule_check").click(function (fn) {
    //find the attribute of the checkbox
        id = $(this).attr("id");
        splitId = id.split("_");
        recordId = splitId[splitId.length - 1];
        if ($(this).is(':checked')) {
            //highlight the containing table row
            $(this).closest("tr").addClass("highlight");
            //check applicable check box
            $('#uniform-applicable_' + recordId).find("span").addClass('checked');
            //show the percent rule form elements
            $("#percent_entry_" + recordId).removeClass("hidden");
            $("#amount_div_" + recordId).removeClass("hidden");
            //attr readonly in amount
            $("#salary_head_amount_" + recordId).attr("readonly", "readonly");
        }
        else
        {   // unchecked percent rule check box
            $("#percent_entry_" + recordId).addClass("hidden");
            $("#salary_head_amount_" + recordId).removeAttr("readonly");
            if ($("#applicable_" + recordId).is(':checked')) {
                $("#amount_div_" + recordId).removeClass("hidden");
            }
        }
    });

    //triggering event to calculate the total amount
    $(document).click(function (fn) {
        calculateAndDisplay();
    });

    $(".form-control").focusout(function (fn) {
        calculateAndDisplay();
    });
    $(".form-control").keyup(function (fn) {
        calculateAndDisplay();
    });

    $("select[id^='select_']").focusin(function () {
        populateSelect($(this));
    });

});

/*
 * calculates total amount of the salary inputs
 */

function calculateAndDisplay() {
    val = 0;
    $("input[id^='salary_head_amount_']").each(function () {
        thisElem = $(this);
        if ($(this).parent("div").hasClass("hidden") === false) {
            //check if the salary head is additon or deduction type
            if ($(this).parent("div").hasClass("add") === true) {
                //addition type salary head
                //if input is invalid or not a number set 0 for the salary head
                val = val + (isNaN(parseInt(thisElem.val())) ? 0 : parseInt(thisElem.val()));
            }

            if ($(this).parent("div").hasClass("minus") === true) {
                //dedution type
                val = val - (isNaN(parseInt(thisElem.val())) ? 0 : parseInt(thisElem.val()));
            }
        }
    });
//    populate total 
    $("#total_amount").html(val);
}

/*
 * populate drop down box on percent rule seletion
 */
function populateSelect(inputElement) {
    options = "";
    //recurse finding all the appplicable check boxes
    $("input[id^='applicable_']").each(function () {
        elem = $(this);
        attrId = elem.attr("id");
        attrArr = attrId.split("_");
        //find if the checkboxes are checked of not
        if (elem.is(':checked')) {
            //find head name of the looping (current) item
            headName = $("#head_name_" + attrArr[attrArr.length - 1]).html();
            //build options for the dropdown
            options += "<option" + " value='" + attrArr[attrArr.length - 1] + "'" + ">";
            options += headName;
            options += "</option>";
        }
    });
//populate options in the dropdown
    $(inputElement).html(options);
}

/*
 * disable amount input text box on checking percent rule
 */
function disableSelect(id) {
    $("select_" + id).attr("disabled", "disabled");
}

/*
 * end of the document
 */