$(document).ready(function (fn) {
    $(document).click(function (fn) {
        if (fn.target.id === 'previousMonth' || fn.target.id === 'thisMonth' || fn.target.id === 'nextMonth') {
            thisDate = $("#thisDate").val();
            callAjax(fn.target.id, thisDate);
        }
    });


});


function callAjax(eventId, currentDateVal) {
    var request = $.ajax({
        type: "post",
        url: "http://localhost/metropayroll/ajaxCalls/attendanceReport", //Please cahnge this url on deployment
        data: {"event": eventId, "inputDate": currentDateVal}
    });
    request.done(function (evt) {
        processAjaxResponse(evt);
    });
}

function processAjaxResponse(response) {
    data = $.parseJSON(response);
    $("#thisDate").val(data.outputDate);
    if (data.next == 1) {
        $("#nextMonth").removeClass("disabled");
    } else {
        $("#nextMonth").addClass("disabled");
    }

    if (data.prev == 1) {
        $("#previousMonth").removeClass("disabled");
    } else {
        $("#previousMonth").addClass("disabled");
    }

    records = $.parseJSON(data.records);
    html = '';

    $.each(records, function (index) {
        element = records[index];

        html += '<tr class="records">';
        html += "<td>";
        html += element.date;
        html += "</td><td>";
        html += "DAY EEROR";
        html += "</td><td>";
        html += element.check_in;
        html += "</td><td>";
        html += element.check_out;
        html += "</td><td>";
        html += element.status;
        html += "</td></tr>";
    }
    );

    $("tbody").empty().html(html);
}