$(document).ready(function (fn) {
    $("input[id^=checkboxes-]").click(function (fn) {
        if ($(this).is(':checked')) {
            action = "add";
        }
        else {
            action = 'remove';
        }

        id = this.id.split("-");
        group = $("#userGroupId").val();
        applyPermission(group, id[id.length - 1], action);
    });

});

function applyPermission(group, record, action) {
    var event = $.ajax({
        type: "post",
        url: "http://localhost/samadhan/ajaxCalls/applyPermission",
        data: {"action": action, "record": record, "group": group}

    });

    event.done(function (evt) {
        data = $.parseJSON(evt);
    });
}