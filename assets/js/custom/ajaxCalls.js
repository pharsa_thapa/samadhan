$(document).ready(function () {
//load designations in respective selected department
    $("#department").on("change", function () {
        var requestGVS = $.ajax({
            type: "post",
            url: "http://localhost/samadhan/ajaxCalls/changeDepartment", //Please cahnge this url on deployment
            data: {"department_id": $("#department").val()}

        });

        requestGVS.done(function (evt) {
            data = $.parseJSON(evt);
            if (data.result == 1) {
                $("#designation").html(data.response);
                $("#designation").prop("disabled", false);
            } else {
                $("#designation").html("<option value=''>Designations</option>");
                $("#submit").attr('disabled', 'disabled');
                $("#gvs").prop("disabled", true);
            }

        });

    });

    //load respective department name as per the selected designation
    $("#designation").on("change", function () {
        var requestDistrict = $.ajax({
            type: "post",
            url: "http://localhost/samadhan/ajaxCalls/changeDesignation",
            data: {"designation_id": $("#designation").val()}

        });

        requestDistrict.done(function (evt) {
            data = $.parseJSON(evt);
            if (data.result == 1) {

                $('#department option').prop('selected', false)
                        .filter('[value="' + data.response + '"]')
                        .prop('selected', true);

            }
        });

    });
//punch-in punch-out function

    $("#punch-in").click(function (fn) {

        fn.preventDefault();
        $("#punch-in").addClass("disabled");

        var punchIn = $.ajax({
            type: "post",
            url: "http://localhost/samadhan/ajaxCalls/punchIn",
            data: {}

        });

        punchIn.done(function (evt) {
            data = $.parseJSON(evt);
            if (data.status == 0) {
                $("#punch-in").removeClass("disabled");
            }
            $('#punch-message').html(data.message)
        });

    });
    $("#punch-out").click(function (fn) {
        fn.preventDefault();
        var punchIn = $.ajax({
            type: "post",
            url: "http://localhost/samadhan/ajaxCalls/punchOut",
            data: {}

        });

        punchIn.done(function (evt) {
            data = $.parseJSON(evt);
            $('#punch-message').html(data.message)
        });

    });

    $("input[id^=acl_list_]").click(function (fn) {
        if ($(this).is(':checked')) {
            status = 1;
        }
        else {
            status = 0;
        }

        id = this.id.split("_");
        listInMenu(status, id[id.length - 1]);
    });



    $("#scheme_id").change(function (fn) {
        var event = $.ajax({
            type: "post",
            url: "http://localhost/samadhan/ajaxCalls/getDiscount",
            data: {"id": $("#scheme_id").val()}
        });

        event.done(function (evt) {
            $("#discount_percentage").val(evt);
        });
    });


    $("#subscription_category_id").click(function (fn) {
        rate = $(this).val();
        $("#subscription_amount").html($("#option-" + rate).attr("data-subscription_rate"));
    });
    
    $("#subscription_category_id").change(function (fn) {
       nodeVal = $('select[name=subscription_category_id]').val();
       rate = $("#target-" + nodeVal).attr("data-rate-value");
       $("#subscription_amount").html(rate);
   });

});

function listInMenu(status, record) {
    var event = $.ajax({
        type: "post",
        url: "http://localhost/samadhan/ajaxCalls/listInMenu",
        data: {"status": status, "id": record}

    });

    event.done(function (evt) {
        data = $.parseJSON(evt);
        $('#punch-message').html(data.message)
    });
}