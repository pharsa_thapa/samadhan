<?php 
    
    
    //filter input
    function e($string){
       //filter input, escape output. never trust data.
        return htmlentities($string);  
    }

//
//    function display_children($parent, $level) {
//    $result = mysql_query("SELECT a.id,a.title,a.template,a.url, a.type,a.body, a.slug, Deriv1.Count FROM `menu` a LEFT  JOIN (SELECT parent_id, COUNT(*) AS Count FROM `menu` GROUP BY parent_id) Deriv1 ON a.id = Deriv1.parent_id WHERE a.parent_id=" . $parent);
//    echo "<ul>";
//    while ($row = mysql_fetch_assoc($result)) {
//         $link =  $row['type'] == '0' ?  site_url($row['url']) : site_url('home/'.$row['slug']) ;
//         echo $link;
//        if ($row['Count'] > 0) {
//            
//            echo "<li class='level".$level."'>"
//                       . anchor($link,$row['title'],array('class' => 'level')) . 
//                "</li>";
//            display_children($row['id'], $level + 1);
//            echo "</li>";
//        } elseif ($row['Count'] == 0) {
//          
//            echo "<li class='level".$level."'>"
//                    . anchor($link,$row['title'],array('class' => 'level')) . 
//                    "</a></li>";
//        } 
//            else;
//    }
//    echo "</ul>";
//    }
    
    
    function display_children($parent, $level) {
    $result = mysql_query("
        SELECT a.id,a.title,a.template,a.url, a.type,a.body, a.slug, Deriv1.Count FROM `menu` a 
        LEFT  JOIN (SELECT parent_id, COUNT(*) AS Count FROM `menu` GROUP BY parent_id)  
        Deriv1 ON a.id = Deriv1.parent_id WHERE a.parent_id=" . $parent ." order by `order` asc" );
//    echo "<ul>";
    while ($row = mysql_fetch_assoc($result)) {
         $link =  $row['type'] == '0' ?  site_url($row['url']) : site_url('home/'.$row['slug']) ;
         //echo $link;
        if ($row['Count'] > 0) {
            echo "<li class='dropdown'>";
            echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'. $row['title'] . '<b class="caret"></b></a>';
            echo "<ul class='dropdown-menu'>";
//            echo "<li class='level".$level."'>"
//                       . anchor($link,"#") . 
//                "</li>";
            display_children($row['id'], $level + 1);
//            echo "</li>";
            echo "</ul></li>";
        } elseif ($row['Count'] == 0) {
          
            echo "<li>"
                    . anchor($link,$row['title'],array('class' => 'level')) . 
                    "</li>";
        } 
            else;
    }
//    echo "</ul>";
    }
    
    
//    function display_children($parent, $level) {
//    $result = mysql_query("SELECT a.id,a.title,a.template,a.url, a.type,a.body, a.slug, Deriv1.Count FROM `menu` a LEFT  JOIN (SELECT parent_id, COUNT(*) AS Count FROM `menu` GROUP BY parent_id) Deriv1 ON a.id = Deriv1.parent_id WHERE a.parent_id=" . $parent);
//    
//    while ($row = mysql_fetch_assoc($result)) {
//        
//         $link =  $row['type'] == '0' ?  site_url($row['url']) : site_url('home/'.$row['slug']) ;
//       //  echo $link;
//         echo '<ul class="nav border-right-1 border-bottom-1 text-nav" >';
//        if ($row['Count'] > 0) {
//            
//            echo "<li class='dropdown'>
//              <a href='#' class='dropdown-toggle' data-toggle='dropdown'>".$row['title']."<b class='caret'></b></a>  
//            <ul class='dropdown-menu'><li>"
//            . anchor($link,$row['title']) . 
//            display_children($row['id'], $level + 1);
//           "</li></ul></li>";
//          
//        } elseif ($row['Count'] == 0) {
//            
//            echo "<li>"
//            . anchor($link,$row['title'],array('class' => 'level')) . 
//            "</li>";
//            
//        } 
//            else;
//            echo "</ul>";
//    }
//    
//    }
    
    function getMenu($parent, $level){
        $result = mysql_query("SELECT a.id,a.title,a.template,a.url, a.type,a.body, a.slug, Deriv1.Count FROM `menu` a LEFT  JOIN (SELECT parent_id, COUNT(*) AS Count FROM `menu` GROUP BY parent_id) Deriv1 ON a.id = Deriv1.parent_id WHERE a.parent_id=" . $parent);
        while ($row = mysql_fetch_assoc($result)) {
        
         $link =  $row['type'] == '0' ?  site_url($row['url']) : site_url('home/'.$row['slug']) ;
       //  echo $link;
         echo '<ul class="nav border-right-1 border-bottom-1 text-nav" >';
        if ($row['Count'] > 0) {
            
            echo "<li class='dropdown'>
              <a href='#' class='dropdown-toggle' data-toggle='dropdown'>".$row['title']."<b class='caret'></b></a>  
            <ul class='dropdown-menu'><li>"
            . anchor($link,$row['title']) . 
            display_children($row['id'], $level + 1);
           "</li></ul></li>";
          
        } elseif ($row['Count'] == 0) {
            
            echo "<li>"
            . anchor($link,$row['title'],array('class' => 'level')) . 
            "</li>";
            
        } 
            else;
            echo "</ul>";
    }
    }
    /**
     * Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.
     * @author Joost van Veen
     * @version 1.0
     */
    if (!function_exists('dump')) {
        function dump ($var, $label = 'Dump', $echo = TRUE)
        {
            // Store dump in variable 
            ob_start();
            var_dump($var);
            $output = ob_get_clean();

            // Add formatting
            $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
            $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';

            // Output
            if ($echo == TRUE) {
                echo $output;
            }
            else {
                return $output;
            }
        }
    }


    if (!function_exists('dump_exit')) {
        function dump_exit($var, $label = 'Dump', $echo = TRUE) {
            dump ($var, $label, $echo);
            exit;
        }
    }
    
    function array_for_select() {
    $args = & func_get_args();

    $return = array();

    switch (count($args)) {
        case 3:
            foreach ($args[0] as $itteration):
                if (is_object($itteration))
                    $itteration = (array) $itteration;
                $return[$itteration[$args[1]]] = $itteration[$args[2]];
            endforeach;
            break;

        case 2:
            foreach ($args[0] as $key => $itteration):
                if (is_object($itteration))
                    $itteration = (array) $itteration;
                $return[$key] = $itteration[$args[1]];
            endforeach;
            break;

        case 1:
            foreach ($args[0] as $itteration):
                $return[$itteration] = $itteration;
            endforeach;
            break;

        default:
            return FALSE;
    }

    return $return;
}

?>