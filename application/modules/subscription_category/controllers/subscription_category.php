<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subscription_category extends Frontend_Controller {

    public $validation_rules = array(
        'subscription_category_label' => array(
            'field' => 'subscription_category_label',
            'label' => 'Subscription Label',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'subscription_category_span' => array(
            'field' => 'subscription_category_span',
            'label' => 'Subscription Span',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]|numeric'
        ),
        'subscription_category_rate' => array(
            'field' => 'subscription_category_rate',
            'label' => 'Subscription Rate',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]|numeric'
        ),
//        status
    );

    public function __construct() {
        parent::__construct();

        $this->template->set_breadcrumb('Home', base_url());
        $this->template->set_breadcrumb("Subscription Types", site_url('subscription_category'));
    }

    public function index() {

        $this->data['records'] = $this->db->get("subscription_categories")->result();

        $this->template->set_breadcrumb("Subscription Type List", site_url('subscription_category'));

        $this->data['page_head'] = "All Subscription Type List";
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('list_view', $this->data);
    }

    public function add() {

        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validation_rules);
        if ($this->form_validation->run() == TRUE) {

            // preparing for insertion
            foreach ($this->validation_rules as $k => $v) {
                $insert_data [$k] = $this->input->post($k);
            }

            if ($this->db->insert("subscription_categories", $insert_data)) {
                $this->session->set_flashdata('success', 'New Subscription Type Added Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to add the Subscription Type.');
            }
            redirect('subscription_category/add/', 'refresh');
        } else {
            $subType = new stdClass();
            // Go through all the known fields and get the post values
            foreach ($this->validation_rules as $key => $field) {
                $subType->$field['field'] = set_value($field['field']);
            }
            $this->data["subscriptionType"] = $subType;
        }

        $this->template->set_breadcrumb("Add Subscription Type", site_url('subscription_category/add'));
        $this->data['page_head'] = "Add Subscription Type Details";
        $this->template->title('Title')
                ->set_layout('form_layout')
                ->build('form_view', $this->data);
    }

    public function edit($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validation_rules);

        if ($this->form_validation->run() == TRUE) {
            // preparing for insertion
            foreach ($this->validation_rules as $k => $v) {
                $update_data [$k] = $this->input->post($k);
            }
            if ($this->db->update("subscription_categories", $update_data, array("subscription_category_id" => $this->input->post("subscription_category_id")))) {
                $this->session->set_flashdata('success', 'Subscription Type Details updated Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to update the Subscription Type details.');
            }
            redirect('subscription_category/index', 'refresh');
        } else {
            if ($id == NULL)
                redirect(base_url("subscription_category"), "refresh");

            $this->data["subscriptionType"] = $this->db->get_where("subscription_categories", array("subscription_category_id" => $id))->row();

            if (empty($this->data["subscriptionType"]))
                redirect(base_url("subscription_category"), "refresh");

            $this->template->set_breadcrumb("Edit Subscription Type", site_url("subscription_category/edit/$id"));

            $this->data['page_head'] = "Edit Subscrtiption Type's Details -- {$this->data['subscriptionType']->subscription_category_label}";
            $this->template->title('Title')
                    ->set_layout('form_layout')
                    ->build('form_view', $this->data);
        }
    }

    /*
     * Delete record
     */

    public function delete($id = NULL) {
        if ($id == NULL)
            redirect(base_url("subscription_category"), "refresh");

        $this->data["subsType"] = $this->db->get_where("subscription_categories", array("subscription_category_id" => $id))->row();

        if (empty($this->data["subsType"]))
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to delete the Subscription Type details.');
        else {
            if ($this->db->delete("subscription_categories", array("subscription_category_id" => $id))) {
                $this->session->set_flashdata('success', 'Subscription Type Details deleted Successfully.');
            }
        }
        redirect(base_url("subscription_category"), "refresh");
    }


    /*
     * single status change
     */

    public function changestatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->uri->segment(4);

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

    /*
     * for bulk status change
     */

    public function bulkstatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->input->post('id');

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

}
