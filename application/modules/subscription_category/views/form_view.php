

<div class="form-group col-md-12">
    <label for="exampleInputEmail1">Subscription Label</label>
    <input type="text" class="form-control" placeholder="Subscription Label" name="subscription_category_label" id="subscription_category_label" value="<?php echo!empty($subscriptionType->subscription_category_label) ? $subscriptionType->subscription_category_label : "" ?>" required="required">
</div>
<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Subscription Span (in number of months)</label>
    <input type="number" class="form-control" placeholder="Number of Months" id="subscription_category_span" name="subscription_category_span" value="<?php echo!empty($subscriptionType->subscription_category_span) ? $subscriptionType->subscription_category_span: "" ?>" required="required">
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Subscription Rate</label>
    <input type="number" class="form-control" placeholder="Subscription Rate" name="subscription_category_rate" value="<?php echo!empty($subscriptionType->subscription_category_rate) ? $subscriptionType->subscription_category_rate : "" ?>" required="required">
</div>


<div class="form-group col-md-10 margin-bottom-40">
    &nbsp;
    <input type="hidden" class="form-control" name="subscription_category_id" id="subscription_category_id" value="<?php echo!empty($subscriptionType->subscription_category_id) ? $subscriptionType->subscription_category_id : "" ?>">
</div>
<div class="col-md-2 margin-bottom-40">
    <button type="submit" class="btn btn-success f-right margin-right-3" >Submit</button>

</div>


<script type="text/javascript">

    $(document).ready(function () {
        $("#date_from").datepicker({
            format: "yyyy-m-d",
            startDate: new Date(),
            orientation: "top auto",
            autoclose: true
        });
        $("#date_to").datepicker({
            format: "yyyy-m-d",
            startDate: "+1d",
            orientation: "top auto",
            autoclose: true
        });

        $('.summernote').summernote({
            height: 50
        });


        $("#form").validate({
            rules: {
                scheme_title: {
                    required: true,
//                    email: true
                },
                date_from: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                date_to: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                description: {
                    required: true,
                }
            }
        });


    });

</script>