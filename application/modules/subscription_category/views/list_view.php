



<div class="table-responsive">
    <table id="example" class="display table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Subscription Label</th>
                <th>Subscription Span</th>
                <th>Subscription Rate</th>
<!--                <th>Status</th>-->
                <th>Actions</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Subscription Label</th>
                <th>Subscription Span</th>
                <th>Subscription Rate</th>
<!--                <th>Status</th>-->
                <th>Actions</th>
            </tr>
        </tfoot>
        <tbody>

            <?php foreach ($records as $record) : ?>
                <tr>
                    <td><?php echo $record->subscription_category_label; ?></td>
                    <td><?php echo $record->subscription_category_span; ?> Months</td>
                    <td> NRS <?php echo $record->subscription_category_rate; ?></td>
                    <!--<td><?php // echo $record->scheme_status == 1 ? "Active" : "Expired"; ?></td>-->
                    <td> 
                       
                        <a href="<?php echo base_url("subscription_category/edit/$record->subscription_category_id"); ?>">
                            Edit
                        </a> 
                        | 
                        <a href="<?php echo base_url("subscription_category/delete/$record->subscription_category_id"); ?>" 
                           onclick="javascript : return confirm('Are you sure, you want to delete this record permanently?');">
                            Delete
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>  
</div>