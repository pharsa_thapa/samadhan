<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Collector's Name</label>
    <br>
    <span><?php echo!empty($collector->collector_name) ? $collector->collector_name : "" ?></span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Collector's Contact</label>
    <br>
    <span><?php echo!empty($collector->collector_phone) ? $collector->collector_phone : "" ?></span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Registered Date</label>
    <br>
    <span> <?php echo!empty($collector->registered_date) ? $collector->registered_date : "" ?> </span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Address</label>
    <br>
    <span>
        <?php echo $collector->gvs_name . " - " . $collector->ward_no . ", " . $collector->district_name; ?> 
    </span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Other Address</label>
    <br>
    <span><?php echo ($collector->address) ?></span>
</div>
