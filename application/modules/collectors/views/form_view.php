
<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Collector's Name</label>
        <input type="text" class="form-control" placeholder="Collector's Name" name="collector_name" id="collector_id" value="<?php echo!empty($collector->collector_name) ? $collector->collector_name : "" ?>" required="required">
    </div>
    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Registered Date</label>
        <input type="text" class="form-control datepicker" placeholder="Registered Date" id="registered_date" name="registered_date" value="<?php echo!empty($collector->registered_date) ? $collector->registered_date : "" ?>" required="required">
    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Phone</label>
        <input type="text" class="form-control" placeholder="Collector's Phone" id="collector_phone" name="collector_phone" value="<?php echo!empty($collector->collector_phone) ? $collector->collector_phone : "" ?>" required="required">
    </div> 

</div>
<div class="col-md-12">

    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">District</label>
        <select class="form-control" name="district" id="district">

            <option value="0" >None</option>
            <?php
            foreach ($districts as $district) {
                ?>
                <option value="<?php echo $district->district_id ?>" <?php echo ($collector->district == $district->district_id) ? "selected='selected'" : "" ?>><?php echo $district->district_name; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">GVS/Municipality</label>
        <select class="form-control" name="gvs" id="gvs" disabled="disabled">
            <option value=''>GVS/Municipality</option>
            <?php foreach ($gvss as $gvs) : ?>
                <option value='<?php echo $gvs->gvs_id ?>' <?php echo $gvs->gvs_id == $client->gvs_id ? "selected='selected'" : "" ?>>
                    <?php echo $gvs->gvs_name ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Ward No.</label>
        <select class="form-control" name="ward_no">
            <?php for ($counter = 1; $counter <= 30; $counter++) : ?>
                <option value="<?php echo $counter; ?> " <?php echo $counter == $collector->ward_no ? "selected='selected' " : '' ?>><?php echo $counter; ?></option>
            <?php endfor; ?>
        </select>

    </div>


    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Address</label>
        <input type="text" class="form-control " placeholder="Address" id="address" name="address" value="<?php echo!empty($collector->address) ? $collector->address : "" ?>" >

    </div>

    <div class="form-group col-md-10 margin-bottom-40">
        &nbsp;
        <input type="hidden" class="form-control" name="collector_id" id="collector_id" value="<?php echo!empty($collector->collector_id) ? $collector->collector_id : "" ?>">
    </div>
    <div class="col-md-2 margin-bottom-40">
        <button type="submit" class="btn btn-success f-right margin-right-3" id="submit" >Submit</button>

    </div>



</div>




<script type="text/javascript">

    $(document).ready(function () {
        $("#gvs").prop("disabled", true);

        $("#registered_date").datepicker({
            format: "yyyy-m-d",
            startDate: new Date(),
            orientation: "top auto",
            autoclose: true
        });
        $("#form").validate({
            rules: {
                collector_name: {
                    required: true,
//                    email: true
                },
                collector_phone: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                registered_date: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                district: {
                    required: true,
                },
                gvs: {
                    required: true,
                },
                ward_no: {
                    required: true,
                },
                address: {
                    required: true,
                },
            }
        });
        $("#district").on("change", function () {
            var requestGVS = $.ajax({
                type: "post",
                url: "<?php echo base_url("collectors/changeDistrict"); ?>", //Please cahnge this url on deployment
                data: {"district_id": $("#district").val()}

            });

            requestGVS.done(function (evt) {
                data = $.parseJSON(evt);
                if (data.result == 1) {
                    $("#gvs").html(data.response);
                    $("#gvs").prop("disabled", false);
                    $("#submit").prop('disabled', false);
                } else {
                    $("#gvs").html("<option value=''>GVS/Municipality</option>");
                    $("#submit").attr('disabled', 'disabled');
                    $("#gvs").prop("disabled", true);
                }

            });
        });
        
         $("#gvs").on("change", function () {
            var requestDistrict = $.ajax({
                type: "post",
                url: "<?php echo base_url("collectors/changeGvs"); ?>", //Please cahnge this url on deployment
                data: {"gvs_id": $("#gvs").val()}

            });

            requestDistrict.done(function (evt) {
                data = $.parseJSON(evt);
                console.log(data);
                if (data.result == 1) {

                    $('#district option').prop('selected', false)
                            .filter('[value="' + data.response + '"]')
                            .prop('selected', true);
                    $("#gvs").prop("disabled", false);
                    $("#submit").prop('disabled', false);
                }
            });

        });
        
    });

</script>