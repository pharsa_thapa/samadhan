<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Collectors extends Frontend_Controller {

    public $validation_rules = array(
        'collector_name' => array(
            'field' => 'collector_name',
            'label' => "Collector's Name",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'registered_date' => array(
            'field' => 'registered_date',
            'label' => 'Registered Date',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'collector_phone' => array(
            'field' => 'collector_phone',
            'label' => 'Collector\'s Name',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'district' => array(
            'field' => 'district',
            'label' => 'District',
            'rules' => 'trim|required'
        ),
        'gvs' => array(
            'field' => 'gvs',
            'label' => 'GVS/Municipality',
            'rules' => 'trim|required'
        ),
        'ward_no' => array(
            'field' => 'ward_no',
            'label' => 'Ward No',
            'rules' => 'trim|required'
        ),
        'address' => array(
            'field' => 'address',
            'label' => 'Address',
            'rules' => 'trim|required'
        ),
    );

    public function __construct() {
        parent::__construct();
        $this->template->set_breadcrumb('Home', base_url());
        $this->template->set_breadcrumb("Collectors", site_url('collectors'));
    }

    public function index() {
        $this->data['records'] = $this->db->join("districts", 'districts.district_id= collectors.district', "left")
                ->join("gvs", "gvs.gvs_id=collectors.gvs", "left")
                ->get("collectors")
                ->result();
        $this->template->set_breadcrumb("Collectors' List", site_url('collectors'));
        $this->data['page_head'] = "All Collector's List";

        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('list_view', $this->data);
    }

    public function add() {
        $this->data['districts'] = $this->db->get("districts")->result();
        $this->data['gvss'] = $this->db->get("gvs")->result();
        $this->data['page_head'] = "Add Collector's Details";

        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validation_rules);
        if ($this->form_validation->run() == TRUE) {

            // preparing for insertion
            foreach ($this->validation_rules as $k => $v) {
                $insert_data [$k] = $this->input->post($k);
            }
            if ($this->db->insert("collectors", $insert_data)) {
                $this->session->set_flashdata('success', 'New Collector\'s Details Added Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to add the Collector\'s details.');
            }
            redirect('collectors/add/', 'refresh');
        } else {
            $collector = new stdClass();
            // Go through all the known fields and get the post values
            foreach ($this->validation_rules as $key => $field) {
                $collector->$field['field'] = set_value($field['field']);
            }
            $this->data["collector"] = $collector;
        }

        $this->template->set_breadcrumb("Add Collector", site_url('collector/add'));
        $this->template->title('Title')
                ->set_layout('form_layout')
                ->build('form_view', $this->data);
    }

    public function edit($id = NULL) {
        $this->data['districts'] = $this->db->get("districts")->result();
        $this->data['gvss'] = $this->db->get("gvs")->result();

        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validation_rules);

        if ($this->form_validation->run() == TRUE) {
            // preparing for insertion
            foreach ($this->validation_rules as $k => $v) {
                $update_data [$k] = $this->input->post($k);
            }
            if ($this->db->update("collectors", $update_data, array("collector_id" => $this->input->post("collector_id")))) {
                $this->session->set_flashdata('success', 'Collector\'s Details updated Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to update the Collector\'s details.');
            }

            redirect('collectors/index', 'refresh');
        } else {
            if ($id == NULL)
                redirect(base_url("collectors"), "refresh");

            $this->data["collector"] = $this->db->get_where("collectors", array("collector_id" => $id))->row();

            if (empty($this->data["collector"]))
                redirect(base_url("collectors"), "refresh");

            $this->template->set_breadcrumb("Edit Collector", site_url("collectors/edit/$id"));
            $this->data['page_head'] = "Edit Collector's Details -- {$this->data["collector"]->collector_name}";

            $this->template->title('Title')
                    ->set_layout('form_layout')
                    ->build('form_view', $this->data);
        }
    }

    /*
     * Delete record
     */

    public function delete($id = NULL) {
        if ($id == NULL)
            redirect(base_url("collectors"), "refresh");

        $this->data["collector"] = $this->db->get_where("collectors", array("collector_id" => $id))->row();

        if (empty($this->data["collector"]))
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to delete the Collector\'s details.');
        else {
            if ($this->db->delete("collectors", array("collector_id" => $id))) {
                $this->session->set_flashdata('success', 'Collector\'s Details deleted Successfully.');
            }
        }
        redirect(base_url("collectors"), "refresh");
    }

    /*
      detail of the records
      Report
     */

    public function detail($id) {
        if ($id == NULL)
            redirect(base_url("collectors"), "refresh");

        $this->data['collector'] = $this->db->join("districts", 'districts.district_id= collectors.district')
                        ->join("gvs", "gvs.gvs_id=collectors.gvs")->get_where("collectors", array("collector_id" => $id))->row();



        if (empty($this->data['collector'])) {
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to retrieve the collector\'s Details.');
            redirect(base_url("collectors"), "redirect");
        }

        if (!empty($this->data['collector'])) {

            $this->template->set_breadcrumb("Collector Details", site_url("collectors/detail/$id"));
            $this->data['page_head'] = "Collector's Details -- {$this->data['collector']->collector_name}";
            $this->template->title('Title')
                    ->set_layout('detail_view_layout')
                    ->build('view', $this->data);
        }
    }

    /*
     * single status change
     */

    public function changestatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->uri->segment(4);

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

    /*
     * for bulk status change
     */

    public function bulkstatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->input->post('id');

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

    /**
     * retrieving gvs/municipality under a district
     */
    public function changeDistrict() {
        if (!$this->input->is_ajax_request())
            redirect(base_url(), 'refresh');

        $records = $this->db->where("district_id", $this->input->post("district_id"))->get("gvs")->result();

        if (empty($records)) {
            $response['result'] = 0;
            echo json_encode($response);
        } else {
            $response['result'] = 1;
            $html = '';
            foreach ($records as $gvs) {
                $html.= '<option value="' . $gvs->gvs_id . '">' . $gvs->gvs_name . '</option>';
            }
            $response['response'] = $html;

            echo json_encode($response);
        }
    }

    public function changeGvs() {
        if (!$this->input->is_ajax_request())
            redirect(base_url(), 'refresh');

        $records = $this->db->where("gvs_id", $this->input->post("gvs_id"))->get("gvs")->row();

        if (empty($records)) {
            $response['result'] = 0;
            echo json_encode($response);
        } else {
            $response['result'] = 1;

            $district = $this->db->get_where("districts", array("district_id" => $records->district_id))->row();

            if (empty($district)) {
                $response['result'] = 0;
            }

            $response['response'] = $district->district_id;

            echo json_encode($response);
        }
    }

}
