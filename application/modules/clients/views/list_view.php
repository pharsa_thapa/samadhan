<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">



                    <div class="table-responsive">
                        <table id="example" class="display table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Clients Full Name</th>
                                    <th>Phone Number</th>
<!--                                    <th>District</th>
                                    <th>GVS/Municipality</th>-->
                                    <th>Address</th>
                                    <!--<th>Other Address</th>-->
                                    <th>Distributor's Name</th>
                                    <th>Expires In</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Clients Full Name</th>
                                    <th>Phone Number</th>
<!--                                    <th>District</th>
                                    <th>GVS/Municipality</th>-->
                                    <th>Address</th>
                                   <!--<th>Other Address</th>-->
                                    <th>Distributor's Name</th>
                                    <th>Expires In</th>
                                    <th>Actions</th>

                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($records as $record) : ?>
                                    <tr>

                                        <td><?php echo $record->client_first_name . ' ' . $record->client_last_name ?> </td>
                                        <td><?php echo $record->phone ?></td>
                                        <td><?php
                                            echo $record->gvs_name . '-' . $record->ward_no . ', ';
                                            echo $record->district_name;
                                            ?></td>
                                        <!--<td><?php // echo $record->address    ?></td>-->
                                        <!--<td><?php // echo $record->registered_date     ?></td>-->
                                        <td><?php echo $record->distributor_name ?></td>
                                        <td <?php if ($record->expiry_days <= 15) { ?>style="color:red" <?php } ?>><?php echo $record->expiry_days > 0 ? $record->expiry_days . " days" : "Expired!" ?></td>
                                        <td>

                                            <a href="<?php echo base_url("clients/detail/$record->client_id"); ?>">
                                                Details
                                            </a> 
                                            |
                                            <a href="<?php echo base_url("clients/edit/$record->client_id"); ?>">
                                                Edit
                                            </a> 
                                            | 
                                            <a href="<?php echo base_url("clients/delete/$record->client_id"); ?>" 
                                               onclick="javascript : return confirm('Are you sure, you want to delete this record permanently?');">
                                                Delete
                                            </a>
                                            <?php if ($record->expiry_days > 0 || $record->expiry_days < 0) {
                                                ?>
                                                | 
                                                <a href="<?php echo base_url("clients/renewSubscription/reports/$record->client_id"); ?>">
                                                    Renew
                                                </a> 
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>  
                    </div>



                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->