<?PHP $date = date("Y-m-d"); ?>
<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <h4>Latest Subscription Date For This Subscription is <strong style="color: red;"><?php echo $latest_subscription->subscription_to ?></strong></h4>
        </div>

        <div class="form-group col-md-6">
            <label for="exampleInputEmail1">Subscription Effective From</label>
            <input type="text" class="form-control date-picker" placeholder="Subscription Effective From" id="subscription_from" name="subscription_from" value="<?php echo!empty($latest_subscription->subscription_to) ? $latest_subscription->subscription_to : (!empty($subscription->subscription_from) ? $subscription->subscription_from : ''); ?>" required="required">
        </div>

        <div class="form-group col-md-6">
            <label for="exampleInputEmail1">Subscription Category</label>
            <select class="form-control" name="subscription_category_id" id="subscription_category_id">
                <?php
                $rate = @$subsType[0]->subscription_category_rate;
                foreach ($subsType as $subs) {
                    ?>
                    <option value="<?php echo $subs->subscription_category_id ?>"
                    <?php echo (@$client->subscription_category_id == $subs->subscription_category_id ) ? "selected='selected'" : "" ?>
                            id="target-<?php echo $subs->subscription_category_id ?>"
                            data-rate-value="<?php echo $subs->subscription_category_rate; ?>"
                            >
                                <?php echo $subs->subscription_category_label ?>
                    </option>
                <?php } ?>
            </select>
            <br>
            <strong>Rate : NRS <span id="subscription_amount"><?php echo $rate; ?></span></strong>  

        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-12">
            <h3>Schemes (Optional) </h3>
            <hr>
        </div>

    </div>
    <div class="row">
        <div class="form-group col-md-6">
            <label for="exampleInputPassword1">Scheme Name</label>
            <select class="form-control" name="scheme_id" id="scheme_id">
                <option value="0" >None</option>

                <?php
                foreach ($schemes as $scheme) {
                    ?>
                    <option value="<?php echo $scheme->scheme_id ?>" <?php echo ($client->scheme_id == $scheme->scheme_id) ? "selected='selected'" : "" ?>><?php echo $scheme->scheme_name; ?></option>
                <?php } ?>
            </select>
        </div>


        <div class="form-group col-md-6">
            <label for="exampleInputEmail1">Scheme Claimed Date</label>
            <input type="text" class="form-control date-picker" placeholder="Scheme Claimed Date" id="claimed_date" name="scheme_claimed_date" value="<?php echo!empty($client->scheme_claimed_date) && ($client->scheme_claimed_date != "0000-00-00") ? $client->scheme_claimed_date : $date; ?>">
        </div>

    </div>
    <div class="row">
        <div class="form-group col-md-12">
            <h3>Bill Number And Receipt Number</h3>
            <hr>
        </div>

    </div>

    <div class="row">
        <div class="form-group  col-md-6">
            <label for="exampleInputName"> Enter Bill Number</label>
            <br>
            <input type="number" class="form-control" placeholder="Bill Number"  name="bill_number" value="<?php echo!empty($client->bill_number) ? $client->bill_number : "" ?>" required="required">
        </div>

        <div class="form-group  col-md-6">
            <label for="exampleInputName"> Enter Receipt Number </label>
            <br>
            <input type="number" class="form-control" placeholder="Receipt Number"  name="receipt_number" value="<?php echo!empty($client->receipt_number) ? $client->receipt_number : "" ?>">
        </div>

    </div>


</div>

<!--<div class="row">
    <div class="col-md-12">
        <div class="form-group col-md-12">
            <h3>VAT And Discounts</h3>
            <hr>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group  col-md-6">
            <label for="exampleInputName"> Enter discount amount applicable for the subscriber !</label>
            <br>
            <input type="number" class="form-control" placeholder="Discount Percentage" id="discount_percentage" name="discount" value="<?php echo!empty($client->discount) ? $client->discount : "" ?>">
        </div>

        <div class="form-group  col-md-6">
            <label for="exampleInputName"> Un-check, if VAT is NOT applicable for this subscription! (13%)</label>
            <br>
            <input type="checkbox" name="vat" class="form-control checkbox-inline" id="vat" checked="checked">
        </div>
    </div>
</div>-->



<div class="row">
    <div class="form-group col-md-10 margin-bottom-40">
        &nbsp;
        <input type="hidden" class="form-control" id="client_id" name="client_id" value="<?php echo!empty($latest_subscription->client_id) ? $latest_subscription->client_id : "" ?>" required="required" >
    </div>
    <div class="col-md-2 margin-bottom-40">
        <button type="submit" class="btn btn-success f-right margin-right-3" id="submit" >Submit</button>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {
        $(".date-picker").datepicker({
            format: "yyyy-m-d",
            orientation: "top auto",
            autoclose: true,
        });
    });
</script>
<script src="<?php echo base_url("assets/js/custom/ajaxCalls.js") ?>"></script>
