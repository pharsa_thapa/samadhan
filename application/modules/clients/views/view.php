<div class="col-md-12">
    <div class="col-md-7">
        <div class="form-group col-md-12">
            <h3>Personal Details</h3>
        </div>

        <div class="form-group col-md-6">
            <label for="exampleInputEmail1">Client's Name</label>
            <br>
            <span><?php echo $client->client_first_name . " " . $client->client_last_name ?></span>
        </div>
        <div class="form-group col-md-6">
            <label for="exampleInputEmail1">Contact</label>
            <br>
            <span><?php echo $client->phone ?></span>
        </div>
        <div class="form-group col-md-6">
            <label for="exampleInputEmail1">Registered Date</label>
            <br>
            <span> <?php echo!empty($client->registered_date) ? $client->registered_date : "" ?> </span>
        </div>
        <div class="form-group col-md-6">
            <label for="exampleInputPassword1">Address</label>
            <br>
            <span>
                <?php echo $client->gvs_name . " - " . $client->ward_no . ", " . $client->district_name; ?> 
            </span>
        </div>
        <div class="form-group col-md-6">
            <label for="exampleInputPassword1">Other Address</label>
            <br>
            <span><?php echo ($client->address) ?></span>
        </div>
        <div class="form-group col-md-12">
            <h3> Distributor And Collector</h3>
        </div>
        <div class="form-group col-md-6">
            <label for="exampleInputPassword1">Distributor</label>
            <br>
            <span><?php echo ($client->distributor_name) ?></span>
        </div>
        <div class="form-group col-md-6">
            <label for="exampleInputPassword1">Collector</label>
            <br>
            <span><?php echo ($client->collector_name) ?></span>
        </div>


        <div class="form-group col-md-12">
            <h3> Scheme Details</h3>
        </div>
        <div class="form-group col-md-6">

            <label for="exampleInputPassword1">Scheme Name</label>
            <br>
            <span><?php echo!empty($client->scheme_name) && ($client->scheme_name != "None") ? $client->scheme_name : "----------" ?></span>
        </div>
        <div class="form-group col-md-6">
            <label for="exampleInputPassword1">Scheme Claimed Date</label>
            <br>
            <span><?php echo!empty($client->scheme_claimed_date) && ($client->scheme_claimed_date != "0000-00-00") ? $client->scheme_claimed_date : "----------" ?></span>
        </div>
    </div>
    <div class="col-md-5">

        <div class="form-group col-md-12">
            <h3>Location Map</h3>

            <?php if (is_file(realpath('./assets/uploads/client_maps/' . $client->map_pic))) { ?>

                <div class="row">
                    <img src="<?php echo base_url("assets/uploads/client_maps/$client->map_pic"); ?>" 
                         class="col-md-12" 
                         alt="<?php echo $client->client_first_name . " " . $client->client_last_name ?> map" 
                         title="<?php echo $client->client_first_name . " " . $client->client_last_name ?> Location Map"
                         />
                </div>

            <?php } else { ?>
                <h5>Location Map Unavailable!</h5>
            <?php } ?>

        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="col-md-12">
        <div class="form-group col-md-12">
            <h3>Subscription Details</h3>
        </div>
        <div class="form-group col-md-12">
            <div class="table-responsive">
                <table id="example" class="display table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
<!--                            <th>SN</th>-->
                            <th>Subscribed From</th>
                            <th>Subscribed Upto</th>
                            <th>Subscription Status</th>
                            <th>Bill Number</th>
                            <th>Receipt Number</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <!--<th>SN</th>-->
                            <th>Subscribed From</th>
                            <th>Subscribed Upto</th>
                            <th>Subscription Status</th>
                            <th>Bill Number</th>
                            <th>Receipt Number</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    <span class="hidden client" id="<?php echo $client->client_id ?>">  </span>
                    <?php
                    $counter = 1;
                    foreach ($subscriptions as $record) :
                        ?>
                        <tr>

                                                                                                                                                                                                                                                                        <!--<td><?php // echo $counter                                                            ?> </td>-->
                            <td><?php echo $record->subscription_from ?></td>
                            <td><?php echo $record->subscription_to; ?></td>
                            <td><?php echo ($record->subscription_status == 1) ? "Active" : "Inactive" ?></td>
                            <td><?php echo $record->bill_number; ?></td>
                            <td class="receipt-id-<?php echo $record->subscription_id; ?>" id="latest-receipt-id"><?php echo!empty($record->receipt_number) ? $record->receipt_number : "N/A"; ?></td>
                            <td>
                                <?php if (empty($record->receipt_number)) { ?>
                                    <span>
                                        <button type="button" class="btn btn-info receipt-button receipt-button-<?php echo $record->subscription_id; ?>" data-toggle="modal" data-target=".receipt-add-form" data-custom="<?php echo $record->subscription_id ?>"> Receipt </button>                       
                                    </span>
                                <?php } ?>
                                <span>
                                    <?php echo ($record->subscription_status == 1) ? "" : "Subscription Expired" ?>
                                    <?php if ($counter == 1) { ?>
                                        <a href="<?php echo base_url("clients/renewSubscription/clients/$record->client_id"); ?>" class="btn btn-success">
                                            Renew
                                        </a>

                                    <?php } ?>                                
                                </span>

                            </td>
                        </tr>
                        <?php
                        $counter++;
                    endforeach;
                    ?>

                    </tbody>
                </table>  
            </div>

        </div>
    </div>
</div>

<div style="display: none;" class="modal fade receipt-add-form" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Receipt Form</h4>
            </div>
            <div class="modal-body" style="height: 130px !important; ">
                <div class="col-md-12">
                    <div class="form-group  col-md-12">
                        <label for="exampleInputName"> Enter Receipt Number </label>

                    </div>
                    <div class="form-group  col-md-12">

                        <input type="number" class="form-control receipt-number" placeholder="Receipt Number"  name="receipt_number" value="<?php echo!empty($client->receipt_number) ? $client->receipt_number : "" ?>">
                    </div>

                    <div class="form-group col-md-12 errorBox hidden text-danger">Please enter a valid receipt number !</div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <div class="form-group  col-md-12">
                        <button type="button" class="btn btn-success" id="save-receipt" data-target=''>Save Receipt</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function (fn) {
        $(".receipt-button").click(function (fnc) {
            clone = $(this).attr("data-custom");
            $("#save-receipt").attr("data-target", clone);
        });

        $("#save-receipt").click(function (ff) {
            receiptNum = $(".receipt-number").val();
            if (receiptNum === "") {
                $(".errorBox").removeClass("hidden");
            }
            else {
                action = confirm("Are you sure ? \n \n" + "You entered : " + receiptNum + "\n\n");
                if (action) {
                    item = $("#save-receipt").attr("data-target");
                    client = $(".client").attr("id");

                    var receiptNumUpdate = $.ajax({
                        type: "post",
                        url: "<?php echo base_url("payments/subscriptionTransaction"); ?>", //Please cahnge this url on deployment
                        data: {"subscription": item, "client": client, "receipt": receiptNum}

                    });

                    receiptNumUpdate.done(function (evt) {
                        data = $.parseJSON(evt);
                        if (data.result == 1) {
                            item = $("#save-receipt").attr("data-target");
                            $(".receipt-id-" + item).html("").html(receiptNum);
                            $(".receipt-button").remove();
                            $("#button-close").trigger("click");
                            $(".close").trigger("click");
                        } else {
                            alert("Sorry!\n\n" + "We are unable to add receipt number! \n\n")
                        }

                    });
                }
            }
        });
    });

</script>