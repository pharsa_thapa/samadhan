
<div class="col-md-12">
    <div class="form-group col-md-12">
        <h3>Client's Personal Details</h3>
        <hr>
    </div>

</div>

<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Clients's First Name/ Organization Name</label>
        <input type="text" class="form-control" placeholder="Clients's First Name/ Organization Name" name="client_first_name" id="client_first_name" value="<?php echo!empty($client->client_first_name) ? $client->client_first_name : "" ?>" required="required">
    </div>
    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Client's Last Name</label>
        <input type="text" class="form-control" placeholder="Client's Last Name" id="client_last_name" name="client_last_name" value="<?php echo!empty($client->client_last_name) ? $client->client_last_name : "" ?>" >
    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Registered Date</label>
        <input type="text" class="form-control date-picker" placeholder="Registered Date" id="registered_date" name="registered_date" value="<?php echo!empty($client->registered_date) ? $client->registered_date : ""; ?>"  required="required">
    </div>
    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Client's Phone</label>
        <input type="text" class="form-control" placeholder="Phone Number" id="phone" name="phone" value="<?php echo!empty($client->phone) ? $client->phone : "" ?>">

    </div>
</div>


<div class="col-md-12">

    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">District</label>
        <select class="form-control" name="district_id" id="district">
            <option value="0" >None</option>
            <?php foreach ($districts as $district) {
                ?>
                <option value="<?php echo $district->district_id ?>" <?php echo ($client->district_id == $district->district_id) ? "selected='selected'" : "" ?>><?php echo $district->district_name; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">GVS/Municipality</label>
        <select class="form-control" name="gvs_id" id="gvs" <?php echo empty($client->gvs_id) ? 'disabled="disabled"' : '' ?>>

            <option value=''>GVS/Municipality</option>
            <?php if (!empty($client->gvs_id)) : foreach ($gvss as $gvs) : ?>
                    <option value='<?php echo $gvs->gvs_id ?>' <?php echo $gvs->gvs_id == $client->gvs_id ? "selected='selected'" : "" ?>>
                        <?php echo $gvs->gvs_name ?>
                    </option>
                    <?php
                endforeach;
            endif;
            ?>
        </select>
    </div>
</div>




<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Ward No.</label>
        <select class="form-control" name="ward_no">
            <?php for ($counter = 1; $counter <= 30; $counter++) : ?>
                <option value="<?php echo $counter; ?> " <?php echo $counter == @$client->ward_no ? "selected='selected' " : '' ?>><?php echo $counter; ?></option>
            <?php endfor; ?>
        </select>

    </div>
    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Address</label>
        <input type="text" class="form-control" placeholder="Address" id="address" name="address" value="<?php echo!empty($client->address) ? $client->address : "" ?>" >
    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Map Picture</label>
        <input type='file' name="map_pic" accept="image/*">
    </div>

</div>

<div class="col-md-12">
    <div class="form-group col-md-12">
        <h3>Subscription Details </h3>
        <hr>
    </div>

</div>
<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Distributor's Name</label>
        <select class="form-control" name="distributor_id" id="distributor_id">
            <?php
            foreach ($distributors as $distributor) {
                ?>
                <option value="<?php echo $distributor->distributor_id ?>" <?php echo (@$client->distributor_id == $distributor->distributor_id ) ? "selected='selected'" : "" ?>><?php echo $distributor->distributor_name; ?></option>
            <?php } ?>
        </select>
    </div>


    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Collector's Name</label>
        <select class="form-control" name="collector_id" id="collector_id">
            <?php
            foreach ($collectors as $collector) {
                ?>
                <option value="<?php echo $collector->collector_id ?>" <?php echo (@$client->collector_id == $collector->collector_id ) ? "selected='selected'" : "" ?>><?php echo $collector->collector_name; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Subscription Status  </label>
        <select class="form-control" name="subscription_status">

            <option value="1" <?php echo empty($client->subscription_status) ? "selected='selected'" : ($client->subscription_status == 1 ? "selected='selected' " : '') ?>>Active</option>
            <option value="0" <?php echo!empty($client->subscription_status) && $client->subscription_status == 0 ? "selected='selected' " : '' ?>>Inactive</option>

        </select>

    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Subscription Effective From</label>
        <input type="text" class="form-control date-picker" placeholder="Subscription Effective From" id="subscription_from" name="subscription_from" value="<?php echo!empty($client->subscription_from) ? $client->subscription_from : "" ?>" required="required">
    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Subscription Category</label>
        <select class="form-control" name="subscription_category_id" id="subscription_category_id">
            <?php
            $rate = @$subsType[0]->subscription_category_rate;
            foreach ($subsType as $subs) {
                ?>
                <option value="<?php echo $subs->subscription_category_id ?>" id="option-<?php echo $subs->subscription_category_id ?>" <?php echo (@$client->subscription_category_id == $subs->subscription_category_id ) ? "selected='selected'" : "" ?> data-subscription_rate="<?php echo $subs->subscription_category_rate ?>"><?php echo $subs->subscription_category_label ?></option>
            <?php } ?>
        </select>
        <br>
        <strong>Rate : NRS <span id="subscription_amount"><?php echo $rate; ?></span></strong>  

    </div>

</div>

<div class="col-md-12">
    <div class="form-group col-md-12">
        <h3>For Client's Via Schemes (Optional) </h3>
        <hr>
    </div>

</div>
<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Scheme Name</label>
        <select class="form-control" name="scheme_id" id="scheme_id">
            <option value="0" >None</option>

            <?php
            foreach ($schemes as $scheme) {
                ?>
                <option value="<?php echo $scheme->scheme_id ?>" <?php echo ($client->scheme_id == $scheme->scheme_id) ? "selected='selected'" : "" ?>><?php echo $scheme->scheme_name; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Scheme Claimed Date</label>
        <input type="text" class="form-control date-picker" placeholder="Scheme Claimed Date" id="claimed_date" name="scheme_claimed_date" value="<?php echo!empty($client->scheme_claimed_date) ? $client->scheme_claimed_date : "" ?>">
    </div>

</div>

<div class="col-md-12">
    <div class="form-group col-md-12">
        <h3> Bill And Receipt </h3>
        <hr>
    </div>

</div>

<div class="col-md-12">
    <div class="form-group  col-md-6">
        <label for="exampleInputName"> Enter Bill Number</label>
        <br>
        <input type="number" class="form-control" placeholder="Bill Number"  name="bill_number" value="<?php echo!empty($client->bill_number) ? $client->bill_number : "" ?>">
    </div>

    <div class="form-group  col-md-6">
        <label for="exampleInputName"> Enter Receipt Number </label>
        <br>
        <input type="number" class="form-control" placeholder="Receipt Number"  name="receipt_number" value="<?php echo!empty($client->receipt_number) ? $client->receipt_number : "" ?>">
    </div>

</div>


<!--<div class="col-md-12">
    <div class="form-group col-md-12">
        <h3> Discounts/VAT</h3>
        <hr>
    </div>

</div>-->
<!--<div class="col-md-12">

    <div class="form-group  col-md-6">
        <label for="exampleInputName"> Enter discount amount applicable for the subscriber ! (IN %)</label>
        <br>
        <input type="number" class="form-control" placeholder="Discount Percentage" id="discount_percentage" name="discount" value="<?php echo!empty($client->discount) ? $client->discount : "" ?>">
    </div>

    <div class="form-group  col-md-6">
        <label for="exampleInputName"> Un-check, if VAT is NOT applicable for this subscriber! (13%)</label>
        <br>
        <input type="checkbox" name="vat" class="form-control checkbox-inline" id="vat" checked="checked">
    </div>
</div>-->



<div class="col-md-12">
    <div class="form-group col-md-10 margin-bottom-40">
        &nbsp;
    </div>
    <div class="col-md-2 margin-bottom-40">
        <button type="submit" class="btn btn-success f-right margin-right-3" id="submit" >Submit</button>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {

//        $("#submit").prop("disabled", true);

        $(".date-picker").datepicker({
            format: "yyyy-m-d",
            orientation: "top auto",
            autoclose: true
        });
        $("#form").validate({
//            rules: {
//                distributor_name: {
//                    required: true,
////                    email: true
//                },
//                distributor_phone: {
//                    required: true,
////                    equalTo: '#exampleInputPassword1'
//                },
//                registered_date: {
//                    required: true,
////                    equalTo: '#exampleInputPassword1'
//                },
//                district: {
//                    required: true,
//                },
//                gvs: {
//                    required: true,
//                },
//                ward_no: {
//                    required: true,
//                },
//                address: {
//                    required: true,
//                },
//            }
        });
        $("#district").on("change", function () {
            var requestGVS = $.ajax({
                type: "post",
                url: "<?php echo base_url("distributors/changeDistrict"); ?>", //Please cahnge this url on deployment
                data: {"district_id": $("#district").val()}

            });

            requestGVS.done(function (evt) {
                data = $.parseJSON(evt);
                if (data.result == 1) {
                    $("#gvs").html(data.response);
                    $("#gvs").prop("disabled", false);
                    $("#submit").prop('disabled', false);
                } else {
                    $("#gvs").html("<option value=''>GVS/Municipality</option>");
                    $("#submit").attr('disabled', 'disabled');
                    $("#gvs").prop("disabled", true);
                }

            });

        });


        $("#gvs").on("change", function () {
            var requestDistrict = $.ajax({
                type: "post",
                url: "<?php echo base_url("distributors/changeGvs"); ?>", //Please cahnge this url on deployment
                data: {"gvs_id": $("#gvs").val()}

            });

            requestDistrict.done(function (evt) {
                data = $.parseJSON(evt);
                if (data.result == 1) {

                    $('#district option').prop('selected', false)
                            .filter('[value="' + data.response + '"]')
                            .prop('selected', true);
                    $("#gvs").prop("disabled", false);
                    $("#submit").prop('disabled', false);
                }
            });

        });



    });

</script>
<script type="text/javascript" src="<?php echo base_url("assets/js/custom/ajaxCalls.js"); ?>"></script>