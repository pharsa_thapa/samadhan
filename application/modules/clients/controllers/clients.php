<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clients extends Frontend_Controller {

    protected $u_config = array();
    protected $upload_path = './assets/uploads/client_maps/';
    public $add_validation_rules = array(
        'client_first_name' => array(
            'field' => 'client_first_name',
            'label' => "Client's First Name/ Organization's Name ",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'client_last_name' => array(
            'field' => 'client_last_name',
            'label' => "Client's Last Name",
            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]'
        ),
        'registered_date' => array(
            'field' => 'registered_date',
            'label' => "Registered Date",
            'rules' => 'trim|htmlspecialchars|required'
        ),
        "phone" => array(
            "field" => "phone",
            "label" => "Client's Phone",
            "rules" => "trim|htmlspecialchars|xss_clean|max_length[20]"
        ),
        'district_id' => array(
            'field' => 'district_id',
            'label' => "District's Name",
            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]|required'
        ),
        'gvs_id' => array(
            'field' => 'gvs_id',
            'label' => "GVS/Municipality's Name",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]|required'
        ),
        'ward_no' => array(
            'field' => 'ward_no',
            'label' => "Ward No",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]|required'
        ),
        'address' => array(
            'field' => 'address',
            'label' => "Address",
            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]|required'
        ),
        'distributor_id' => array(
            'field' => 'distributor_id',
            'label' => "Distributor's Name",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'collector_id' => array(
            'field' => 'collector_id',
            'label' => "Collector's Name",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'scheme_id' => array(
            'field' => 'scheme_id',
            'label' => "Scheme's Name",
            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]'
        ),
        'scheme_claimed_date' => array(
            'field' => 'scheme_claimed_date',
            'label' => "Scheme Claimed Date",
            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]'
        ),
        'map_pic' => array(
            'field' => 'map_pic',
            'label' => 'Map Picture',
            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]'
        ),
        'subscription_from' => array(
            'field' => 'subscription_from',
            'label' => 'Subscription Effective From',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'subscription_category_id' => array(
            'field' => 'subscription_category_id',
            'label' => 'Subscription Category',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'subscription_status' => array(
            'field' => 'subscription_status',
            'label' => 'Subscription Status',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'bill_number' => array(
            'field' => 'bill_number',
            'label' => 'Bill Number',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]|numeric'
        ),
        'receipt_number' => array(
            'field' => 'receipt_number',
            'label' => 'Receipt Number',
            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]|numeric'
        ),
//        'discount' => array(
//            'field' => 'discount',
//            'label' => 'Discount',
//            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]|numeric'
//        ),
    );
    public $edit_validation_rules = array(
        'client_first_name' => array(
            'field' => 'client_first_name',
            'label' => "Client's First Name/ Organization's Name ",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'client_last_name' => array(
            'field' => 'client_last_name',
            'label' => "Client's Last Name",
            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]'
        ),
        "registered_date" => array(
            "field" => "registered_date",
            "label" => "Registered Date",
            "rules" => "trim|htmlspecialchars|required|xss_clean|max_length[20]"
        ),
        "phone" => array(
            "field" => "phone",
            "label" => "Client's Phone",
            "rules" => "trim|htmlspecialchars|xss_clean|max_length[20]"
        ),
        'district_id' => array(
            'field' => 'district_id',
            'label' => "District's Name",
            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]|required'
        ),
        'gvs_id' => array(
            'field' => 'gvs_id',
            'label' => "GVS/Municipality's Name",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]|required'
        ),
        'ward_no' => array(
            'field' => 'ward_no',
            'label' => "Ward No",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]|required'
        ),
        'address' => array(
            'field' => 'address',
            'label' => "Address",
            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]|required'
        ),
//        'scheme_id' => array(
//            'field' => 'scheme_id',
//            'label' => "Scheme's Name",
//            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]'
//        ),
//        'scheme_claimed_date' => array(
//            'field' => 'scheme_claimed_date',
//            'label' => "Scheme Claimed Date",
//            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]'
//        ),
        'map_pic' => array(
            'field' => 'map_pic',
            'label' => 'Map Picture',
            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]'
        ),
    );

    public function __construct() {
        parent::__construct();

        $this->template->set_breadcrumb('Home', base_url());
        $this->template->set_breadcrumb("Subscribers", site_url("clients"));

        $this->u_config['upload_path'] = $this->upload_path;
        $this->u_config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
        $this->u_config['max_size'] = '5120'; //5Mb
        $this->u_config['encrypt_name'] = TRUE;
    }

    public function index() {
        $this->load->model("clients_model");
        $this->data['records'] = $this->clients_model->allClients();
        $this->data["page_head"] = "All Subscribers' List";

        $this->template->set_breadcrumb("Subscribers' List ", site_url("clients"));
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('list_view', $this->data);
    }

    public function add() {
        $this->data['districts'] = $this->db->get("districts")->result();
        $this->data['gvss'] = $this->db->get("gvs")->result();
        $this->data['distributors'] = $this->db->get("distributors")->result();
        $this->data['collectors'] = $this->db->get("collectors")->result();

        $this->data['schemes'] = $this->db->get_where("schemes", array("scheme_status" => '1'))->result();
        $this->data['subsType'] = $this->db->get_where("subscription_categories", array("subscription_category_status" => 1))->result();

        $this->data["page_head"] = "Add New Client's Detail";


        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->add_validation_rules);
        if ($this->form_validation->run() == TRUE) {
            $img_name = '';

            if ($_FILES["map_pic"]["size"] > 0) {
                $this->load->library('upload', $this->u_config);

                if (!$this->upload->do_upload('map_pic')) {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect('clients/add', 'refresh');
                } else {
                    $img = $this->upload->data();
                    $img_name = $img['file_name'];
                }
            }
            // preparing for insertion
            foreach ($this->add_validation_rules as $k => $v) {
                $insert_data [$k] = $this->input->post($k);
                if (in_array($k, array("subscription_from", "subscription_category_id", "subscription_status", "bill_number", "receipt_number"))) {
                    $sub_insert['subscription_to'] = $this->getSubscriptionToDate($this->input->post("subscription_from"), $this->input->post("subscription_category_id"));
                    $sub_insert[$k] = $this->input->post($k);

                    unset($insert_data[$k]);
                    unset($sub_insert['subscription_category_id']);
                }
                if (in_array($k, array("scheme_id", "scheme_claimed_date"))) {
                    $schemeInfo[$k] = $this->input->post($k);
                    unset($insert_data[$k]);
                }

//                unset($insert_data['discount']);
            }

            $insert_data['map_pic'] = $img_name;

            if ($this->db->insert("clients", $insert_data)) {

                $sub_insert['client_id'] = $this->db->insert_id();

                $schemeInfo['client_id'] = $this->db->insert_id();
                $schemeInfo['effective_from'] = date("Y-m-d");

                $sub_insert['subscription_status'] = '1';
                $sub_insert['subscription_to'] = $this->getSubscriptionToDate($this->input->post("subscription_from"), $this->input->post("subscription_category_id"));

                //retrieve payable amount
                $subscriptionDetail = $this->db->get_where("subscription_categories", array("subscription_category_id" => $_POST['subscription_category_id']))->row();

                //deduct  scheme discount
                $schemeDetail = $this->db->get_where("schemes", array("scheme_id" => $schemeInfo["scheme_id"]))->row();

                $payable_amount = $subscriptionDetail->subscription_category_rate - (!empty($schemeDetail) ? $schemeDetail->discount : 0);

                $sub_insert['subscription_category_id'] = $subscriptionDetail->subscription_category_id;

                //commented in latest phase of beta release
                //calculate effective payable amount
//                if ($_POST['vat'] != 'on') {
//                    $vat = 0;
//                } else {
//                    $vat = 13;
//                }
//                $effective_payable_amount = $this->effectivePayAmount($payable_amount, $vat, $_POST['discount']);
//                unset($_POST['vat']);

                $reference_type = "1";

                unset($insert_data['subscription_category_id']);

                if ($this->db->insert("subscriptions", $sub_insert)) {
                    //reference id is subscription id
                    $referenceId = $this->db->insert_id();
                    if ($sub_insert["receipt_number"] !== "") {
                        //transaction commented in latest beta release
                        $transaction = array(
                            "payable_amount" => $payable_amount,
                            'reference_id' => $referenceId,
                            'client_sponsor_id' => $sub_insert['client_id'],
                            "reference_type" => $reference_type,
//                        "vat" => $vat,
//                        "discount" => $_POST['discount'],
                        );

                        $particular = $subscriptionDetail->subscription_category_label;

                        //insert transaction detail
                        $invoice_id = $this->insertTransactionLog($transaction, NULL, NULL, $particular);
                    }
                }
                $this->session->set_flashdata('success', 'Client\'s details added successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to add the Client\'s details.');
            }

            //commented in latest beta release
//            redirect('payments/paymentView/' . $invoice_id, 'refresh');
            redirect('clients', 'refresh');
        } else {
            $client = new stdClass();
            // Go through all the known fields and get the post values
            foreach ($this->add_validation_rules as $key => $field) {
                $client->$field['field'] = set_value($field['field']);
            }
            $this->data["client"] = $client;
        }
        $this->template->set_breadcrumb("Add Subscriber's Details", site_url("clients/add"));
        $this->template->title('Title')
                ->set_layout('form_layout')
                ->build('form_view', $this->data);
    }

    public function edit($id = NULL) {
        if ($id == NULL)
            redirect(base_url("clients"), "refresh");
        $this->data['client'] = $this->db->get_where("clients", array("client_id" => $id))->row();

        if (empty($this->data['client']))
            redirect(base_url("clients"), "refresh");

        $this->data['districts'] = $this->db->get("districts")->result();
        $this->data['gvss'] = $this->db->get("gvs")->result();
        $this->data['distributors'] = $this->db->get("distributors")->result();
        $this->data['collectors'] = $this->db->get("collectors")->result();

//        $this->data['schemes'] = $this->db->get_where("schemes", array("scheme_status" => '1'))->result();


        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->edit_validation_rules);

        if ($this->form_validation->run() == TRUE) {

            $img_name = $this->data["client"]->map_pic;

            if ($_FILES["map_pic"]["size"] > 0) {
                $this->load->library('upload', $this->u_config);

                if ($this->upload->do_upload('map_pic')) {
                    @unlink($this->upload_path . $this->data['client']->map_pic);
                    $img = $this->upload->data();
                    $img_name = $img['file_name'];
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br>' . $this->upload->display_errors());
                    redirect('clients/edit/' . $id, 'refresh');
                }
            }

            // preparing for insertion
            foreach ($this->edit_validation_rules as $k => $v) {
                $update_data [$k] = $this->input->post($k);
            }
            $update_data['map_pic'] = $img_name;

            if ($this->db->update("clients", $update_data, array("client_id" => $this->input->post("client_id")))) {
                $this->session->set_flashdata('success', 'Client\'s Details updated Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to update the client_id\'s details.');
            }
            redirect(base_url("clients"), "refresh");
        } else {
            $this->template->set_breadcrumb("Edit Subscriber's Details", site_url("clients/edit/$id"));
            $this->data["page_head"] = "Edit Client's Details -- {$this->data['client']->client_first_name } {$this->data['client']->client_last_name}";

            $this->template->title('Title')
                    ->set_layout('form_layout')
                    ->build('edit_form_view', $this->data);
        }
    }

    /*
     * Delete record
     */

    public function delete($id = NULL) {
        if ($id == NULL)
            redirect(base_url("clients"), "refresh");

        $this->data["client"] = $this->db->get_where("clients", array("client_id" => $id))->row();

        if (empty($this->data["client"]))
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to delete the Client\'s details.');
        else {
            if ($this->db->delete("clients", array("client_id" => $id))) {
                $this->session->set_flashdata('success', 'Clients\'s Details deleted Successfully.');
            }
        }
        redirect(base_url("clients"), "refresh");
    }

    /*
      detail of the records
      Report
     */

    public function detail($id = NULL) {
        if ($id == NULL)
            redirect(base_url("clients"), "refresh");

        $this->data['client'] = $this->db
                ->select("c.client_first_name, c.client_last_name, c.registered_date,c.phone, districts.district_name, "
                        . "gvs.gvs_name, distributors.distributor_name, c.client_id, c.ward_no, c.address, c.map_pic, collectors.collector_name")
                ->join("districts", 'districts.district_id= c.district_id')
                ->join("gvs", "gvs.gvs_id=c.gvs_id")
                ->join("distributors", "distributors.distributor_id= c.distributor_id", "left")
                ->join("collectors", "collectors.collector_id= c.collector_id", "left")
//                ->join("schemes", "schemes.scheme_id= c.scheme_id", "left")
                ->get_where("clients c", array("c.client_id" => $id))
                ->row();
      
        if (empty($this->data['client'])) {
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to retrieve the Clients\'s Details.');
            redirect(base_url("clients"), "redirect");
        }
        if (!empty($this->data['client'])) {
            $this->data["page_head"] = "Client's Details -- {$this->data['client']->client_first_name } {$this->data['client']->client_last_name}";
            $this->data['subscriptions'] = $this->db
                    ->where(array("client_id" => $this->data['client']->client_id))
                    ->order_by("subscription_id", "desc")
                    ->get("subscriptions")
                    ->result();
            $this->template->set_breadcrumb("Subscriber's Details", site_url("clients/detail/$id"));
            $this->template->title('Title')
                    ->set_layout('data_table_layout')
                    ->build('view', $this->data);
        }
    }

    public function renewSubscription($referer = NULL, $client_id = NULL, $redirect = NULL) {

        $this->data['schemes'] = $this->db->get_where("schemes", array("scheme_status" => '1'))->result();
        $this->data['subsType'] = $this->db->get_where("subscription_categories", array("subscription_category_status" => 1))->result();


        $renew_validation = array(
            'subscription_from' => array(
                'field' => 'subscription_from',
                'label' => 'Subscription Effective From',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
//            'subscription_to' => array(
//                'field' => 'subscription_to',
//                'label' => 'Subscription Effective To',
//                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
//            ),
            'client_id' => array(
                'field' => 'client_id',
                'label' => 'Client Id',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'scheme_id' => array(
                'field' => 'scheme_id',
                'label' => "Scheme's Name",
                'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]'
            ),
            'scheme_claimed_date' => array(
                'field' => 'scheme_claimed_date',
                'label' => "Scheme Claimed Date",
                'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]'
            ),
            'subscription_category_id' => array(
                'field' => 'subscription_category_id',
                'label' => 'Subscription Category',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'bill_number' => array(
                'field' => 'bill_number',
                'label' => 'Bill Number',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]|numeric'
            ),
            'receipt_number' => array(
                'field' => 'receipt_number',
                'label' => 'Receipt Number',
                'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]|numeric'
            ),
        );
        $this->load->library('form_validation');
        $this->form_validation->set_rules($renew_validation);

        if ($this->form_validation->run() == TRUE) {

            // preparing for insertion
            foreach ($renew_validation as $k => $v) {
                $insert_data [$k] = $this->input->post($k);
                if (in_array($k, array("scheme_id", "scheme_claimed_date"))) {
                    $schemeInfo[$k] = $this->input->post($k);
                    $schemeInfo["client_id"] = $this->input->post("client_id");
                    unset($insert_data[$k]);
                }
            }

            //retrieve payable amount
            $subscriptionDetail = $this->db->get_where("subscription_categories", array("subscription_category_id" => $_POST['subscription_category_id']))->row();

//            $payable_amount = $subscriptionDetail->subscription_category_rate;

            $schemeDetail = $this->db->get_where("schemes", array("scheme_id" => $schemeInfo["scheme_id"]))->row();

            $effective_payable_amount = $subscriptionDetail->subscription_category_rate - (!empty($schemeDetail) ? $schemeDetail->discount : 0);

//            //calculate effective payable amount
//            if ($_POST['vat'] != 'on') {
//                $vat = 0;
//            } else {
//                $vat = 13;
//            }
//            
//            $effective_payable_amount = $this->effectivePayAmount($payable_amount, $vat, $_POST['discount']);
//            unset(@$_POST['vat']);

            $reference_type = "1";
            $insert_data['subscription_status'] = '1';
            $insert_data['subscription_to'] = $this->getSubscriptionToDate($this->input->post("subscription_from"), $this->input->post("subscription_category_id"));

            if ($this->db->insert("subscriptions", $insert_data)) {
                //reference id is subscription id
                $referenceId = $this->db->insert_id();
                if ($this->input->post('receipt_number') != "") {
                    $transaction = array(
                        "payable_amount" => $effective_payable_amount,
                        'reference_id' => $referenceId,
                        'client_sponsor_id' => $_POST['client_id'],
                        "reference_type" => $reference_type,
//                    "vat" => $vat,
//                    "discount" => $_POST['discount'],
                    );
                    $particular = $subscriptionDetail->subscription_category_label;

                    //insert transaction detail
                    $invoice_id = $this->insertTransactionLog($transaction, NULL, NULL, $particular);
                }
                $this->deactivatePreviousSubscriptions($this->db->insert_id(), $insert_data['client_id']);


                if ($schemeInfo['scheme_id'] != 0) {
                    if ($this->db->insert("scheme_clients", $schemeInfo)) {
                        $this->session->set_flashdata('success', 'Subscription renewed Successfully.');
                    }
                }
//                if ($redirect == "accounts") {
//                    $url = base_url("accounts/index");
//                } else {
//                    $url = base_url("$referer/index");
//                }
                $this->session->set_flashdata('success', 'Subscription renew successful.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to renew the subscription.');
            }
//            proceed to payment after renew is successful
//            redirect(base_url("payments/paymentView/$invoice_id"));
            redirect(base_url("clients/detail"), 'refresh');
        } else {
            if ($client_id == NULL)
                redirect(base_url("$referer"), "refresh");

            $this->data['client'] = $this->db->get_where("clients", array("client_id" => $client_id))->row();

            if (empty($this->data['client'])) {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to retrieve the Clients\'s Details.');
                redirect(base_url("$referer"), "redirect");
            }

            $this->data['latest_subscription'] = $this->db
                    ->where(array("client_id" => $this->data["client"]->client_id))
                    ->order_by("subscription_id", "desc")
                    ->get("subscriptions")
                    ->row();
//            var_dump($this->data['latest_subscription']);die;   
            $this->template->set_breadcrumb("Renew Subscriber's Subscription", site_url("#"));
            $this->data["page_head"] = "Client's Subscription Renew -- {$this->data['client']->client_first_name } {$this->data['client']->client_last_name}";
            $this->template->title('Title')
                    ->set_layout('form_layout')
                    ->build('renew_form', $this->data);
        }
    }

    /*
     * single status change
     */

    public function changestatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->uri->segment(4);

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

    /*
     * for bulk status change
     */

    public function bulkstatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->input->post('id');

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

    /**
     * retrieving gvs/municipality under a district
     */
    public function changeDistrict() {
        if (!$this->input->is_ajax_request())
            redirect(base_url(), 'refresh');

        $records = $this->db->where("district_id", $this->input->post("district_id"))->get("gvs")->result();

        if (empty($records)) {
            $response['result'] = 0;
            echo json_encode($response);
        } else {
            $response['result'] = 1;
            $html = '';
            foreach ($records as $gvs) {
                $html.= '<option value="' . $gvs->gvs_id . '">' . $gvs->gvs_name . '</option>';
            }
            $response['response'] = $html;

            echo json_encode($response);
        }
    }

    private function getSubscriptionToDate($effective_from, $subscription_category_id) {
        $subsDetail = $this->db->get_where("subscription_categories", array("subscription_category_id" => $subscription_category_id))->row();
        $subscriptionTo = date('Y-m-d', strtotime($effective_from . " +" . $subsDetail->subscription_category_span . " month"));
        return $subscriptionTo;
    }

}
