<?php

class Clients_model extends MY_Model {

    public $_table_name = 'clients';
    protected $_primary_key = 'client_id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'client_id';
    protected $_timestamps = FALSE;

    public function __construct() {
        parent::__construct();
    }

    public function allClients() {
        $date = date('Y-m-d');
        $query = 'SELECT c.client_first_name, c.client_last_name,c.phone, districts.district_name, gvs.gvs_name,c.ward_no, distributors.distributor_name, c.client_id, '
                . '(SELECT DATEDIFF( '
                . '(SELECT sc.subscription_to FROM subscriptions sc WHERE sc.client_id=s.client_id order by sc.subscription_id desc limit 1 ), "' . $date . '")'
                . ') AS expiry_days  FROM clients c '
                . 'LEFT JOIN `districts` ON `districts`.`district_id` = `c`.`district_id` '
                . 'LEFT JOIN `gvs` ON `gvs`.`gvs_id`= `c`.`gvs_id` '
                . 'LEFT JOIN `distributors` ON `distributors`.`distributor_id`= `c`.`distributor_id`'
                . 'LEFT JOIN subscriptions s ON s.client_id = c.client_id '
                . 'LEFT JOIN collectors co ON c.collector_id = co.collector_id '
                . ' AND s.client_id IS NOT NULL '
                . 'GROUP BY s.client_id ';
//        echo $query;die;

        return $this->db->query($query)->result();
    }

}
