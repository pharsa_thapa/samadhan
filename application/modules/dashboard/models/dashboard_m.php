<?php

class Dashboard_m extends MY_Model {


    public function __construct() {
        parent::__construct();
    }

    public function recently_added($limit, $orderby, $case) {
        $sql="select * from users u , performers p where u.user_id=p.user_id and p.status='1' and p.host_status='1' and u.host='1'  order by $orderby $case limit $limit";

        return $this->db->query($sql)->result();
    }
    
    public function popular_performers($limit=''){
        $sql="select * from users u, performers p, views v where v.host_id=p.host_id and p.user_id=u.user_id order by v.counter desc";
        if(!empty($limit) ){
            
            $sql.=" limit $limit"; 
        }
         return $this->db->query($sql)->result();
        
    }
    
}
