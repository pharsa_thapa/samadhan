<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends Frontend_Controller {

    function __construct() {
        parent::__construct();
        $this->template->set_breadcrumb("Home", site_url('dashboard'));
    }

    function index() {
        $build = "dashboard";
        $this->load->model("reports/reports_model");

        if ($this->ion_auth->is_admin()) {
            $build = "admin_dashboard";

            $totalRoles = $this->db->get_where("groups", array("id !=" => 1))->result();
            $this->data['totalRoles'] = sizeof($totalRoles);

            $totalUsers = $this->db->get_where("users", array("id !=" => 1))->result();
            $this->data['totalUsers'] = sizeof($totalUsers);

            $totalACL = $this->db->get_where("acl_list", array())->result();
            $this->data['totalACL'] = sizeof($totalACL);
        } else {
            $this->data['totalSubscribers'] = $this->db->count_all("clients");

            $this->data['renewableSubscribers'] = sizeof($this->reports_model->allRenewableClients());

            $today = date("Y-m-d");
            $this->data['expiredSubscriptions'] = sizeof($this->db->join("subscriptions s", "s.client_id = c.client_id")
                            ->where("s.subscription_to < '" . $today . "'")
                            ->get("clients c", array())
                            ->result());
        }


        $this->template->set_breadcrumb("Dashboard", site_url('dashboard'));
        $this->data['page_head'] = "Welcome to Dashboard";
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build($build, $this->data);
    }

}
