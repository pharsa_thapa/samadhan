<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat purple">
                                <div class="visual">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo $totalSubscribers ?>
                                    </div>
                                    <div class="desc">                           
                                        Total Subscribers
                                    </div>
                                </div>
                                <a class="more" href="<?php echo base_url("clients"); ?>">
                                    View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>                 
                            </div>
                        </div>
                        
                        
                        
                        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="fa fa-user" ></i>
                                </div>
                                <div class="details">
                                    <div class="number"> <?php echo $renewableSubscribers ?></div>
                                    <div class="desc">Renewable Subscribers</div>
                                </div>
                                <a class="more" href="<?php echo base_url("reports/renewableSubscribers"); ?>">
                                    View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>                 
                            </div>
                        </div>
                        
                        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="details">
                                    <div class="number"> <?php echo $expiredSubscriptions ?> &nbsp;</div>
                                    <div class="desc">Expired Subscriptions</div>
                                </div>
                                <a class="more" href="#">
                                    View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>                 
                            </div>
                        </div>

                        
                        
                    </div>

                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->