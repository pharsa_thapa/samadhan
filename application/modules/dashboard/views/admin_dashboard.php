<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="row">
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat blue">
                            <div class="visual">
                                <i class="fa fa-user"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <?php echo $totalRoles ?>
                                </div>
                                <div class="desc">                           
                                    Total User Roles
                                </div>
                            </div>
                            <a class="more" href="#">
                                View more <i class="m-icon-swapright m-icon-white"></i>
                            </a>                 
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat purple">
                            <div class="visual">
                                <i class="fa fa-comments"></i>
                            </div>
                            <div class="details">
                                <div class="number"> <?php echo $totalUsers ?></div>
                                <div class="desc">Total Users</div>
                            </div>
                            <a class="more" href="#">
                                View more <i class="m-icon-swapright m-icon-white"></i>
                            </a>                 
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat green">
                            <div class="visual">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="details">
                                <div class="number"> <?php echo $totalACL ?> &nbsp;</div>
                                <div class="desc">Total ACL</div>
                            </div>
                            <a class="more" href="#">
                                View more <i class="m-icon-swapright m-icon-white"></i>
                            </a>                 
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->