<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->template->set_breadcrumb('Home', base_url());

        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');

        $app_name = $this->db->get_where("settings", array("settings_name" => "application_name"))->row();
        $com_name = $this->db->get_where("settings", array("settings_name" => "company_name"))->row();
        $app_logo = $this->db->get_where("settings", array("settings_name" => "application_logo"))->row();

        $this->data['settings']['application_title'] = $app_name->settings_value;
        $this->data['settings']['company_name'] = $com_name->settings_value;
        $this->data['settings']['application_logo'] = $app_logo->settings_value;
    }

    //redirect if needed, otherwise display the user list
    function index() {

        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        } else {
            redirect(base_url("dashboard", "refresh"));
        }
    }

    //log the user in

    function login() {
        if ($this->ion_auth->logged_in()) {
            redirect(base_url("dashboard"));
        }
        $this->data['title'] = "Login";

        //validate form input
        $this->form_validation->set_rules('identity', 'Identity', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == true) {
            //check to see if the user is logging in
            //check for "remember me"
            $remember = (bool) $this->input->post('remember');



            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {

                //if the login is successful
                //redirect them back to the home page
                $this->db->where("id", $this->ion_auth->get_user_id())->update("users", array("last_login_ip" => $this->input->ip_address()));
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect('auth/login', 'refresh');
            } else {
                //if the login was un-successful
                //redirect them back to the login page
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('auth/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
            }
        } else {
            //the user is not logging in so display the login page
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
                "type" => "text",
                "class" => "form-control",
                "placeholder" => "Email address"
            );
            $this->data['password'] = array('name' => 'password',
                'id' => 'password',
                'type' => 'password',
                "class" => "form-control",
                "name" => "password",
                "placeholder" => "Password"
            );

            $this->template->title("Login")
                    ->set_layout('empty_layout')
                    ->build("login", $this->data);
        }
    }

    //log the user out
    function logout() {
        $this->data['title'] = "Logout";

        //log the user out
        $logout = $this->ion_auth->logout();

        //redirect them to the login page
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        redirect('auth/login', 'refresh');
    }

    //change password
    function change_password() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        $userDetail = $this->db->get_where("users", array("id" => $this->ion_auth->get_user_id()))->row();
        $this->data["menus"] = $this->getMenus();
        $this->data["username_header"] = $userDetail->username;

        $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
        $this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');



        $user = $this->ion_auth->user()->row();

        if ($this->form_validation->run() == false) {
            //display the form
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');

            $this->data['old_password'] = array(
                "class" => "form-control input-group-sm",
                "placeholder" => "Old Password",
                "required" => "required",
                'name' => 'old',
                'id' => 'old',
                'type' => 'password'
            );
            $this->data['new_password'] = array(
                "class" => "form-control input-group-sm",
                "placeholder" => "New Password",
                "required" => "required",
                'name' => 'new',
                'id' => 'new',
                'type' => 'password',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
            $this->data['new_password_confirm'] = array(
                "class" => "form-control input-group-sm",
                "placeholder" => "Re-type Password",
                "required" => "required",
                'name' => 'new_confirm',
                'id' => 'new_confirm',
                'type' => 'password',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
            $this->data['user_id'] = array(
                'name' => 'user_id',
                'id' => 'user_id',
                'type' => 'hidden',
                'value' => $user->id,
            );

            $this->template->set_breadcrumb('Change Password', base_url("auth/change_password"));
            $this->data['page_head'] = "Change Password";
            //render
            $this->template->title("Change Password")
                    ->set_layout('form_layout')
                    ->build("change_password", $this->data);


//            $this->_render_page('auth/change_password', $this->data);
        } else {
            $identity = $this->session->userdata('identity');

            $change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

            if ($change) {
                //if the password was successfully changed
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $this->logout();
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('auth/change_password', 'refresh');
            }
        }
    }

    function _get_csrf_nonce() {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    function _valid_csrf_nonce() {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
                $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')
        ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function getMenus() {
        $menus = array();
        $menuHeads = $this->db->order_by("default_menu_head", "ASC")->get("menu_group_head")->result();
        foreach ($menuHeads as $head) {
            $index = empty($head->menu_head) ? $head->menu_head : $head->default_menu_head;

            //join with permissions to retrieve all the related urls only
            $groupDetail = $this->db->get_where("users_groups", array("user_id" => $this->ion_auth->get_user_id()))->row();

            $menus[$index] = $this->db
                    ->join("permissions p", "al.acl_list_id=p.acl_id")
                    ->get_where("acl_list al", array("al.menu_group_head" => $head->menu_group_head_id, "al.include_in_menu" => "1", "p.group_id" => $groupDetail->group_id))
                    ->result();
        }
        return $menus;
    }

}
