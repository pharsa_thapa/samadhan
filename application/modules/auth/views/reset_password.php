<section  class="subtitle-section mb20">
    <div class="container">


        <!--        <div class="subtitle">
                    <b>Height :</b><span class="mini-subtitle" > 5 feet 10 inches</span>
                    <b>Weight :</b><span class="mini-subtitle" > 250 pounds</span>
                    <b>Last Seen :</b><span class="mini-subtitle" > 212 Railway Street</span>
        
                </div>
                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 pull-right">
                    <div class="reward text-center btn btn-success ">Provide Lead </div>
                </div>-->
    </div>

</section>


<section  class="home-section ">

    <div class="container">
        <div class="row"> 
            <div class="col-xs-12 col-lg-12 mb30 ">
                <div>
                    <div class="team boxed-grey">

                        <div class="row" style="padding:0px 30px;">

                            <div class="inner ">


                                <p class="nickname"><?php echo lang('reset_password_heading'); ?></p>
                                <hr/>
                                <div class="col-lg-4">

                                    <div id="infoMessage"><?php echo $message; ?></div>

                                    <?php echo form_open('auth/reset_password/' . $code); ?>

                                    <p>
                                        <label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length); ?></label> <br />
                                        <?php echo form_input($new_password); ?>
                                    </p>

                                    <p>
                                        <?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm'); ?> <br />
                                        <?php echo form_input($new_password_confirm); ?>
                                    </p>

                                    <?php echo form_input($user_id); ?>
                                    <?php echo form_hidden($csrf); ?>
                                    <br><br>
                                    
                                    <p><?php echo form_submit('submit', lang('reset_password_submit_btn'), "class='btn btn-info'"); ?></p>

                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> 
            </div>
        </div>
    </div>
</section>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>