<div class="col-md-12">
    <div class="col-md-1">&nbsp;</div>
    <div class="col-md-10">
        <div class="col-md-12">
            <h3 class="nickname">
                <?php echo lang('change_password_heading'); ?>
            </h3>
        </div>
        <div class="col-md-12">
            <div id="infoMessage" style="color: red;"><?php echo $message; ?></div>
        </div>
        <div class="col-md-12">
            <?php echo form_open("auth/change_password"); ?>

        </div>
        <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Old Password</label>
            <?php echo form_input($old_password); ?>
        </div>

        <div class="form-group col-md-12">
            <label for="exampleInputEmail1">New Password</label>
            <?php echo form_input($new_password); ?>
        </div>     

        <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Confirm New Password</label>
            <?php echo form_input($new_password_confirm); ?>
        </div>  
        <div class="col-md-12">
            <?php echo form_input($user_id); ?>

            <?php echo form_submit('submit', lang('change_password_submit_btn'), " class='btn btn-success pull-right' style='margin-top:10px;'"); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
    <div class="col-md-1">&nbsp;</div>
</div>
