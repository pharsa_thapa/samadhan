<section  class="subtitle-section mb20">
    <div class="container">


        <!--        <div class="subtitle">
                    <b>Height :</b><span class="mini-subtitle" > 5 feet 10 inches</span>
                    <b>Weight :</b><span class="mini-subtitle" > 250 pounds</span>
                    <b>Last Seen :</b><span class="mini-subtitle" > 212 Railway Street</span>
        
                </div>
                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 pull-right">
                    <div class="reward text-center btn btn-success ">Provide Lead </div>
                </div>-->
    </div>

</section>


<section  class="home-section ">

    <div class="container">
        <div class="row"> 
            <div class="col-xs-12 col-lg-12 mb30 ">
                <div>
                    <div class="team boxed-grey">
                        <div class="row" style="padding:0px 30px;">

                            <div class="inner ">
                                <p class="nickname"><?php echo lang('forgot_password_heading'); ?></p>
                                <hr/>
                                <div class="col-lg-8">
                                    <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label); ?>
                                        <br></p>

                                    <div id="infoMessage"><?php echo $message; ?></div>

                                    <?php echo form_open("auth/forgot_password"); ?>

                                    <p>
                                        <label for="email"><?php echo sprintf(lang('forgot_password_email_label'), $identity_label); ?></label> <br />
                                        <?php echo form_input($email); ?>
                                        <br>
                                    </p>

                                    <p> <input type="submit" value="Submit" class="btn btn-success pull-right" style="margin-top: 10px;"></p>

                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>