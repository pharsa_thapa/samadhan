<section class="signup">
    <div class="container">    
        <div id="signupbox"  class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info wow shake" data-wow-delay="0.2s">
                <div class="panel-heading">
                    <div class="panel-title">Sign up as a viewer</div>
                    <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="#" onclick="$('#signupbox').hide();
                            $('#loginbox').show()">Sign In</a></div>
                </div>  
                <div class="panel-body" >
                    <?php echo form_open($action, array('class' => 'form-horizontal', 'id' => 'signupform', "role" => "form")); ?>
                    <?php if ($message) { ?>
                        <div id="signupalert"  class="alert alert-danger">
                            <span>Error: <?php echo $message; ?></span>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                            <?php echo form_input($email); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="firstname" class="col-md-3 control-label">First Name</label>
                        <div class="col-md-9">
                            <?php echo form_input($first_name); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-md-3 control-label">Last Name</label>
                        <div class="col-md-9">
                            <?php echo form_input($last_name); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="zipcode" class="col-md-3 control-label">Zip Code</label>
                        <div class="col-md-4">
                            <?php echo form_input($zip_code); ?>
                        </div>
                        <div class="col-md-4" style="color:red;display: none;" id="zip-err">Please enter a valid zip code!</div>
                    </div>

                    <div class="form-group">
                        <label for="state" class="col-md-3 control-label">State</label>
                        <div class="col-md-9">
                            <?php echo form_input($states); ?>
                        </div>

                    </div>

                    <!--                    <div class="form-group">
                                            <label for="city" class="col-md-3 control-label">City</label>
                                            <div class="col-md-9">
                    <?php // echo form_dropdown('city', $cities, '', "id='change-city' class='form-control' placeholder='City'"); ?>
                                            </div>
                    
                                        </div>-->

                    <!--
                    
                    
                                        <div class="form-group">
                                            <label for="zipcode" class="col-md-3 control-label">Zip Code</label>
                                            <div class="col-md-4">
                    <?php // echo form_dropdown('zip_code', $zip_code, '', 'class="form-control" placeholder="Zip Code"'); ?>
                                            </div>
                    
                                        </div>-->

                    <!--                    
                                        <div class="form-group">
                                            <label for="country" class="col-md-3 control-label">Country</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="country" placeholder="Country">
                                            </div>
                    
                                        </div>-->

                    <hr/>
                    <div class="form-group">
                        <label for="username" class="col-md-3 control-label">Username</label>
                        <div class="col-md-9">
                            <?php echo form_input($username); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Password</label>
                        <div class="col-md-9">
                            <?php echo form_input($password); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="cpassword" class="col-md-3 control-label">Confirm Password</label>
                        <div class="col-md-9">
                            <?php echo form_input($password_confirm); ?>
                        </div>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label for="paypal" class="col-md-3 control-label">Paypal ID</label>
                        <div class="col-md-9">
                            <?php echo form_input($paypal); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <!-- Button -->                                        
                        <div class="col-md-offset-3 col-md-9">


                            <button id="btn-signup" type="submit" class="btn btn-info pull-right"><i class="icon-hand-right"></i> &nbsp; Sign Up</button>

                        </div>
                    </div>

                    <?php echo form_close(); ?>
                </div>
            </div>

        </div> 
    </div>
</section>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
<script src="<?php echo base_url("assets/frontend/js/signup-validation.js");?>"></script>
