<!DOCTYPE html>
<html>
    <head>

        <!-- Title -->
        <title><?php echo $settings['application_title']; ?> | Login </title>

        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="Pharsa Bahadur Thapa" content="Metro-Payroll" />

        <!-- Styles -->
        <link href='<?php echo base_url('fonts/csse092.css?family=Open+Sans:400,300,600'); ?>' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url('assets/plugins/pace-master/themes/blue/pace-theme-flash.css'); ?>" rel="stylesheet"/>
        <link href="<?php echo base_url('assets/plugins/uniform/css/uniform.default.min.css'); ?>" rel="stylesheet"/>
        <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url('assets/plugins/fontawesome/css/font-awesome.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url('assets/plugins/line-icons/simple-line-icons.css'); ?>" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url('assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css'); ?>" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url('assets/plugins/waves/waves.min.css'); ?>" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url('assets/plugins/switchery/switchery.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url('assets/plugins/3d-bold-navigation/css/style.css'); ?>" rel="stylesheet" type="text/css"/>	

        <!-- Theme Styles -->
        <link href="<?php echo base_url('assets/css/modern.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url('assets/css/themes/white.css'); ?>" class="theme-color" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            .custom-margin-right-nav{ margin-top:10px !important; margin-right: 30px !important;}

        </style>



        <script src="<?php echo base_url('assets/plugins/3d-bold-navigation/js/modernizr.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/offcanvasmenueffects/js/snap.svg-min.js'); ?>"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="page-login">
        <div class="page-content">

            <div class="navbar" >
                <div class="navbar-inner">

                    <div class="logo-box">
                        <a href="index.html" class="logo-text"><span><?php echo $settings['application_logo']; ?></span></a>
                    </div><!-- Logo Box -->

                    <div class="topmenu-outer" >
                        <div class="top-menu">

                            <ul class="nav navbar-nav navbar-right custom-margin-right-nav">
                                <li>
                                   <span style="font-weight: bold;">
                                       <?php
                                       $day = date("j");
                                       echo date("j");
                                       ?>
                                       <sup><?php echo $day == 1 ? "st" : ($day == 2 ? "nd" : ($day == 3 ? "rd" : "th")) ?></sup>

                                       <?php
                                       echo date("F");
                                       ?>,

                                       <?php
                                       echo date("Y");
                                       ?>

                                       <br>


                                       <?php
                                       echo date("l");
                                       ?>
                                </li>
                            </ul><!-- Nav -->
                        </div><!-- Top Menu -->
                    </div>
                </div>
            </div>



            <div class="page-inner">
                <div id="main-wrapper">
                    <div class="row">
                        <div class="col-md-3 center">


                            <div class="login-box" style="margin:80px auto">

                                <a href="<?php echo base_url(); ?>" class="logo-name text-lg text-center margin-bottom-30"  style="color:#1DB198;"><?php echo $settings['application_title']; ?> Login</a>
                                <!--<p class="text-center m-t-md">Please login into your account.</p>-->

                                <?php
                                if ($this->session->flashdata('message')) {
                                    echo "<div class='alert alert-danger alert-dismissible' role='alert'>";
                                    ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <?php
                                    echo $this->session->flashdata('message');
                                    echo "</div>";
                                }
                                ?>

                                <?php echo form_open("auth/login", array("id" => "loginform", "class" => "m-t-md", "role" => "form")); ?>

                                <div class="form-group">
                                    <?php echo form_input($identity); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo form_input($password); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?>Keep me Logged in!
                                </div>
                                <button type="submit" class="btn btn-success btn-block">Login</button>


                                </form>
                                <p class="text-center m-t-xs text-sm">2015 &copy; <?php echo $settings['company_name']; ?></p>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
            </div><!-- Page Inner -->
        </div><!-- Page Content -->


        <!-- Javascripts -->
        <script src="<?php echo base_url('assets/plugins/jquery/jquery-2.1.3.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>


        <script src="<?php echo base_url('assets/plugins/pace-master/pace.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-blockui/jquery.blockui.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/switchery/switchery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/uniform/jquery.uniform.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/offcanvasmenueffects/js/classie.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/offcanvasmenueffects/js/main.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/waves/waves.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/3d-bold-navigation/js/main.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/modern.min.js'); ?>"></script>

    </body>

</html>