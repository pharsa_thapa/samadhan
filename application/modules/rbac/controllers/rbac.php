<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class RBAC extends Frontend_Controller {

    private $menuGroupHead;
    private $menuHeadDescription;
    private $defaultMenuHead;
    private $alert = "<br span style='color=/'red !important;'/'>Strictly admin area! If you are not the system admin, Kindly, Please <a href='/auth/logout'>getback</a></span>";

    public function __construct() {
        parent::__construct();
        //set breadcrumb for the controller
        $this->template->set_breadcrumb('Home', base_url());
        $this->template->set_breadcrumb("RBAC", site_url('rbac'));

        $this->load->model("rbac_model");
    }

    public function index() {
        $this->listMenuHeadList();
    }

    /* get post from the form  submitted by the user for setting Menu Group Head
     * set attributes value
     */

    public function setMenuGroupHead() {

//        validation rules for the form input

        $validation_rules = array(
            'default_menu_head' => array(
                'field' => 'default_menu_head',
                'label' => 'Default Menu Head',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'menu_head' => array(
                'field' => 'menu_head',
                'label' => 'Menu Head',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'menu_head_description' => array(
                'field' => 'menu_head_description',
                'label' => 'Menu Head Description',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            )
        );

        $this->load->library('form_validation');
        $this->form_validation->set_rules($validation_rules);
        //        No any validation errors
        if ($this->form_validation->run() == TRUE) {
            // setting attributes value
            $this->menuGroupHead = $this->input->post("menu_head");
            $this->menuHeadDescription = $this->input->post("menu_head_description");
            $this->defaultMenuHead = $this->input->post("default_menu_head");
            //call addMenuGroupHead function
            $this->addMenuGroupHead();
        } else {
//            validation errors detected
            $menuHead = new stdClass();
            // Go through all the known fields and get the post values
            foreach ($validation_rules as $key => $field) {
                $menuHead->$field['field'] = set_value($field['field']);
            }
            $this->data["menuHead"] = $menuHead;
        }

        $this->template->set_breadcrumb("Add Menu Head", site_url('rbac/setMenuGroupHead'));
        $this->data['page_head'] = "Add Menu Head Details" . $this->alert;

//        page render
        $this->template->title('Title')
                ->set_layout('form_layout')
                ->build('menu_head_form_view', $this->data);
    }

    /*
     * Inserting set values of the attributes in the physical table
     */

    private function addMenuGroupHead() {
        $insert_data = array("default_menu_head" => $this->defaultMenuHead, 'menu_head' => $this->menuGroupHead, 'menu_head_description' => $this->menuHeadDescription);
        if ($this->db->insert("menu_group_head", $insert_data)) {
//            successful insertion /set success message
            $this->session->set_flashdata('success', 'New Menu Group Head Added Successfully.');
        } else {
//            error while inserting data in database
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to add the Menu Group Head.');
        }
        redirect('rbac/listMenuHeadList', 'refresh');
    }

    /*
     * List all the avilable menu head list in database table
     */

    public function listMenuHeadList() {
//        query all the records
        $this->data['records'] = $this->db->get("menu_group_head")->result();

        $this->template->set_breadcrumb("Menu Head List", site_url('rbac/listMenuHeadList'));
        $this->data['page_head'] = "Menu Head List" . $this->alert;
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('menu_head_list_view', $this->data);
    }

    /*
     * Edit Menu Head as requested by the user
     * @param menu_head_id /pk for Menu head table
     */

    public function editMenuHead($id = NULL) {
        $this->load->library('form_validation');
//setting up validation rules
        $validation_rules = array(
            'default_menu_head' => array(
                'field' => 'default_menu_head',
                'label' => 'Default Menu Head',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'menu_head' => array(
                'field' => 'menu_head',
                'label' => 'Menu Head',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'menu_head_description' => array(
                'field' => 'menu_head_description',
                'label' => 'Menu Head Description',
                'rules' => 'trim|htmlspecialchars|required|xss_clean'
            )
        );
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run() == TRUE) {
            // preparing for insertion
            foreach ($validation_rules as $k => $v) {
                $update_data [$k] = $this->input->post($k);
            }

//            updating the record details by the post values
            if ($this->db->update("menu_group_head", $update_data, array("menu_group_head_id" => $this->input->post("menu_group_head_id")))) {
                $this->session->set_flashdata('success', 'Menu Head Details updated Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to update the Menu Head details.');
            }
            redirect('rbac/listMenuHeadList', 'refresh');
        } else {
//            if @param is not found
            if ($id == NULL)
                redirect(base_url("rbac/listMenuHeadList"), "refresh");

            $this->data["menuHead"] = $this->db->get_where("menu_group_head", array("menu_group_head_id" => $id))->row();

            if (empty($this->data["menuHead"]))
                redirect(base_url("rbac/listMenuHeadList"), "refresh");

            $this->template->set_breadcrumb("Edit Menu Head Details", site_url("rbac/editMenuHead/$id"));

            $this->data['page_head'] = "Edit Menu Head's Details -- {$this->data['menuHead']->default_menu_head}" . $this->alert;
            $this->template->title('Title')
                    ->set_layout('form_layout')
                    ->build('menu_head_form_view', $this->data);
        }
    }

    /*
     * Detail view of menu Head
     * @param $id/Menu_head_id/Pk for the phisical table
     */

    public function detailMenuHead($id = NULL) {
        if ($id == NULL)
            redirect(base_url("rbac/listMenuHeadList"), "refresh");
        $this->data['record'] = $this->db->get_where("menu_group_head", array("menu_group_head_id" => $id))->row();
        if (empty($this->data['record']))
            redirect(base_url("rbac/listMenuHeadList"), "refresh");
        $this->template->set_breadcrumb("Menu Head Details", site_url("rbac/detailMenuHead/$id"));

        $this->data['page_head'] = "Menu Head Details -- {$this->data['record']->menu_head}";
        $this->template->title('Title')
                ->set_layout('detail_view_layout')
                ->build('menu_head_view', $this->data);
    }

    /*
     * Delete menu head
     */

    public function deleteMenuHead($menu_id = null) {
        if (is_null($menu_id))
            redirect(base_url("rbac/listMenuHeadList", "refresh"));
        //default menu heads cannot be deleted
        if (in_array($menu_id, array(1, 2, 3, 4, 5, 6))) {
            $this->session->set_flashdata("error", "Cannot Delete default Menu Heads");
            redirect(base_url("rbac/listMenuHeadList", "refresh"));
        }
        $data = $this->db->get_where("menu_group_head", array("menu_group_head_id" => $menu_id))->row();
        if (empty($data)) {
            $this->session->set_flashdata("error", "Cannot Delete default Menu Heads");
            redirect(base_url("rbac/listMenuHeadList", "refresh"));
        }
        $this->db->delete("menu_group_head", array("menu_group_head_id" => $menu_id));
        $this->session->set_flashdata("success", "Menu Deleted Successfully!");
        redirect(base_url("rbac/listMenuHeadList", "refresh"));
    }

    /*
     * Add ACL Item under a menu head
     */

    public function addACLItem($menuId = NULL) {
        $menuHead = $this->db->get_where("menu_group_head", array("menu_group_head_id" => $menuId))->row();
        if (empty($menuHead))
            redirect(base_url("rbac"));

        $acl_rules = array(
            'acl_list_name_0' => array(
                'field' => 'acl_list_name_0',
                'label' => "ACL Label",
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'acl_list_url_0' => array(
                'field' => 'acl_list_url_0',
                'label' => 'ACL URL',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'menu_group_head_0' => array(
                'field' => 'menu_group_head_0',
                'label' => 'Menu Head',
                'rules' => 'trim|xss_clean|max_length[100]|required'
            )
        );


        if ($this->input->post()) {
            $this->load->library("bulk_form_validation");
            $this->bulk_form_validation->parseBulkForm($this->input->post(), $acl_rules);
            $response = $this->bulk_form_validation->processResponse();
            if (!empty($response["validInputs"])) {
                $this->data["successMessage"] = $response['totalValidInputsCount'] . " of " . $response["totalInputsCount"] . " inserted successfully!";
                $this->db->insert_batch("acl_list", $response["validInputs"]);
                $this->session->set_flashdata('success', $this->data['successMessage']);
            }

            if (!empty($response["invalidInputs"])) {
                $this->data["errorMessage"] = $response['totalInvalidInputsCount'] . " of " . $response["totalInputsCount"] . " seems to have some errors!";
                $this->data["invalidInputs"] = $response["invalidInputs"];
                $this->session->set_flashdata('error', $this->data["errorMessage"]);
            }

            redirect(base_url("rbac/viewACLItems/$menuId"), "refresh");
        }
        $this->data["menu_id"] = $menuId;
        $this->template->set_breadcrumb($menuHead->default_menu_head, site_url("rbac/detailMenuHead/$menuId"));
        $this->template->set_breadcrumb("Add ACL Item", site_url("rbac/addACLItem/$menuId"));

        $this->data['page_head'] = "Add ACL Item - {$menuHead->default_menu_head}";
        $this->template->title("")
                ->set_layout('form_layout')
                ->build('acl_form', $this->data);
    }

    //view all the menu related acl Items

    public function viewACLItems($menuId) {
        $this->data['detail'] = $this->db->get_where("menu_group_head", array("menu_group_head_id" => $menuId))->row();

        if (empty($this->data['detail']))
            redirect(base_url("rbac", "refresh"));

        $this->data['records'] = $this->db->join("menu_group_head mg", "mg.menu_group_head_id= acl_list.menu_group_head")->get_where("acl_list", array("menu_group_head" => $menuId))->result();
//        var_dump($expression)
        $this->template->set_breadcrumb(!empty($this->data['detail']->menu_head) ? $this->data['detail']->menu_head : $this->data['detail']->default_menu_head, site_url('#'));
        $this->data['page_head'] = "ACL List Under ";
        $this->data['page_head'].=!empty($this->data['detail']->menu_head) ? $this->data['detail']->menu_head : $this->data['detail']->default_menu_head;
        $this->data['page_head'].= $this->alert;
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('menu_acl_list_view', $this->data);
    }

    /*
     * load page for managing access levels for user group
     * @param group id 
     */

    public function managePermission($groupId) {
        $this->alert = '';
        $this->data['group'] = $this->db->get_where("groups", array("id" => $groupId))->row();
        if (empty($this->data['group']))
            redirect(base_url("userGroups", "refresh"));

        $menuGroup = $this->db->get("menu_group_head")->result();
        $aclList = array();
        foreach ($menuGroup as $menuGrouphead) {
            $index = empty($menuGrouphead->menu_head) ? $menuGrouphead->default_menu_head : $menuGrouphead->menu_head;
            $aclList[$index] = $this->rbac_model->getPermissions($groupId, $menuGrouphead->menu_group_head_id);
//            _where("acl_list", array("menu_group_head" => $menuGrouphead->menu_group_head_id))->result();
        }
        $this->data['acl'] = $aclList;
        $this->data['userGroup'] = $groupId;
        $this->data['userGroupName'] = $this->data['group']->name;

        $this->template->set_breadcrumb($this->data['group']->name, site_url("#"));
        $this->template->set_breadcrumb("Manage User Group Permissions", site_url("rbac/managePermission/{$this->data['group']->id}"));
        $this->data['page_head'] = "Manage Permissions to " . $this->data['group']->name;


        $this->template->title('Title')
                ->set_layout('form_layout')
                ->build('manage_permission_view', $this->data);
    }

    /*
     * Save allocated permissions for user group
     */

    function savePermissions() {
        $this->session->set_flashdata('success', 'Permissions Saved successfully');
        redirect(base_url("userGroups"), "refresh");
    }

    /*
     * List of all available acl items 
     */

    function listAllACLItems() {
        $this->data['records'] = $this->db->join("menu_group_head m", "m.menu_group_head_id= al.menu_group_head")->get("acl_list al")->result();

        $this->template->set_breadcrumb("All ACL Items", site_url("rbac/listAllACLItems"));
        $this->data['page_head'] = " List Of All ACL Items ";


        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('acl_list_view', $this->data);
    }

    /*
     * 
     * delete acl item
     * @param aclid to be deleted
     * 
     */

    function deleteACLItem($acl_id, $redirect = null) {
        if ($acl_id == NULL)
            redirect(base_url("rbac"), "refresh");

        $this->data["acl"] = $this->db->get_where("acl_list", array("acl_list_id" => $acl_id))->row();

        if (empty($this->data["acl"]))
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to delete the ACL Item details.');
        else {
            if ($this->db->delete("acl_list", array("acl_list_id" => $acl_id))) {
                $this->session->set_flashdata('success', 'ACL Item Details deleted Successfully.');
            }
        }

        if ($redirect == "menuList") {
            $newRedirect = "viewACLItems";
        } else {
            $newRedirect = 'listAllACLItems';
        }
        redirect(base_url("rbac/$newRedirect"), "refresh");
    }

    /*
     * Delete record
     */

    public function editACLItem($id = NULL) {

        $validation_rules = array(
            'acl_list_name' => array(
                'field' => 'acl_list_name',
                'label' => "ACL Label",
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'acl_list_url' => array(
                'field' => 'acl_list_url',
                'label' => 'ACL URL',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            "acl_list_id" => array(
                'field' => 'acl_list_id',
                'label' => 'ACL Id',
                'rules' => 'trim|xss_clean|max_length[100]|required'
            ),
        );
        $this->load->library('form_validation');
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run() == TRUE) {
            // preparing for insertion
            foreach ($validation_rules as $k => $v) {
                $update_data [$k] = $this->input->post($k);
            }
            unset($update_data['acl_list_id']);
            if ($this->db->update("acl_list", $update_data, array("acl_list_id" => $this->input->post("acl_list_id")))) {
                $this->session->set_flashdata('success', 'ACL Details updated Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to update the ACL details.');
            }
            redirect('rbac/index', 'refresh');
        } else {
            if ($id == NULL)
                redirect(base_url("rbac"), "refresh");

            $this->data["acl"] = $this->db->get_where("acl_list", array("acl_list_id" => $id))->row();

            if (empty($this->data["acl"]))
                redirect(base_url("rbac"), "refresh");

            $this->template->set_breadcrumb("All ACL List", site_url("rbac/listAllACLItems"));
            $this->template->set_breadcrumb("Edit ACL Details", site_url("rbac/editACLItem/$id"));

            $this->data['page_head'] = "Edit ACL Details - {$this->data['acl']->acl_list_name}";
            $this->template->title('Title')
                    ->set_layout('form_layout')
                    ->build('acl_edit_form_view', $this->data);
        }
    }

}
