<div class="table-responsive">
    <table id="example" class="display table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>ACL Label</th>
                <th>ACL URL</th>
                <th>Include in Menu List</th>
                <th>Actions</th>
                <!--<th>Actions</th>-->
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ACL Label</th>
                <th>ACL URL</th>
                <th>Include in Menu List</th>
                <th>Actions</th>
            </tr>
        </tfoot>
        <tbody>

            <?php foreach ($records as $record) : ?>
                <tr>
                    <td><?php echo $record->acl_list_name; ?></td>
                    <td><?php echo $record->acl_list_url; ?></td>
                    <td>
                        <input type="checkbox" id="acl_list_<?php echo $record->acl_list_id; ?>" <?php echo $record->include_in_menu == '1' ? "checked" : "" ?>>
                    <td> 

                        <a href="<?php echo base_url("rbac/editACLItem/$record->acl_list_id"); ?>">
                            Edit
                        </a> 
                        |
                         <a href="<?php echo base_url("rbac/deleteACLItem/$record->acl_list_id/menuList"); ?>" 
                           onclick="javascript : return confirm('Are you sure, you want to delete this record permanently?');">
                            Delete
                        </a>

                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>  
</div>

<script src="<?php echo base_url("assets/js/custom/ajaxCalls.js")?>" type="text/javascript"></script>