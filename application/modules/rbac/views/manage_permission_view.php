<div class="row">
    <input type="hidden"  value="<?php echo $userGroup; ?>" id="userGroupId"> 
    <div class="col-md-12">


        <div class="col-md-12">
            <h2> Check the items which you want to provide access to <?php echo $userGroupName; ?>!</h2>
            <hr>
            <?php
            foreach ($acl as $key => $val) {
                ?>

                <div class="col-md-12">
                    <h3><?= $key ?></h3>
                    <?php if (empty($val)) { ?>
                        <label class="color-red">Acl Items Not Available!</label>
                        <?php
                    } else {
                        ?>
                        <?php foreach ($val as $item): ?>
                            <label class="checkbox-inline" for="checkboxes-<?php echo $item->acl_list_id ?>">
                                <input type="checkbox" id="checkboxes-<?php echo $item->acl_list_id ?>" <?php echo ($item->count > 0) ? "checked" : "" ?>>
                                <?php echo $item->acl_list_name; ?>

                            </label>
                            <br>
                            <?php
                        endforeach;
                    }
                    ?>
                    <hr>
                </div>
                <?php
            }
            ?>
        </div>


    </div>
    <div class="form-group col-md-offset-11">
        <a href="<?php echo base_url("rbac/savePermissions"); ?>" class="btn btn-success">Save</a>
    </div>

</div>
<script src="<?php echo base_url("assets/js/custom/acl.js") ?>" type="text/javascript"></script>