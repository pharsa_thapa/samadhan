

<div class="form-group col-md-12">
    <label for="exampleInputEmail1">Default Menu Head</label>
    <input type="text" class="form-control" placeholder="Default Menu Head" name="default_menu_head" id="menu_head" value="<?php echo!empty($menuHead->default_menu_head) ? $menuHead->default_menu_head : "" ?>" required="required">
</div>

<div class="form-group col-md-12">
    <label for="exampleInputEmail1">Menu Head</label>
    <input type="text" class="form-control" placeholder="Menu Head" name="menu_head" id="menu_head" value="<?php echo!empty($menuHead->menu_head) ? $menuHead->menu_head : "" ?>" required="required">
</div>


<div class="form-group col-md-12 control-group">
    <label for="exampleInputEmail1">Menu Head Description</label>
    <textarea name="menu_head_description" class="summernote" id="description" style="display: none" required="required">
       
        <?php echo!empty($menuHead->menu_head_description) ? $menuHead->menu_head_description : "" ?>                         
        

    </textarea>

</div>

<div class="form-group col-md-10 margin-bottom-40">
    &nbsp;
    <input type="hidden" class="form-control" name="menu_group_head_id" id="menu_group_head_id" value="<?php echo!empty($menuHead->menu_group_head_id) ? $menuHead->menu_group_head_id : "" ?>">
</div>
<div class="col-md-2 margin-bottom-40">
    <button type="submit" class="btn btn-success f-right margin-right-3" >Submit</button>

</div>


<script type="text/javascript">

    $(document).ready(function () {
//        $("#date_from").datepicker({
//            format: "yyyy-m-d",
//            startDate: new Date(),
//            orientation: "top auto",
//            autoclose: true
//        });
//        $("#date_to").datepicker({
//            format: "yyyy-m-d",
//            startDate: "+1d",
//            orientation: "top auto",
//            autoclose: true
//        });

        $('.summernote').summernote({
            height: 50
        });


        $("#form").validate({
            rules: {
                scheme_title: {
                    required: true,
//                    email: true
                },
                date_from: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                date_to: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                description: {
                    required: true,
                }
            }
        });


    });

</script>