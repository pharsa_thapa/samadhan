<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Default Menu Head</label>
    <br>
    <span><?php echo!empty($record->default_menu_head) ? $record->default_menu_head : "" ?></span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Effective Menu Head</label>
    <br>
    <span><?php echo!empty($record->menu_head) ? $record->menu_head : "" ?></span>
</div>

<div class="form-group col-md-12 control-group">

    <label for="exampleInputEmail1">Description</label>
    <br>
    <span>                               
        <?php if (!empty($record->menu_head_description)) { ?>
            <?php echo $record->menu_head_description; ?>
        <?php } ?>
    </span>
</div>
