



<div class="table-responsive">
    <table id="example" class="display table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="col-md-4">Default Menu Head</th>
                <th class="col-md-4">Effective Menu Head</th>
                <!--<th>Description</th>-->
                <th class="col-md-4">Actions</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Default Menu Head</th>
                <th>Effective Menu Head</th>
                <!--<th>Description</th>-->
                <th>Actions</th>
            </tr>
        </tfoot>
        <tbody>

            <?php foreach ($records as $record) : ?>
                <tr>
                    <td><?php echo $record->default_menu_head; ?></td>
                    <td><?php echo $record->menu_head; ?></td>
                    <!--<td><?php // echo $record->menu_head_description; ?></td>-->
                    <td> 
                        <a href="<?php echo base_url("rbac/detailMenuHead/$record->menu_group_head_id"); ?>">
                            Details
                        </a>
                        |
                        <a href="<?php echo base_url("rbac/editMenuHead/$record->menu_group_head_id"); ?>">
                            Edit
                        </a> 
                        |
                        <a href="<?php echo base_url("rbac/deleteMenuHead/$record->menu_group_head_id"); ?>">
                            Delete
                        </a> 
                        |
                        <a href="<?php echo base_url("rbac/viewACLItems/$record->menu_group_head_id"); ?>">
                            View ACL List
                        </a> 
                        |
                        <a href="<?php echo base_url("rbac/addACLItem/$record->menu_group_head_id"); ?>">
                            Add ACL Item
                        </a> 

                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>  
</div>