
<div class="form-group col-md-6">
    <label for="exampleInputEmail1">ACL Label</label>
    <input type="text" class="form-control" placeholder="Menu Label To be displayed" name="acl_list_name" id="acl_name_0" value="<?php echo!empty($acl->acl_list_name) ? $acl->acl_list_name : "" ?>" required="required">
</div>
<div class="form-group col-md-6">
    <label for="exampleInputEmail1">ACL URL</label>
    <input type="text" class="form-control" placeholder="URL for the ACL"  name="acl_list_url" value="<?php echo!empty($acl->acl_list_url) ? $acl->acl_list_url : "" ?>" required="required">
</div>
<div class="form-group col-md-6">
    <label>
        Add In Menu List
    </label>
    <select class="form-control" name="include_in_menu">
        <option value="1" <?php echo ($acl->acl_list_name == 1) ? "selected" : "" ?>>Include In Menu List</option>
        <option value="0" <?php echo ($acl->acl_list_name == 0) ? "selected" : "" ?>>Do not Include In Menu List</option>
    </select>
</div>
<div class="form-group col-md-6">

    <input type="hidden" class="form-control"  name="acl_list_id" value="<?php echo $acl->acl_list_id; ?>" >
</div>



<div class="form-group col-md-9">
    &nbsp;
</div>
<div class="col-md-3">

    <button type="submit" class="btn btn-success f-right">Submit</button>

</div>

<script type="text/javascript" src="<?php echo base_url("assets/js/custom/clone.js"); ?>"></script>
<script type="text/javascript">

    $(document).ready(function () {

    });

</script>