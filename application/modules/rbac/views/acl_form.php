<div id="clone-wrapper" class="col-md-12">
    <div id="clone-0" class="row clone-content">
        <div class="form-group col-md-6">
            <label for="exampleInputEmail1">ACL Label</label>
            <input type="text" class="form-control" placeholder="Menu Label To be displayed" name="acl_list_name_0" id="acl_name_0" value="<?php echo!empty($scheme->scheme_name) ? $scheme->scheme_name : "" ?>" required="required">
        </div>
        <div class="form-group col-md-6">
            <label for="exampleInputEmail1">ACL URL</label>
            <input type="text" class="form-control" placeholder="URL for the ACL"  name="acl_list_url_0" value="<?php echo!empty($scheme->effective_from) ? $scheme->effective_from : "" ?>" required="required">
        </div>
        <div class="form-group col-md-6">
            <label>
                Add In Menu List
            </label>
            <select class="form-control" name="include_in_menu_0">
                <option value="1">Include In Menu List</option>
                <option value="0">Do not Include In Menu List</option>
            </select>
        </div>
        <div class="form-group col-md-6">

            <input type="hidden" class="form-control"  name="menu_group_head_0" value="<?php echo $menu_id; ?>" required="required">
        </div>
        <div class="form-group col-md-6">

            <input type="hidden" class="form-control"  name="menu_group_head_0" value="<?php echo $menu_id; ?>" required="required">
        </div>

    </div>
</div>
<div class="form-group col-md-8">
    &nbsp;
</div>
<div class="col-md-4">

    <button type="submit" class="btn btn-success f-right">Submit</button>
    <button type="button" class="btn btn-primary f-right margin-right-3" id="clone-button" data-action="clone" >Add More</button>
    <button type="button" class="btn btn-danger f-right margin-right-3 disabled" id="remove-last-clone" data-action="clone" >Remove Last</button>

</div>

<script type="text/javascript" src="<?php echo base_url("assets/js/custom/clone.js"); ?>"></script>
