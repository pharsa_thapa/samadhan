<?php

class Rbac_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getPermissions($groupId, $menuId) {
        $query = "SELECT *, (SELECT count(*) FROM permissions WHERE group_id= $groupId AND acl_id= al.acl_list_id) as count "
                . " FROM acl_list al "
                . "WHERE menu_group_head = $menuId ";

        return $this->db->query($query)->result();
    }

}
