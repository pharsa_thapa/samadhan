<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Distributors extends Frontend_Controller {

    public $validation_rules = array(
        'distributor_name' => array(
            'field' => 'distributor_name',
            'label' => "Distributor's Name",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'registered_date' => array(
            'field' => 'registered_date',
            'label' => 'Registered Date',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'distributor_phone' => array(
            'field' => 'distributor_phone',
            'label' => 'Distributor\'s Name',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'district' => array(
            'field' => 'district',
            'label' => 'District',
            'rules' => 'trim|required'
        ),
        'gvs' => array(
            'field' => 'gvs',
            'label' => 'GVS/Municipality',
            'rules' => 'trim|required'
        ),
        'ward_no' => array(
            'field' => 'ward_no',
            'label' => 'Ward No',
            'rules' => 'trim|required'
        ),
        'address' => array(
            'field' => 'address',
            'label' => 'Address',
            'rules' => 'trim|required'
        ),
    );

    public function __construct() {
        parent::__construct();
        $this->template->set_breadcrumb('Home', base_url());
        $this->template->set_breadcrumb("Distributors", site_url('distributors'));
    }

    public function index() {
        $this->data['records'] = $this->db->join("districts", 'districts.district_id= distributors.district')
                ->join("gvs", "gvs.gvs_id=distributors.gvs")
                ->get("distributors")
                ->result();

        $this->template->set_breadcrumb("Distributors' List", site_url('distributors'));
        $this->data['page_head'] = "All Distributor's List";

        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('list_view', $this->data);
    }

    public function add() {
        $this->data['districts'] = $this->db->get("districts")->result();
        $this->data['gvss'] = $this->db->get("gvs")->result();
        $this->data['page_head'] = "Add Distributor's Details";

        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validation_rules);
        if ($this->form_validation->run() == TRUE) {

            // preparing for insertion
            foreach ($this->validation_rules as $k => $v) {
                $insert_data [$k] = $this->input->post($k);
            }
            if ($this->db->insert("distributors", $insert_data)) {
                $this->session->set_flashdata('success', 'New Distributor\'s Details Added Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to add the Distribtor\'s details.');
            }
            redirect('distributors/add/', 'refresh');
        } else {
            $distributor = new stdClass();
            // Go through all the known fields and get the post values
            foreach ($this->validation_rules as $key => $field) {
                $distributor->$field['field'] = set_value($field['field']);
            }
            $this->data["distributor"] = $distributor;
        }

        $this->template->set_breadcrumb("Add Distributor", site_url('distributors/add'));
        $this->template->title('Title')
                ->set_layout('form_layout')
                ->build('form_view', $this->data);
    }

    public function edit($id = NULL) {
        $this->data['districts'] = $this->db->get("districts")->result();
        $this->data['gvss'] = $this->db->get("gvs")->result();

        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validation_rules);

        if ($this->form_validation->run() == TRUE) {
            // preparing for insertion
            foreach ($this->validation_rules as $k => $v) {
                $update_data [$k] = $this->input->post($k);
            }
            if ($this->db->update("distributors", $update_data, array("distributor_id" => $this->input->post("distributor_id")))) {
                $this->session->set_flashdata('success', 'Distributor\'s Details updated Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to update the Distributor\'s details.');
            }

            redirect('distributors/index', 'refresh');
        } else {
            if ($id == NULL)
                redirect(base_url("distributors"), "refresh");

            $this->data["distributor"] = $this->db->get_where("distributors", array("distributor_id" => $id))->row();

            if (empty($this->data["distributor"]))
                redirect(base_url("distributors"), "refresh");

            $this->template->set_breadcrumb("Edit Distributor", site_url("distributors/edit/$id"));
            $this->data['page_head'] = "Edit Distributor's Details -- {$this->data["distributor"]->distributor_name}";

            $this->template->title('Title')
                    ->set_layout('form_layout')
                    ->build('form_view', $this->data);
        }
    }

    /*
     * Delete record
     */

    public function delete($id = NULL) {
        if ($id == NULL)
            redirect(base_url("distributors"), "refresh");

        $this->data["distributor"] = $this->db->get_where("distributors", array("distributor_id" => $id))->row();

        if (empty($this->data["distributor"]))
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to delete the distributor details.');
        else {
            if ($this->db->delete("distributors", array("distributor_id" => $id))) {
                $this->session->set_flashdata('success', 'Distributor\'s Details deleted Successfully.');
            }
        }
        redirect(base_url("distributors"), "refresh");
    }

    /*
      detail of the records
      Report
     */

    public function detail($id) {
        if ($id == NULL)
            redirect(base_url("distributors"), "refresh");

        $this->data['distributor'] = $this->db->join("districts", 'districts.district_id= distributors.district')
                        ->join("gvs", "gvs.gvs_id=distributors.gvs")->get_where("distributors", array("distributor_id" => $id))->row();



        if (empty($this->data['distributor'])) {
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to retrieve the distributor\'s Details.');
            redirect(base_url("distributors"), "redirect");
        }

        if (!empty($this->data['distributor'])) {

            $this->template->set_breadcrumb("Distributor Details", site_url("distributors/detail/$id"));
            $this->data['page_head'] = "Distributor's Details -- {$this->data['distributor']->distributor_name}";
            $this->template->title('Title')
                    ->set_layout('detail_view_layout')
                    ->build('view', $this->data);
        }
    }

    /*
     * single status change
     */

    public function changestatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->uri->segment(4);

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

    /*
     * for bulk status change
     */

    public function bulkstatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->input->post('id');

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

    /**
     * retrieving gvs/municipality under a district
     */
    public function changeDistrict() {
        if (!$this->input->is_ajax_request())
            redirect(base_url(), 'refresh');

        $records = $this->db->where("district_id", $this->input->post("district_id"))->get("gvs")->result();

        if (empty($records)) {
            $response['result'] = 0;
            echo json_encode($response);
        } else {
            $response['result'] = 1;
            $html = '';
            foreach ($records as $gvs) {
                $html.= '<option value="' . $gvs->gvs_id . '">' . $gvs->gvs_name . '</option>';
            }
            $response['response'] = $html;

            echo json_encode($response);
        }
    }

    public function changeGvs() {
        if (!$this->input->is_ajax_request())
            redirect(base_url(), 'refresh');

        $records = $this->db->where("gvs_id", $this->input->post("gvs_id"))->get("gvs")->row();

        if (empty($records)) {
            $response['result'] = 0;
            echo json_encode($response);
        } else {
            $response['result'] = 1;

            $district = $this->db->get_where("districts", array("district_id" => $records->district_id))->row();

            if (empty($district)) {
                $response['result'] = 0;
            }

            $response['response'] = $district->district_id;

            echo json_encode($response);
        }
    }

}
