<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">



                    <div class="table-responsive">
                        <table id="example" class="display table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Distributor's Name</th>
                                    <th>Distributor's Address</th>
                                    <th>Registered Date</th>
                                    <th>Distributor's Phone</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                <tr>
                                    <th>Distributor's Name</th>
                                    <th>Distributor's Address</th>
                                    <th>Registered Date</th>
                                    <th>Distributor's Phone</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($records as $record) : ?>
                                    <tr>

                                        <td><?php echo $record->distributor_name; ?></td>
                                        <td><?php echo $record->gvs_name . " - " . $record->ward_no . ", " . $record->district_name; ?></td>
                                        <td><?php echo $record->registered_date; ?></td>
                                        <td><?php echo $record->distributor_phone ?></td>
                                        <td>
                                            <a href="<?php echo base_url("distributors/detail/$record->distributor_id"); ?>">
                                                Details
                                            </a>
                                            |
                                            <a href="<?php echo base_url("distributors/edit/$record->distributor_id"); ?>">
                                                Edit
                                            </a> 
                                            | 
                                            <a href="<?php echo base_url("distributors/delete/$record->distributor_id"); ?>" 
                                               onclick="javascript : return confirm('Are you sure, you want to delete this record permanently?');">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>  
                    </div>



                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->