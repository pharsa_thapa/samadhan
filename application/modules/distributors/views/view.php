<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Distributor's Name</label>
    <br>
    <span><?php echo!empty($distributor->distributor_name) ? $distributor->distributor_name : "" ?></span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Distributor's Contact</label>
    <br>
    <span><?php echo!empty($distributor->distributor_phone) ? $distributor->distributor_phone : "" ?></span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Registered Date</label>
    <br>
    <span> <?php echo!empty($distributor->registered_date) ? $distributor->registered_date : "" ?> </span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Address</label>
    <br>
    <span>
        <?php echo $distributor->gvs_name . " - " . $distributor->ward_no . ", " . $distributor->district_name; ?> 
    </span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Other Address</label>
    <br>
    <span><?php echo ($distributor->address) ?></span>
</div>
