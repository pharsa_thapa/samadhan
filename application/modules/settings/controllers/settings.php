<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends Frontend_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("settingsmodel");

        $this->template->set_breadcrumb('Home', base_url());
        $this->template->set_breadcrumb("Schemes", site_url('schemes'));
    }

    public function index() {

        $this->data['page_head'] = "Application Settings";
        $this->data['setting'] = $this->settingsmodel->getSettings();
        $this->template->title('Title')
                ->set_layout('form_layout')
                ->build('settings', $this->data);
    }

    public function update() {
        $status = $this->settingsmodel->updateSettings();
        echo $status;
    }

    /*
     * used for setting up districts and munnicipalities for a zone
     * 
     */

    public function listDistricts() {
        $this->data['page_head'] = "List of All available Districts";
        $this->data['districtsList'] = $this->db->get("districts")->result();
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('district_list', $this->data);
    }

    public function addDistrict() {
        if (!$this->input->is_ajax_request())
            redirect(base_url());

        if ($this->db->insert("districts", array("district_name" => $this->input->post("districtName")))) {
       
            $response['result'] = 1;
            $response['id'] = $this->db->insert_id();
            $response['size'] = sizeof($this->db->get("districts")->result());
        } else {
            $response['result'] = 0;
        }
        echo json_encode($response);
    }

    public function deleteDistrict() {
        if (!$this->input->is_ajax_request())
            redirect(base_url());
        if ($this->db->delete("districts", array("district_id" => $this->input->post("district")))) {
            $this->db->delete("gvs", array("district_id" => $this->input->post("district")));
            $response["response"] = 1;
        } else {
            $response["response"] = 0;
        }
        echo json_encode($response);
    }

    public function updateDistrict() {
        if (!$this->input->is_ajax_request())
            redirect(base_url());
        if ($this->db->update("districts", array("district_name" => $this->input->post("new_name")), array("district_id" => $this->input->post("item")))) {
            $response['result'] = 1;
        } else {
            $response['result'] = 0;
        }
        echo json_encode($response);
    }

    public function listVdcs($districtId) {
        if ($districtId == null)
            redirect(base_url());
        $district = $this->db->get_where("districts", array("district_id" => $districtId))->row();
        if (($district == null))
            redirect(base_url());
        if ($this->input->post()) {
            $vdcs = $this->getVdcs($this->input->post("vdcs"));
            $insertData = array();
            foreach ($vdcs as $vdc) {
                $insertData[] = array("district_id" => $districtId, "gvs_name" => $vdc);
            }
            $this->db->insert_batch("gvs", $insertData);
            redirect(base_url("settings/listVdcs/$districtId"));
        }


        $this->data['vdcs'] = $this->db->get_where("gvs", array("district_id" => $districtId))->result();

        $this->data['page_head'] = "List of All available Vdcs in " . $district->district_name;
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('vdc_list', $this->data);
    }

    private function getVdcs($commaSeparated) {
        $exploded = explode(",", $commaSeparated);
        $arr = array();

        foreach ($exploded as $item) {
            if (trim($item) !== "") {
                $arr[] = trim($item);
            }
        }
        return $arr;
    }

    public function deleteGVS() {
        if (!$this->input->is_ajax_request())
            redirect(base_url());
        if ($this->db->delete("gvs", array("gvs_id" => $this->input->post("gvs")))) {
            $response["response"] = 1;
        } else {
            $response["response"] = 0;
        }
        echo json_encode($response);
    }

}
