<style>
    .des{
        font-size: 9px;
        font-weight: lighter;
    }
</style>
<script type="text/javascript">
    function updateSetting(fldid, msgid)
    {
        var fldvalue = jQuery('#' + fldid).val();
        //alert(fldvalue);
        jQuery.ajax({
            type: 'POST',
            url: '<?php echo base_url() . 'index.php/settings/update'; ?>',
            data: {field: fldid, value: fldvalue},
            /*
             dataType: function(updstat){
             alert(updstat);
             },
             */
            success: function (status) {
                //alert(status);
                if (status == 1)
                {
                    jQuery('<div class="success-left"></div><div class="success-inner">Successfully Updated.</div>').appendTo('#' + msgid).fadeOut(5000, function () {
                        jQuery(this).remove();
                    });

                }
                else
                {
                    //jQuery('#'+msgid).html('Update Failed');
                    jQuery('<div class="error-left"></div><div class="error-inner">Update Failed.</div>').appendTo('#' + msgid).fadeOut(5000, function () {
                        jQuery(this).remove();
                    });

                }
            }

        });
    }
</script>



<div class="col-md-12">
    <h3>Application Settings</h3>

    <?php
    foreach ($setting as $set):   ?>
        <div class="form-group col-md-6">
            <label for="exampleInputEmail1"><?php echo $set->settings; ?></label>
                    <span class="des"><?php echo $set->description; ?></span>

            <input type="text" class="form-control" name="<?php echo $set->settings_name; ?>" type="text" size="39" id="<?php echo $set->settings_name; ?>" value="<?php echo $set->settings_value; ?>" onblur="updateSetting('<?php echo $set->settings_name; ?>', <?php echo $set->settings_id; ?>)" />
            <span id="<?php echo $set->settings_id; ?>" style="padding-left:5px"></span>
        </div>
        <?php
    endforeach;
    ?>



</div>

