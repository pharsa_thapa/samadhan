<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">

            <div class="col-md-4 h4" > Add a New District</div>
        </div>
        <div class="col-md-12">
            <div class="col-md-6"></div>
            <div class="col-md-4"> <input type="text" name="district_name" id="district-name" class="form-control">
            </div>
            <div class="col-md-2"><button class="btn btn-success f-right" id="add-button">Add Now</button></div>
        </div>
        <div class="col-md-12" style="border-top: 1px solid #ccc; margin: 20px !important;">
            <div class="col-md-12">
                <div class="panel panel-white">

                    <div class="panel-body">


                        <div class="table-responsive">
                            <table id="example" class="display table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>District Name </th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>SN</th>
                                        <th>District Name </th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php
                                    $counter = 1;
                                    foreach ($districtsList as $record):
                                        ?>
                                        <tr id="record-<?php echo $record->district_id; ?>">
                                            <td><?php echo $counter; ?></td>
                                            <td><?php echo $record->district_name ?></td>
                                            <td>
                                                <button type="button" class="btn btn-danger  delete-button-<?php echo $record->district_id ?> delete-button" data-custom="<?php echo $record->district_id ?>" > 
                                                    Delete Record
                                                </button>

                                                <button type="button" class="btn btn-info edit-button-<?php echo $record->district_id ?> edit-button" data-toggle="modal" data-target=".receipt-add-form" data-custom="<?php echo $record->district_id ?>"> 
                                                    Edit Record
                                                </button>
                                                <a  class="btn btn-info" href="<?php echo base_url("settings/listVdcs/$record->district_id") ?>"> 
                                                    View all VDCs
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                        $counter++;
                                    endforeach;
                                    ?>

                                </tbody>
                            </table>  
                        </div>



                    </div>
                </div>
            </div>
        </div><!-- Row -->
    </div><!-- Main Wrapper -->




    <div style="display: none;" class="modal fade receipt-add-form" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit District Name : <span id="district-head"></span></h4>
                </div>
                <div class="modal-body" style="height: 130px !important; ">
                    <div class="col-md-12">

                        <div class="form-group  col-md-12">
                            <label for="exampleInputName"> Enter New Name </label>

                        </div>
                        <div class="form-group  col-md-12">

                            <input type="text" class="form-control district-name" placeholder="District Name"  name="district_name" value="" data-custom="">
                        </div>

                        <div class="form-group col-md-12 errorBox hidden text-danger">Please provide valid name !</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <div class="form-group  col-md-12">
                            <button type="button" class="btn btn-success" id="save-changes" data-target=''>Save Changes</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <script type="text/javascript">
        $(document).ready(function (fn) {
            $("#add-button").click(function (fn) {
                districtName = $("#district-name").val();
                if (districtName === "") {
                    alert("District Name cannot be empty!");
                }
                else {

                    var districtAdd = $.ajax({
                        type: "post",
                        url: "<?php echo base_url("settings/addDistrict"); ?>", //Please cahnge this url on deployment
                        data: {"districtName": districtName}

                    });
                    districtAdd.done(function (evt) {
                        console.log(evt);
                        data = $.parseJSON(evt);
                        if (data.result == 1) {
                            window.location.href = ("<?php echo base_url("settings/listDistricts"); ?>");
                        } else {
                            alert("Sorry!\n\n" + "We are unable to add district ! \n\n")
                        }

                    });
                }
            });
            $(".delete-button").click(function (fn) {
                confirmed = confirm("Are you sure, you want to delete this record? \n\n");
                val = $(this).attr("data-custom");

                if (confirmed == true) {

                    var deleteDistrict = $.ajax({
                        type: "post",
                        url: "<?php echo base_url("settings/deleteDistrict"); ?>", //Please cahnge this url on deployment
                        data: {"district": val}

                    });
                    deleteDistrict.done(function (evt) {
                        data = $.parseJSON(evt);
                        if (data.response == 1) {
                            $("#record-" + val).remove();
                        } else {
                            alert("Sorry!!\n \n Unable to delete the record.\n");
                        }
                    });
                }
            });
            $(".edit-button").click(function (fnc) {
                clone = $(this).attr("data-custom");
                tds = $("#record-" + clone).find("td");
                $("#save-receipt").attr("data-target", clone);
                $("#district-head").html(tds[1].innerHTML);
                $(".district-name").val(tds[1].innerHTML);
                $(".district-name").attr("data-custom", clone);
            });
            $("#save-changes").click(function (ff) {
                newVal = $(".district-name").val();
                if (newVal === "") {
                    $(".errorBox").removeClass("hidden");
                }
                else {
                    action = confirm("Are you sure ? \n \n" + "You entered : " + newVal + "\n\n");
                    if (action) {
                        item = $(".district-name").attr("data-custom");

                        var updateName = $.ajax({
                            type: "post",
                            url: "<?php echo base_url("settings/updateDistrict"); ?>", //Please cahnge this url on deployment
                            data: {"item": item, "new_name": newVal}

                        });
                        updateName.done(function (evt) {
                            data = $.parseJSON(evt);
                            if (data.result == 1) {
                                tds = $("#record-" + clone).find("td");
                                $(tds[1]).html("").html(newVal);
                                $("#button-close").trigger("click");
                                $(".close").trigger("click");
                            } else {
                                alert("Sorry!\n\n" + "We are unable to add make changes! \n\n")
                            }

                        });
                    }
                }
            });
        });


    </script>   