<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">

            <div class="col-md-4 h4" > Add a VDCS</div>
        </div>
        <div class="col-md-12">
            <div class="col-md-6 text-right h5"> Please, Ensure you used comma ( , ) separated values!</div>
            <form method="post">
                <div class="col-md-4"> <input type="text" name="vdcs" class="form-control">
                </div>
                <div class="col-md-2"><button class="btn btn-success f-right" type="submit">Add VDCS</button></div>
            </form>
        </div>
        <div class="col-md-12" style="border-top: 1px solid #ccc; margin: 20px !important;">
            <div class="col-md-12">
                <div class="panel panel-white">

                    <div class="panel-body">


                        <div class="table-responsive">
                            <table id="example" class="display table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>District Name </th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>SN</th>
                                        <th>District Name </th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php
                                    $counter = 1;
                                    foreach ($vdcs as $record):
                                        ?>
                                        <tr id="record-<?php echo $record->gvs_id; ?>">
                                            <td><?php echo $counter; ?></td>
                                            <td><?php echo $record->gvs_name ?></td>
                                            <td>
                                                <button type="button" class="btn btn-danger  delete-button-<?php echo $record->gvs_id ?> delete-button" data-custom="<?php echo $record->gvs_id ?>" > 
                                                    Delete Record
                                                </button>
                                            </td>
                                        </tr>
                                        <?php
                                        $counter++;
                                    endforeach;
                                    ?>

                                </tbody>
                            </table>  
                        </div>



                    </div>
                </div>
            </div>
        </div><!-- Row -->
    </div><!-- Main Wrapper -->

    <script type="text/javascript">
        $(document).ready(function (fn) {
            $(".delete-button").click(function (fn) {
                confirmed = confirm("Are you sure, you want to delete this record? \n\n");
                val = $(this).attr("data-custom");

                if (confirmed == true) {

                    var deleteDistrict = $.ajax({
                        type: "post",
                        url: "<?php echo base_url("settings/deleteGVS"); ?>", //Please cahnge this url on deployment
                        data: {"gvs": val}

                    });
                    deleteDistrict.done(function (evt) {
                        data = $.parseJSON(evt);
                        if (data.response == 1) {
                            $("#record-" + val).remove();
                        } else {
                            alert("Sorry!!\n \n Unable to delete the record.\n");
                        }
                    });
                }
            });

        });
    </script>