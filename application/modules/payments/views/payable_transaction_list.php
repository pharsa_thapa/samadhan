<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Particular</th>
                                    <th>Transaction Date</th>
                                    <th>Effective P/Amount</th>
                                    <th>Remaining Amount</th>
                                    <th>Full Payment</th>
                                    <th>Allocated Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($records as $record) : ?>
                                    <tr>

                                        <td><?php echo "Particular Name"; ?></td>
                                        <td><?php echo $record->transaction_date; ?></td>
                                        <td><?php echo $record->effective_payable_amount; ?></td>
                                        <td class="td-remaining-amount" id="td-remaining-amount-<?php echo $record->transaction_id; ?>"><?php echo $record->remaining_amount ?></td>
                                        <td> 

                                            <div class="col-md-1">
                                                <input type="checkbox" class="full_payment_check"  id="full-payment-<?php echo $record->transaction_id ?>"> 
                                            </div>

                                        </td>
                                        <td class="col-md-2">
                                            <input type="number" class="amount-field form-control col-md-1" name="items[<?php echo $record->transaction_id; ?>]" id="amount-<?php echo $record->transaction_id; ?>" > 
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <tr style="font-weight:bolder;">
                                    <td colspan="4"></td>
                                    <td >Total</td>
                                    <td><span>Rs.</span><span id="total_amount">0</span></td>
                                </tr>
                                <tr>
                                    <td colspan="5">&nbsp;</td>
                                    <td><input type="submit" class="btn btn-success f-right" id="submit-form"  value="Pay Now"></button></td>
                                </tr>
                            </tbody>
                        </table> 
                    </div>

                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->

<script type="text/javascript">
    $(document).ready(function (fn) {
//add action attribute in the form
        $(document).find("form").attr("action", "<?php echo base_url("payments/processPayment") ?>");

        //track event on check box under applicable table head
        $(".full_payment_check").click(function (fn) {
            //find out the id of the clicked check box
            id = $(this).attr("id");
            splitId = id.split("-");
            recordId = splitId[splitId.length - 1];

            //check if the box is checked or not
            if ($(this).is(':checked')) {
                //highlight the table which contains the check box
                amount = $(this).closest("tr").find(".td-remaining-amount").html();
                $(this).closest("tr").find(".amount-field").val(amount).prop("disabled", "disabled");

                $(this).closest("tr").removeClass("text-danger");
            }
            else
            {
                $(this).closest("tr").find(".amount-field").val(0).prop("disabled", false);
            }
        });

        //trigger event to calculate the total amount
        $(document).click(function (fn) {
            calculateAndDisplay();
            checkAllValidity();
        });

        $(".form-control").focusout(function (fn) {
            calculateAndDisplay();
        });
        $("input[id^='amount-']").keyup(function (fn) {
            id = $(this).prop("id");
            splittedId = id.split("-");
            checkValidEntry(splittedId[splittedId.length - 1], $(this).val());
            calculateAndDisplay();
            checkAllValidity();

        });

    });

    /*
     * calculates total amount of the salary inputs
     */

    function calculateAndDisplay() {
        val = 0;
        $("input[id^='amount-']").each(function () {
            thisElem = $(this);
            //if input is invalid or not a number set 0 for the salary head
            val = val + (isNaN(parseInt(thisElem.val())) ? 0 : parseInt(thisElem.val()));
        });
        $("#total_amount").html(val);
    }
    function checkValidEntry(itemId, value) {
        amount = parseInt($("#td-remaining-amount-" + itemId).html());
        if (parseInt(value) > amount) {
            showError(itemId);
            disableSubmit();
        } else {
            autoCorrect(itemId);
        }
    }

    function showError(itemId) {
        $("#td-remaining-amount-" + itemId).parent("tr").addClass("text-danger");
    }
    function autoCorrect(itemId) {
        $("#td-remaining-amount-" + itemId).parent("tr").removeClass("text-danger");
    }


    function checkAllValidity() {
        valid = true;
        $(document).find("tr").each(function () {
            valid = ($(this).hasClass("text-danger")) ? false : true;
        });

        if (valid) {
            enableSubmit();
        } else {
            disableSubmit();
        }
    }

    function enableSubmit() {
        $("#submit-form").prop("disabled", false);
    }

    function disableSubmit() {
        $("#submit-form").attr("disabled", "disabled");
    }
</script>