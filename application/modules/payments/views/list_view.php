<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">

                    <div class="table-responsive">
                        <table id="example" class="display table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Sponsor's Name</th>
                                    <th>Sponsor's Address</th>
                                    <th>Registered Date</th>
                                    <th>Sponsor's Phone</th>
                                    <th>VAT/PAN</th>
                                    <th class="center">Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                <tr>
                                    <th>Sponsor's Name</th>
                                    <th>Sponsor's Address</th>
                                    <th>Registered Date</th>
                                    <th>Sponsor's Phone</th>
                                    <th>VAT/PAN</th>
                                    <th class="center">Actions</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($records as $record) : ?>
                                    <tr>

                                        <td><?php echo $record->sponsor_name; ?></td>
                                        <td><?php echo $record->address; ?></td>
                                        <td><?php echo $record->registered_date; ?></td>
                                        <td><?php echo $record->contact ?></td>
                                        <td><?php echo $record->sponsor_vat ?></td>
                                        <td>
                                            <?php if ($record->remaining_total > 0) { ?>
                                                <a href="<?php echo base_url("payments/proceedPayment/$record->sponsor_id"); ?>">
                                                    Pay 
                                                </a> 
                                                |
                                            <?php } else { ?>
                                                <a href="#">
                                                    Account Cleared 
                                                </a> 
                                                | 
                                            <?php } ?>
                                            <a href="<?php echo base_url("payments/accountDetail/$record->sponsor_id"); ?>" >
                                                Account Details
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table> 
                    </div>

                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->