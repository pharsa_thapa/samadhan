

<div class="col-lg-10 col-lg-offset-1 col-md-10" id="printContainer">

    <div class="text-center">
        <h2>Samadhan Media Pvt Ltd</h2>
        <h3>Nayabazar, Pokhara</h3>
        <h4>COMPANY VAT NO : 239270267</h4>
        <h5>Phone No: 061-538-977, Fax: 39283923</h5>
    </div>

    <hr/>

    <div class="text-center">
        <h2>Tax Invoice</h2>
    </div>                            

    <div class="col-lg-3 col-md-6 text-left ">
        <div class="">
            <h5>ISSUED DATE : <?php ECHO @$createdDate; ?></h5>
            <h5>BILL NO : <?php echo @$billNumber; ?></h5>
            <h5>MR/MRS : <?php echo @$customerName; ?></h5>
            <h5>ADDRESS : <?php echo @$customerAddress; ?></h5>
            <?php if (@$customerVat != '') { ?>
            <h5>CUSTOMER VAT NO : <?php echo @$customerVat; ?></h5>
            <?php } ?>
        </div>
    </div>
    <!--
        <div class="col-lg-3 col-md-6 text-right pull-right col-lg-offset-6">
            <div class="">
    
    
            </div>
        </div>-->

    <div class="panel panel-white">
        <div class="panel-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>SN</th>
                        <th>PARTICULARS</th>
                        <th>QUANTITY</th>
                        <th>RATE</th>
                        <th>AMOUNT</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    foreach ($detail as $det) :

                    if($det->particular_type == 1){
                    ?>

                    <tr>
                        <th scope="row">1</th>
                        <td><?php echo $det->particular; ?></td>
                        <td>1</td>
                        <td><?php echo $rate ?></td>
                        <td><?php echo $rate ?></td>
                    </tr>
                    <?php }else{ ?>
                    
                    <?php if ($det->percent >= 0) { ?>
                    <tr>
                        <td colspan="3"><?php echo $det->particular; ?></td>
                        <td colspan="2"><?php echo $det->percent; ?>%</td>
                    </tr>
                    <?php } ?>
                    
                    
                    <?php } endforeach; ?>
                    
                    <tr style=" font-weight: 700; size: 20px;">
                        <td colspan="3" class="text-right">Grand Total</td>
                        <td colspan="2"><?php echo @$effectiveTotal; ?></td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>

    <div class="col-lg-6 col-md-6 text-left clearfix" style="height: 150px !important;">
        <!--        <div class="panel-heading" style="height: 100% !important;">
                    <p style="color: #ddd;">_________________</p>
                    <h5>Customer Signature</h5>
                </div>-->
    </div>

    <div class="col-lg-6 col-md-6 text-right clearfix" style="height: 150px !important;">
        <div class="panel-heading" style="height: 100% !important;">
            <p style="color: #ddd;">_________________</p>
            <h5>Company Signature</h5>
        </div>
    </div>



</div>

<div class="col-lg-10 col-lg-offset-1 col-md-10">
    <div class="col-md-12 text-default margin-bottom-10">
        <div class="col-md-12 ">

            <span class="col-md-1 btn btn-info f-right" id="printButton">
                Print
            </span>
            <a href="<?php echo base_url("clients") ?>" class="col-md-2 btn btn-success f-right margin-right-3">
                Get Back
            </a>

        </div>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#printButton').on('click', function () {
            $("#printContainer").printThis({
                debug: false,
                importCSS: true,
                printContainer: true,
                loadCSS: "<?php echo base_url("assets/css/print.css") ?>",
                pageTitle: "",
                removeInline: false
            });
        });
    });
</script>