<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payments extends Frontend_Controller {

    public function __construct() {
        parent::__construct();

        $this->template->set_breadcrumb('Home', base_url());
        $this->template->set_breadcrumb("Payment", site_url('payments'));
    }

    public function index() {
        $this->load->model("ad_sponsors/ad_sponsors_model");
        $this->data['records'] = $this->ad_sponsors_model->allSponsors();
        $this->data["page_head"] = "All Ad Sponsor's List";

        $this->template->set_breadcrumb("Ad Sponsors' List ", site_url("payments/index"));
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('list_view', $this->data);
    }

    function proceedPayment($sponsorID = null) {

        $sponsorDetail = $this->db->get_where("ad_sponsors", array("sponsor_id" => $sponsorID))->row();
        empty($sponsorDetail) ? redirect("payments") : '';

        $transactions = $this->db
                ->get_where("transactions", array("client_sponsor_id" => $sponsorDetail->sponsor_id, "reference_type" => '2', "remaining_amount >" => 0))
                ->result();
        $this->data['records'] = $transactions;
        $this->data["page_head"] = ucfirst($sponsorDetail->sponsor_name) . " : All Payble Transactions";

        $this->template->set_breadcrumb("Ad Sponsors' List ", site_url("payments/index"));
        $this->template->set_breadcrumb("Payable transactions of " . $sponsorDetail->sponsor_name, site_url("payments/proceedPayment/" . $sponsorDetail->sponsor_id));

        $this->template->title('Title')
                ->set_layout('form_layout')
                ->build('payable_transaction_list', $this->data);
    }

    function processPayment() {
        echo "<pre>";
        var_dump($this->input->post());
        die;
    }

    //itself a invoicing for this version
    //later may be changed
    public function paymentView($invoice = NULL) {

//        $transactionDetail = $this->db->get_where("transactions", array("transaction_id" => $transaction_id))->row();
//
//        $table = $transactionDetail->reference_type == '1' ? "clients" : "ad_sponsors";
//
//        $customer = $this->db->get_where($table, array($table == "clients" ? "client_id" : "sponsor_id" => $transactionDetail->client_sponsor_id))->row();
//
//        if ($transaction_id == NULL OR empty($transactionDetail)) {
//            redirect("error/notFound");
//        }

        $invoiceDet = $this->db->get_where("invoice", array("invoice_id" => $invoice))->row();

        $this->data["billNumber"] = $invoiceDet->invoice_id;
        $this->data["customerName"] = $invoiceDet->customer_name;
        $this->data["customerAddress"] = $invoiceDet->customer_address;
        $this->data["customerVat"] = $invoiceDet->vat;
        $this->data["createdDate"] = $invoiceDet->created_date;


        $this->data["detail"] = $this->db->get_where("invoice_details", array("invoice_id" => $invoice))->result();

        $payment = $this->db->get_where("payment_logs", array("payment_log_id" => $invoiceDet->payment_log_id))->row();

        $transaction = $this->db->get_where("transactions", array("transaction_id" => $payment->transaction_id))->row();

        $this->data['rate'] = $transaction->payable_amount;

        $this->data["effectiveTotal"] = $invoiceDet->grand_total;

        $this->data["page_head"] = "Invoice";

        $this->template->set_breadcrumb("Payment Invoice", site_url("payments/payment_view/$invoice"));
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('payment_view', $this->data);
    }

    //Escalated for later versions
//    public function processPayment() {
//        
//    }
//
//    public function invoicing() {
//        
//    }


    public function subscriptionTransaction() {
        if (!$this->input->is_ajax_request())
            redirect(base_url());

        $subs = $this->db->get_where("subscriptions", array("subscription_id" => $this->input->post("subscription"), "client_id" => $this->input->post("client")))->row();

        if (!empty($subs) && $subs->receipt_number != "") {
            $response["result"] = 1;
        }
        if (!empty($subs) && $subs->receipt_number == "") {
            $update_data["receipt_number"] = $this->input->post("receipt");
            if ($this->db->update("subscriptions", $update_data, array("client_id" => $this->input->post("client"), "subscription_id" => $this->input->post("subscription")))) {
                //reference id is subscription id
                $referenceId = $this->input->post("subscription");
                $reference_type = "1";
                
                
                //retrieve payable amount
                $subscriptionDetail = $this->db->get_where("subscription_categories", array("subscription_category_id" => $subs->subscription_category_id))->row();

                $payable_amount = $subscriptionDetail->subscription_category_rate;
                
                $transaction = array(
                    "payable_amount" => $payable_amount,
                    'reference_id' => $referenceId,
                    'client_sponsor_id' => $this->input->post("client"),
                    "reference_type" => $reference_type,
//                        "vat" => $vat,
//                        "discount" => $_POST['discount'],
                );

                $particular = $subscriptionDetail->subscription_category_label;

                //insert transaction detail
                $invoice_id = $this->insertTransactionLog($transaction, NULL, NULL, $particular);

                $response["result"] = 1;
            }
            $response["result"] = 1;
        } else {
            $response["result"] = 0;
        }
        echo (json_encode($response));
    }

}
