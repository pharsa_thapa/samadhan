<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AjaxCalls extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->input->is_ajax_request())
            redirect(base_url());
        $this->load->library("ion_auth");
    }

    /**
     * retrieving Designations under a department
     */
    public function changeDepartment() {
        $records = $this->db->where("department_id", $this->input->post("department_id"))->get("designations")->result();

        if (empty($records)) {
            $response['result'] = 0;
            echo json_encode($response);
        } else {
            $response['result'] = 1;
            $html = '';
            foreach ($records as $designation) {
                $html.= '<option value="' . $designation->designation_id . '">' . $designation->designation_full_name . '</option>';
            }
            $response['response'] = $html;

            echo json_encode($response);
        }
    }

    /*
     * choose department 
     */

    public function changeDesignation() {
        $records = $this->db->where("designation_id", $this->input->post("designation_id"))->get("designations")->row();

        if (empty($records)) {
            $response['result'] = 0;
            echo json_encode($response);
        } else {
            $response['result'] = 1;

            $department = $this->db->get_where("departments", array("department_id" => $records->department_id))->row();

            if (empty($department)) {
                $response['result'] = 0;
            }

            $response['response'] = $department->department_id;

            echo json_encode($response);
        }
    }

    /*
     * punch in ajax call for attendance management punch in punch out functionality
     */

    public function punchIn() {
        $response["status"] = 0;

        $response["message"] = "<span class='error danger'>Failed to process your punch in record!</span>";

        $this->load->library("ion_auth");

        $user_id = $this->ion_auth->get_user_id();


        $attendance_data = array(
            "user_id" => $user_id,
            "date" => date("Y-m-d"),
            "check_in" => date("H:i")
        );

        if ($this->db->insert("attendance", $attendance_data)) {
            $response["status"] = 1;
            $response["message"] = "<span class='success'>Thank You!</span>";
        }

        echo json_encode($response);
    }

    /*
     * punch in ajax call for attendance management punch in punch out functionality
     */

    public function punchOut() {
        $response["status"] = 0;
        $response["message"] = "<span class='error danger'>Failed to process your punch out record!</span>";
        $this->load->library("ion_auth");
        $user_id = $this->ion_auth->get_user_id();

        $attendance_data = array("user_id" => $user_id);
        if ($this->db->update("attendance", array("check_out" => date("H:i")), array("user_id" => $user_id, "date" => date("Y-m-d")))) {
            $response["status"] = 1;
            $response["message"] = "<span class='success'>Thank You!</span>";
        }

        echo json_encode($response);
    }

    /*
     * ajax call for buttons to load previous and next months attendance records
     */

    function attendanceReport() {
        $response["next"] = 0;
        $response["prev"] = 0;
        $response['outputDate'] = date("Y-m");
        //if the event is current month
        if ($this->input->post("event") == 'thisMonth') {

            $response["next"] = 0;
            $response["prev"] = $this->previousMonthCount(date('Y-m', strtotime($this->input->post("inputDate")))) >= 1 ? 1 : 0;
            $result = $this->db->like("date", date("Y-m"))
                    ->where("user_id", $this->ion_auth->get_user_id())
                    ->get("attendance")
                    ->result();
        }

        if ($this->input->post("event") == "previousMonth") {
            $date = date('Y-m', strtotime($this->input->post("inputDate") . " -1 month"));
            $result = $this->previousMonthReport($date);
            $response["next"] = $this->nextMonthCount($date) >= 1 ? 1 : 0;
            $response["prev"] = $this->previousMonthCount($date) >= 1 ? 1 : 0;
            $response['outputDate'] = $date;
        }

        if ($this->input->post("event") == "nextMonth") {
            $date = date('Y-m', strtotime($this->input->post("inputDate") . " +1 month"));
            $result = $this->nextMonthReport($date);
            $response["next"] = $this->nextMonthCount($date) >= 1 ? 1 : 0;
            $response["prev"] = $this->previousMonthCount($date) >= 1 ? 1 : 0;
            $response['outputDate'] = $date;
        }

        $response["records"] = json_encode($result);
        echo json_encode($response);
    }

    private function previousMonthReport($curDate) {
//manipulate Date set to previous month
        $prevDate = $curDate;

        return $this->db->like("date", $prevDate)
                        ->where("user_id", $this->ion_auth->get_user_id())
                        ->get("attendance")
                        ->result();
    }

    private function nextMonthReport($curDate) {
//manipulate date set to next month
        $nextDate = $curDate;
        return $this->db->like("date", $nextDate)
                        ->where("user_id", $this->ion_auth->get_user_id())
                        ->get("attendance")
                        ->result();
    }

    private function nextMonthCount($date) {
//find next month 
        $newDate = date('Y-m', strtotime($date . " +1 month"));
        return $this->db->like("date", $newDate)
                        ->where("user_id", $this->ion_auth->get_user_id())
                        ->count_all_results("attendance");
    }

    private function previousMonthCount($date) {
//find previous month
        $newDate = date('Y-m', strtotime($date . " -1 month"));
        return $this->db->like("date", $newDate)
                        ->where("user_id", $this->ion_auth->get_user_id())
                        ->count_all_results("attendance");
    }

    /*
     * ajax call for checkbox to include in menu head list from acl list view
     */

    public function listInMenu() {
        $this->db->update("acl_list", array("include_in_menu" => $this->input->post("status")), array("acl_list_id" => $this->input->post("id")));
    }

    /*
     * ajax calls for permission management for user groups
     */

    public function applyPermission() {
        $action = $this->input->post("action");
        $groupId = $this->input->post("group");
        $aclId = $this->input->post("record");

        $record = $this->db->get_where("permissions", array("acl_id" => $aclId, "group_id" => $groupId))->result();
        if ($action == "add") {

            $this->db->insert("permissions", array("acl_id" => $aclId, "group_id" => $groupId));
        }

        if ($action == 'remove') {
            $this->db->delete("permissions", array("acl_id" => $aclId, "group_id" => $groupId));
        }
    }

    function getDiscount() {
        $discount = 0;
        if ($_POST['id'] == 0) {
            $discount = 0;
        } else {
            $record = $this->db->get_where("schemes", array("scheme_id" => $_POST['id']))->row();
            $discount = $record->discount;
        }
        echo $discount;
    }

}
