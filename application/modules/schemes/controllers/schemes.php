<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Schemes extends Frontend_Controller {

    public $validation_rules = array(
        'scheme_name' => array(
            'field' => 'scheme_name',
            'label' => 'Scheme Title',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'effective_from' => array(
            'field' => 'effective_from',
            'label' => 'Effective From',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'effective_to' => array(
            'field' => 'effective_to',
            'label' => 'Effective To',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required'
        ),
        'discount' => array(
            'field' => 'discount',
            'label' => 'Discount',
            'rules' => 'trim|numeric'
        ),
        'other' => array(
            'field' => 'other',
            'label' => 'Scheme Theme',
            'rules' => 'trim|htmlspecialchars|xss_clean|max_length[100]'
        )
    );

    public function __construct() {
        parent::__construct();

        $this->template->set_breadcrumb('Home', base_url());
        $this->template->set_breadcrumb("Schemes", site_url('schemes'));
                
    }

    public function index() {

        $this->data['records'] = $this->db->get("schemes")->result();

        $this->template->set_breadcrumb("Schemes List", site_url('schemes'));
        
        $this->data['page_head'] = "All Schemes' List";
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('list_view', $this->data);
    }

    public function add() {

        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validation_rules);
        if ($this->form_validation->run() == TRUE) {

            // preparing for insertion
            foreach ($this->validation_rules as $k => $v) {
                $insert_data [$k] = $this->input->post($k);
            }

            if ($this->db->insert("schemes", $insert_data)) {
                $this->session->set_flashdata('success', 'New Scheme Added Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to add the Scheme.');
            }
            redirect('schemes/add/', 'refresh');
        } else {
            $scheme = new stdClass();
            // Go through all the known fields and get the post values
            foreach ($this->validation_rules as $key => $field) {
                $scheme->$field['field'] = set_value($field['field']);
            }
            $this->data["scheme"] = $scheme;
        }
        
        $this->template->set_breadcrumb("Add Scheme", site_url('schemes/add'));
        $this->data['page_head'] = "Add Scheme Details";
        $this->template->title('Title')
                ->set_layout('form_layout')
                ->build('form_view', $this->data);
    }

    public function edit($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validation_rules);

        if ($this->form_validation->run() == TRUE) {
            // preparing for insertion
            foreach ($this->validation_rules as $k => $v) {
                $update_data [$k] = $this->input->post($k);
            }
            if ($this->db->update("schemes", $update_data, array("scheme_id" => $this->input->post("scheme_id")))) {
                $this->session->set_flashdata('success', 'Scheme Details updated Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to update the Scheme details.');
            }
            redirect('schemes/index', 'refresh');
        } else {
            if ($id == NULL)
                redirect(base_url("schemes"), "refresh");

            $this->data["scheme"] = $this->db->get_where("schemes", array("scheme_id" => $id))->row();

            if (empty($this->data["scheme"]))
                redirect(base_url("schemes"), "refresh");
            
            $this->template->set_breadcrumb("Edit Scheme", site_url("schemes/edit/$id"));
                
            $this->data['page_head'] = "Edit Scheme's Details -- {$this->data['scheme']->scheme_name}";
            $this->template->title('Title')
                    ->set_layout('form_layout')
                    ->build('form_view', $this->data);
        }
    }

    /*
     * Delete record
     */

    public function delete($id = NULL) {
        if ($id == NULL)
            redirect(base_url("schemes"), "refresh");

        $this->data["scheme"] = $this->db->get_where("schemes", array("scheme_id" => $id))->row();

        if (empty($this->data["scheme"]))
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to delete the Scheme details.');
        else {
            if ($this->db->delete("schemes", array("scheme_id" => $id))) {
                $this->session->set_flashdata('success', 'Scheme Details deleted Successfully.');
            }
        }
        redirect(base_url("schemes"), "refresh");
    }

    /*
      detail of the records
      Report
     */

    public function detail($id) {
        if ($id == NULL)
            redirect(base_url("schemes"), "refresh");

        $this->data['scheme'] = $this->db->get_where("schemes", array("scheme_id" => $id))->row();

        if (empty($this->data['scheme'])) {
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to retrieve the Scheme Details.');
            redirect(base_url("schemes"), "redirect");
        }

        if (!empty($this->data['scheme'])) {
            
            $this->template->set_breadcrumb("Scheme Details", site_url("schemes/detail/$id"));

            $this->data['page_head'] = "Scheme's Details -- {$this->data['scheme']->scheme_name}";
            $this->template->title('Title')
                    ->set_layout('detail_view_layout')
                    ->build('view', $this->data);
        }
    }

    /*
     * single status change
     */

    public function changestatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->uri->segment(4);

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

    /*
     * for bulk status change
     */

    public function bulkstatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->input->post('id');

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

}
