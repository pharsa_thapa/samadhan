<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Scheme Title</label>
    <br>
    <span><?php echo!empty($scheme->scheme_name) ? $scheme->scheme_name : "" ?></span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Effective From</label>
    <br>
    <span>  <?php echo!empty($scheme->effective_from) ? $scheme->effective_from : "" ?> </span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Effective To</label>
    <br>
    <span><?php echo!empty($scheme->effective_to) ? $scheme->effective_to : "" ?></span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Status</label>
    <br>
    <span><?php echo ($scheme->scheme_status == 1) ? "Active" : "Expired" ?></span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Discount</label>
    <br>
    <span><?php echo ($scheme->discount !=0 ? "NRS ".$scheme->discount : "---")?></span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Discount</label>
    <br>
    <span><?php echo ($scheme->other)?></span>
</div>

<div class="form-group col-md-12 control-group">

    <label for="exampleInputEmail1">Description</label>
    <br>
    <span>                               
        <?php if (!empty($scheme->description)) { ?>
            <?php echo $scheme->description; ?>
        <?php } ?>
    </span>
</div>
