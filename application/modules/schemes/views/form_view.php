<div class="col-md-12">
    <div class="form-group col-md-12">
        <label for="exampleInputEmail1">Scheme Title</label>
        <input type="text" class="form-control" placeholder="Scheme Title" name="scheme_name" id="scheme_title" value="<?php echo!empty($scheme->scheme_name) ? $scheme->scheme_name : "" ?>" required="required">
    </div>
</div>
<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Effective From</label>
        <input type="text" class="form-control" placeholder="Effective From" id="date_from" name="effective_from" value="<?php echo!empty($scheme->effective_from) ? $scheme->effective_from : "" ?>" required="required">
    </div>
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Effective To</label>
        <input type="text" class="form-control datepicker" placeholder="Effective To" id="date_to" name="effective_to" value="<?php echo!empty($scheme->effective_to) ? $scheme->effective_to : "" ?>" required="required">
    </div>
</div>

<div class="col-md-12">
    <div class="col-md-12">
        <h3>Scheme Discounts/Other Details</h3>
    </div>
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Discount Amount</label>
        <input type="number" class="form-control" placeholder="Discount Rate" name="discount" value="<?php echo!empty($scheme->discount) ? $scheme->discount : "" ?>" >
    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Others</label>
        <input type="text" class="form-control" placeholder="Specify Scheme Theme "  name="other" value="<?php echo!empty($scheme->other) ? $scheme->other : "" ?>" >
    </div>

</div>
<div class="col-md-12">
    <div class="col-md-12">
        <h3>Scheme Description</h3>
    </div>
    <div class="form-group col-md-12 control-group">
        <textarea name="description" class="summernote" placeholder="Short Description" id="description" style="display: none" required="required">
                                
            <?php if (!empty($scheme->description)) { ?>
                <?php echo $scheme->description; ?>
            <?php } ?>


        </textarea>

    </div>

</div>
<div class="col-md-12">

    <div class="form-group col-md-10 margin-bottom-40">
        &nbsp;
        <input type="hidden" class="form-control" name="scheme_id" id="scheme_id" value="<?php echo!empty($scheme->scheme_id) ? $scheme->scheme_id : "" ?>">
    </div>
    <div class="col-md-2 margin-bottom-40">
        <button type="submit" class="btn btn-success f-right margin-right-3" >Submit</button>

    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $("#date_from").datepicker({
            format: "yyyy-m-d",
            startDate: new Date(),
            orientation: "top auto",
            autoclose: true
        });
        $("#date_to").datepicker({
            format: "yyyy-m-d",
            startDate: "+1d",
            orientation: "top auto",
            autoclose: true
        });

        $('.summernote').summernote({
            height: 50
        });


        $("#form").validate({
            rules: {
                scheme_title: {
                    required: true,
//                    email: true
                },
                date_from: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                date_to: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                description: {
                    required: true,
                }
            }
        });


    });

</script>