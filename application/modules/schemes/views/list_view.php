



<div class="table-responsive">
    <table id="example" class="display table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Scheme Name</th>
                <th>Effective From</th>
                <th>Effective To</th>
                <th>Discount Amount</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Scheme Name</th>
                <th>Effective From</th>
                <th>Effective To</th>
                <th>Discount Amount</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </tfoot>
        <tbody>

            <?php foreach ($records as $record) : ?>
                <tr>
                    <td><?php echo $record->scheme_name; ?></td>
                    <td><?php echo $record->effective_from; ?></td>
                    <td><?php echo $record->effective_to; ?></td>
                    <td><?php echo ($record->discount !=0 ? "NRS ".$record->discount : "---"); ?></td>
                    <td><?php echo $record->scheme_status == 1 ? "Active" : "Expired"; ?></td>
                    <td> 
                        <a href="<?php echo base_url("schemes/detail/$record->scheme_id"); ?>">
                            Details
                        </a>
                        |
                        <a href="<?php echo base_url("schemes/edit/$record->scheme_id"); ?>">
                            Edit
                        </a> 
                        | 
                        <a href="<?php echo base_url("schemes/delete/$record->scheme_id"); ?>" 
                           onclick="javascript : return confirm('Are you sure, you want to delete this record permanently?');">
                            Delete
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>  
</div>