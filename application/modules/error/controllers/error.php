<?php

Class Error extends Frontend_Controller {

    function __construct() {
        parent::__construct();
        $this->template->set_breadcrumb("Home", site_url(""));
        $this->template->set_breadcrumb("Error", site_url(""));
    }

    public function accessDenied() {
        $this->template->set_breadcrumb("Access Denied", site_url(""));

        $this->data['page_head'] = "Access Denied !!";
        $this->template->title('Title')
                ->set_layout('detail_view_layout')
                ->build('access_denied_view', $this->data);
    }

    public function notFound() {
        $this->template->set_breadcrumb("404 Error", site_url(""));

        $this->data['page_head'] = "Page Not Found!";
        $this->template->title('Title')
                ->set_layout('detail_view_layout')
                ->build('404_view', $this->data);
    }

}
