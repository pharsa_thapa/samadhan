<div class="col-md-12">
    <div class="form-group col-md-12">
        <label for="exampleInputPassword1">Sponsor's Name</label>

        <select class="form-control" name="sponsor_id" id="sponsor_id">
            <?php
            foreach ($sponsors as $sponsor) {
                ?>
                <option value="<?php echo $sponsor->sponsor_id ?>" <?php echo ($gAdvertisement->sponsor_id == $sponsor->sponsor_id) ? "selected='selected'" : "" ?>><?php echo $sponsor->sponsor_name; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Advertisement Title</label>
        <input type="text" class="form-control" placeholder="Advertisement Title" name="advertisement_title" id="advertisement_title" value="<?php echo!empty($gAdvertisement->advertisement_title) ? $gAdvertisement->advertisement_title : "" ?>" required="required">    
    </div>
    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Page </label>
        <select class="form-control" name="page">
            <option value="1" <?php echo $gAdvertisement->page == 1 ? "selected='selected' " : '' ?>>Front Page</option>
            <option value="2" <?php echo $gAdvertisement->page == 2 ? "selected='selected' " : '' ?>>Inner Page</option>
            <option value="3" <?php echo $gAdvertisement->page = 3 ? "selected='selected' " : '' ?>>Back Page</option>
        </select>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group col-md-12">
        <h3>Frequency And Dates</h3>
        <hr>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Effective Dates</label>
        <input type="text" class="form-control datePick" placeholder="Effective Dates" name="effective_dates" id="datePick" value="<?php echo!empty($gAdvertisement->effective_dates) ? $gAdvertisement->effective_dates : "" ?>" required="required">    
    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Total Frequency (In Days)</label>
        <input type="number" readonly="readonly" id="total_frequency" class="form-control" placeholder="Number of days advertisement appear" name="number_of_days" value="<?php echo!empty($gAdvertisement->number_of_days) ? $gAdvertisement->number_of_days : "" ?>" required="required">    
    </div>
</div>


<div class="col-md-12">
    <div class="form-group col-md-12">
        <h3>Advertisement Specifications</h3>
        <hr>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Advertisement Size</label>
        <div class="row">
            <div class="col-md-6">
                <input type="number" class="form-control col-md-6" placeholder="Length" name="size_length" id="size_length" value="<?php echo!empty($gAdvertisement->size) ? $gAdvertisement->size : "0" ?>" required="required">
            </div>
            <div class="col-md-6">
                <input type="number" class="form-control col-md-6" placeholder="Breadth" name="size_breadth" id="size_breadth" value="<?php echo!empty($gAdvertisement->size) ? $gAdvertisement->size : "0" ?>" required="required">
            </div>
        </div>
    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Rate Per Unit Area (Nrs)</label>
        <input type="number" class="form-control" placeholder="Rate Per unit Area" name="rate" id="rate" value="<?php echo!empty($gAdvertisement->rate) ? $gAdvertisement->rate : "0" ?>" required="required">    
    </div>
</div>
<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Amount Per Day (Nrs)</label>
        <input type="number" disabled="disabled" class="form-control" placeholder="Amount" name="per_day_amount" id="amount" value="<?php echo!empty($gAdvertisement->per_day_amount) ? $gAdvertisement->per_day_amount : "0" ?>" required="required">    
    </div>
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Total Amount (Nrs)</label>
        <input type="number" class="form-control" placeholder="Total Amount" name="payable_amount" id="total_amount" value="<?php echo!empty($gAdvertisement->payable_amount) ? $gAdvertisement->payable_amount : "0" ?>" required="required">    
    </div>
</div>
<div class="col-md-12">
    <div class="form-group col-md-12">
        <h3>Discounts</h3>
        <hr>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group  col-md-6">
        <label for="exampleInputName"> Discount amount for the advertisement (Nrs)</label>
        <br>
        <input type="number" class="form-control" placeholder="Discount Amount" id="discount_percentage" name="discount" value="<?php echo!empty($client->discount) ? $client->discount : "0" ?>">
    </div>
    <!--
        <div class="form-group  col-md-6">
            <label for="exampleInputName"> Un-check, if VAT is NOT applicable for this advertisement! (13%)</label>
            <br>
            <input type="checkbox" name="vat" class="form-control checkbox-inline" id="vat" checked="checked">
        </div>-->

</div>


<div class="col-md-12">
    <div class="form-group col-md-10 margin-bottom-40">
        &nbsp;
        <input type="hidden" class="form-control" name="cAdvertisement_id" id="cAdvertisement_id" value="<?php echo!empty($gAdvertisement->cAdvertisement_id) ? $gAdvertisement->cAdvertisement_id : "" ?>">
    </div>
    <div class="col-md-2 margin-bottom-40">
        <button type="submit" class="btn btn-success f-right margin-right-3" >Submit</button>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {
        $('#datePick').multiDatesPicker({
            dateFormat: "yy-mm-dd"
        });

        $('#datePick').focus(function (fn) {
            dates = $("#datePick").val();
            dateArr = dates.split(",");
            $("#total_frequency").val(dateArr.length);
             updateAmounts();
        });

        $("#form").validate({
            rules: {
                sponsor_name: {
                    required: true,
//                    email: true
                },
                contact_number: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                advertisement_title: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                page_no: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                effective_from: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                effective_to: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                payable_amount: {
                    required: true,
                    number: true,
//                    equalTo: '#exampleInputPassword1'
                }
            }
        });
        $("#size_length,#size_breadth,#rate,#amount").keyup(function (fn) {
            updateAmounts();
        });
    });
    function updateAmounts() {
        length = $("#size_length").val();
        breadth = $("#size_breadth").val();
        rate = $("#rate").val();
        days = $("#total_frequency").val();
        $("#amount").val(length * breadth * rate);
        $("#total_amount").val(length * breadth * rate * days);
    }
</script>