<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Sponsor's Name</label>
        <input type="text" class="form-control" placeholder="Sponsor Name" name="sponsor_name" id="sponsor_name" value="<?php echo!empty($cAdvertisement->sponsor_name) ? $cAdvertisement->sponsor_name : "" ?>" required="required">    
    </div>
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Sponsor's Phone</label>
        <input type="text" class="form-control" placeholder="Sponsor's Phone" name="contact_number" id="contact_number" value="<?php echo!empty($cAdvertisement->contact_number) ? $cAdvertisement->contact_number : "" ?>" >    
    </div>
</div>
<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Advertisement Title</label>
        <input type="text" class="form-control" placeholder="Advertisement Title" name="advertisement_title" id="advertisement_title" value="<?php echo!empty($cAdvertisement->advertisement_title) ? $cAdvertisement->advertisement_title : "" ?>" required="required">    
    </div>
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Amount Payable</label>
        <input type="number" class="form-control" placeholder="Amount Payable" name="payable_amount" id="payable_amount" value="<?php echo!empty($cAdvertisement->payable_amount) ? $cAdvertisement->payable_amount : "" ?>" required="required">    
    </div>
</div>

<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Effective From</label>
        <input type="text" class="form-control custom-date-format" placeholder="Effective From" name="effective_from" id="effective_from" value="<?php echo!empty($cAdvertisement->effective_from) ? $cAdvertisement->effective_from : "" ?>" required="required">    
    </div>

    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Effective To</label>
        <input type="text" class="form-control date-picker" placeholder="Effective To" name="effective_to" id="effective_to" value="<?php echo!empty($cAdvertisement->effective_to) ? $cAdvertisement->effective_to : "" ?>" required="required">    
    </div>

</div>

<div class="col-md-12">
    <div class="form-group col-md-6">
        <label for="exampleInputPassword1">Advertisement Collector</label>
        <input type="text" class="form-control" placeholder="Collector Name" name="collector" id="collector" value="<?php echo!empty($cAdvertisement->collector) ? $cAdvertisement->collector : "" ?>" required="required">
    </div>

</div>

<div class="col-md-12">
    <div class="form-group col-md-10 margin-bottom-40">
        &nbsp;
        <input type="hidden" class="form-control" name="cAdvertisement_id" id="cAdvertisement_id" value="<?php echo!empty($cAdvertisement->cAdvertisement_id) ? $cAdvertisement->cAdvertisement_id : "" ?>">
    </div>
    <div class="col-md-2 margin-bottom-40">
        <button type="submit" class="btn btn-success f-right margin-right-3" >Submit</button>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {
        $("#form").validate({
            rules: {
                sponsor_name: {
                    required: true,
//                    email: true
                },      
                contact_number: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                advertisement_title: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                page_no: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                effective_from: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                effective_to: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                payable_amount: {
                    required: true,
                    number: true,
//                    equalTo: '#exampleInputPassword1'
                },
            }
        });

        $('#effective_from').datepicker({
            format: "yyyy-mm-dd",
            orientation: "top auto",
            autoclose: true
        });

        $('#effective_to').datepicker({
            format: "yyyy-mm-dd",
            orientation: "top auto",
            autoclose: true
        });


    });

</script>