<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">



                    <div class="table-responsive">
                        <table id="example" class="display table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Sponsor's Name</th>
                                    <th>Sponsor's Phone</th>
                                    <th>Advertisement Title</th>
                                    <th>Effective From</th>
                                    <th>Effective To</th>

                                    <th>Payable Amount</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Sponsor's Name</th>
                                    <th>Sponsor's Phone</th>
                                    <th>Advertisement Title</th>
                                    <th>Effective From</th>
                                    <th>Effective To</th>

                                    <th>Payable Amount</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($records as $record) : ?>
                                    <tr>

                                        <td><?php echo empty($record->sponsor_name) ? "<span style='color:red;'> Sponsor's Name Not Available!</span>" : $record->sponsor_name; ?></td>
                                        <td><?php echo $record->contact_number; ?></td>
                                        <td><?php echo $record->advertisement_title; ?></td>
                                        <td><?php echo $record->effective_from; ?></td>
                                        <td><?php echo $record->effective_to ?></td>
                                        <td><?php echo $record->payable_amount ?></td>
                                        <td>
                                            <a href="<?php echo base_url("advertisements/deleteClassified/$record->c_advertisement_id"); ?>" 
                                               onclick="javascript : return confirm('Are you sure, you want to delete this record permanently?');">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table> 
                    </div>



                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->