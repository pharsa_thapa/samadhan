<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">



                    <div class="table-responsive">
                        <table id="example" class="display table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Sponsor's Name</th>
                                    <th>Advertisement Title</th>
                                    <th>Effective Dates</th>
                                    <th>Page </th>
                                    <th>Ad Size </th>
                                    <th>Payable Amount</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Sponsor's Name</th>
                                    <th>Advertisement Title</th>
                                    <th>Effective Dates</th>
                                    <th>Page</th>
                                    <th>Ad Size</th>
                                    <th>Payable Amount</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($records as $record) : ?>
                                    <tr>

                                        <td><?php echo empty($record->sponsor_name) ? "<span style='color:red;'> Sponsor's Name Not Available!</span>" : $record->sponsor_name; ?></td>
                                        <td><?php echo $record->advertisement_title; ?></td>
                                        <td><?php echo $record->effective_dates ?></td>
                                        <td><?php echo (($record->page == 1) ? "Front Page" : ($record->page == 2 ? "Mid Page" : "Last Page")); ?></td>
                                        <td><?php echo $record->size_length . " X ". $record->size_breadth ?></td>
                                        <td><?php echo $record->payable_amount ?></td>
                                        <td>
                                            <a href="<?php echo base_url("advertisements/deleteGeneral/$record->g_advertisement_id"); ?>" 
                                               onclick="javascript : return confirm('Are you sure, you want to delete this record permanently?');">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table> 
                    </div>



                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->