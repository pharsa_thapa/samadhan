<?php

class Updates_model extends MY_Model {

    public $_table_name = 'updates';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    public function __construct() {
        parent::__construct();
    }
    public function deleteRecord($id) {
//         echo "this is a function in model";
//          exit();
          
        
       $delornot= $this->db->delete('updates', array('id' => $id));
      return $delornot;   
         
    }
}