<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertisements extends Frontend_Controller {

    protected $u_config = array();
    protected $r_config = array();
    protected $upload_path = '../assets/uploads/updates/';
    //for general advertisement
    public $validation_rules = array(
        'sponsor_id' => array(
            'field' => 'sponsor_id',
            'label' => "Sponsor Name",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'size_length' => array(
            'field' => 'size_length',
            'label' => "Advertisement Length",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'size_breadth' => array(
            'field' => 'size_breadth',
            'label' => "Advertisement Breadth",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'advertisement_title' => array(
            'field' => 'advertisement_title',
            'label' => "Advertisement Title",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'page' => array(
            'field' => 'page',
            'label' => "Page",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'effective_dates' => array(
            'field' => 'effective_dates',
            'label' => "Effective Dates",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'payable_amount' => array(
            'field' => 'payable_amount',
            'label' => 'Amount',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]|number'
        ),
    );

    public function __construct() {
        parent::__construct();
        $this->template->set_breadcrumb('Home', base_url());
        $this->template->set_breadcrumb("Advertisements", site_url("ad_pages_cost"));
    }

    /*
     * general advertisement records
     */

    public function index() {
        $this->data['records'] = $this->db->join("ad_sponsors", "ad_sponsors.sponsor_id = general_advertisements.sponsor_id", "left")
                        ->get("general_advertisements")->result();

        $this->template->set_breadcrumb("All General Advertisements List", site_url("advertisements"));
        $this->data['page_head'] = 'All General Advertisement Records';
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('general_list_view', $this->data);
    }

    /*
     * classified advertisement records
     */

    public function allClassifiedAdvertisements() {
        $this->data['records'] = $this->db->get("classified_advertisements")->result();

        $this->template->set_breadcrumb("All Classified Advertisements List", site_url("advertisements/allClassifiedAdvertisements"));
        $this->data['page_head'] = 'All Classified Advertisement Records';
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('classified_list_view', $this->data);
    }

    public function addGeneralAdvertisement() {
        $this->data['sponsors'] = $this->db->get("ad_sponsors")->result();
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validation_rules);
        if ($this->form_validation->run() == TRUE) {
            $payable_amount = $_POST['payable_amount'];

            //calculate effective payable amount
//            if ($_POST['vat'] != 'on') {
//                $vat = 0;
//            } else {
//                $vat = 13;
//            }
            $effective_payable_amount = $payable_amount - $_POST['discount'];
            //updated that vat is not required in second phase
            $vat = 0;
            $reference_type = "2";

            // preparing for insertion
            foreach ($this->validation_rules as $k => $v) {
                $insert_data [$k] = $this->input->post($k);
            }

            $insert_data['effective_payable_amount'] = $effective_payable_amount;
            if ($this->db->insert("general_advertisements", $insert_data)) {
                $this->session->set_flashdata('success', 'New General Advertisement Details Added Successfully.');
                //reference id is general advertisement id
                $referenceId = $this->db->insert_id();

                $transaction = array(
                    "payable_amount" => $payable_amount,
                    'reference_id' => $referenceId,
                    'client_sponsor_id' => $_POST['sponsor_id'],
                    "reference_type" => $reference_type,
                    "vat" => $vat,
                    "discount" => $_POST['discount'],
                );
                $page = $_POST['page'] == 1 ? "Front" : ($_POST['page'] == 2 ? "Inner" : "Back" );
                $particular = "General Advertisement In " . $page . " Page [" . $_POST['size_length'] . " X " . $_POST['size_breadth'] . "]";
                //insert transaction detail 
                $transaction_id = $this->insertgGENERAL_AD_TransactionLog($transaction, $particular, $effective_payable_amount);
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to add the General advertisement details.');
            }
            redirect('advertisements/addGeneralAdvertisement/', 'refresh');
        } else {
            $gAdvertisement = new stdClass();
            // Go through all the known fields and get the post values
            foreach ($this->validation_rules as $key => $field) {
                $gAdvertisement->$field['field'] = set_value($field['field']);
            }
            $this->data["gAdvertisement"] = $gAdvertisement;
        }

        $this->template->set_breadcrumb("Add General Advertisement Details", site_url("advertisements/addGeneralAdvertisement"));
        $this->data['page_head'] = 'Add General Advertisement Details';
        $this->template->title('Title')
                ->set_layout('general_advertisement_form_layout')
                ->build('general_advertisement_form_view', $this->data);
    }

    public function addClassifiedAdvertisement() {
        $validation_rules = array(
            'sponsor_name' => array(
                'field' => 'sponsor_name',
                'label' => "Sponsor Name",
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'contact_number' => array(
                'field' => 'contact_number',
                'label' => "Sponsor's Number",
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'advertisement_title' => array(
                'field' => 'advertisement_title',
                'label' => "Advertisement Title",
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'effective_from' => array(
                'field' => 'effective_from',
                'label' => "Effective From",
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'effective_to' => array(
                'field' => 'effective_to',
                'label' => "Effective To",
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'payable_amount' => array(
                'field' => 'payable_amount',
                'label' => 'Payable Amount',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]|number'
            ),
        );
        $this->load->library('form_validation');
        $this->form_validation->set_rules($validation_rules);
        if ($this->form_validation->run() == TRUE) {
            //reference type is 3 for classified advertisement
            $reference_type = "3";
            // preparing for insertion
            foreach ($validation_rules as $k => $v) {
                $insert_data [$k] = $this->input->post($k);
            }
            if ($this->db->insert("classified_advertisements", $insert_data)) {
                $this->session->set_flashdata('success', 'New Classified Advertisement Details Added Successfully.');
                //reference id is general advertisement id
                $referenceId = $this->db->insert_id();

                $transaction = array(
                    "payable_amount" => $_POST['payable_amount'],
                    'reference_id' => $referenceId,
                    'client_sponsor_id' => '',
                    "reference_type" => $reference_type,
                    "vat" => 0,
                    "discount" => 0,
                );

                $particular = "Classified Advertisement ";
                //insert transaction detail
                $transaction_id = $this->insertTransactionLog($transaction, 0, 0, $particular);
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to add the General advertisement details.');
            }
            redirect('advertisements/addClassifiedAdvertisement/', 'refresh');
        } else {
            $cAdvertisement = new stdClass();
            // Go through all the known fields and get the post values
            foreach ($validation_rules as $key => $field) {
                $cAdvertisement->$field['field'] = set_value($field['field']);
            }
            $this->data["cAdvertisement"] = $cAdvertisement;
        }

        $this->template->set_breadcrumb("Add Classified Advertisement Details", site_url("advertisements/addClassifiedAdvertisement"));
        $this->data['page_head'] = 'Add Classified Advertisement Details';
        $this->template->title('Title')
                ->set_layout('form_layout')
                ->build('classified_advertisement_form_view', $this->data);
    }

    /*
     * Delete record
     */

    public function deleteGeneral($id = NULL) {
        if ($id == NULL)
            redirect(base_url("advertisements"), "refresh");

        $this->data["ad"] = $this->db->get_where("general_advertisements", array("g_advertisement_id" => $id))->row();

        if (empty($this->data["ad"]))
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to delete the General Advertisement  details.');
        else {
            if ($this->db->delete("general_advertisements", array("g_advertisement_id" => $id))) {
                $this->session->set_flashdata('success', 'General Advertisement Details deleted Successfully.');
            }
        }
        redirect(base_url("advertisements"), "refresh");
    }

    public function deleteClassified($id = NULL) {
        if ($id == NULL)
            redirect(base_url("advertisements/allClassifiedAdvertisements"), "refresh");

        $this->data["ad"] = $this->db->get_where("classified_advertisements", array("c_advertisement_id" => $id))->row();

        if (empty($this->data["ad"]))
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to delete the Classified Advertisement  details.');
        else {
            if ($this->db->delete("classified_advertisements", array("c_advertisement_id" => $id))) {
                $this->session->set_flashdata('success', 'Classified Advertisement Details deleted Successfully.');
            }
        }
        redirect(base_url("advertisements/allClassifiedAdvertisements"), "refresh");
    }

    /*
      detail of the records
      Report
     */

//
//    public function detail($id) {
//        if ($id == NULL)
//            redirect(base_url("distributors"), "refresh");
//
//        $this->data['distributor'] = $this->db->join("districts", 'districts.district_id= distributors.district')
//                        ->join("gvs", "gvs.gvs_id=distributors.gvs")->get_where("distributors", array("distributor_id" => $id))->row();
//
//        if (empty($this->data['distributor'])) {
//            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to retrieve the distributor\'s Details.');
//            redirect(base_url("distributors"), "redirect");
//        }
//
//        if (!empty($this->data['distributor'])) {
//            $this->template->title('Title')
//                    ->set_layout('detail_view_layout')
//                    ->build('view', $this->data);
//        }
//    }

    /*
     * single status change
     */

    public function changestatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->uri->segment(4);

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

    /*
     * for bulk status change
     */

    public function bulkstatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->input->post('id');

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

}
