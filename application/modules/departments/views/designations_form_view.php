<div id="clone-wrapper" class="col-md-12">
    <div id="clone-0" class="row clone-content">
        <div class="form-group col-md-6">
            <label for="exampleInputEmail1">Designation  Name</label>
            <input type="text" class="form-control" id="designation_full_name" name="designation_full_name_0" placeholder="Designation Name" required="required">
        </div>
        <div class="form-group col-md-6">
            <label for="exampleInputPassword1">Designation Acronym</label>
            <input type="text" class="form-control" id="designation_short_name" name="designation_short_name_0" placeholder="Designation Acronym" required="required">
        </div>
        <input type="hidden" class="form-control" name="department_id_0" id="department_short_name" value="<?php echo $department_id ?>" required="required">
    </div>
</div>
<div class="form-group col-md-8">
    &nbsp;
</div>
<div class="col-md-4">

    <button type="submit" class="btn btn-success f-right">Submit</button>
    <button type="button" class="btn btn-primary f-right margin-right-3" id="clone-button" data-action="clone" >Add More</button>
    <button type="button" class="btn btn-danger f-right margin-right-3 disabled" id="remove-last-clone" data-action="clone" >Remove Last</button>


</div>

<script type="text/javascript" src="<?php echo base_url("assets/js/custom/clone.js"); ?>"></script>