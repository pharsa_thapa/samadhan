
<div class="col-md-12">

    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Department Name</label>
        <input type="text" class="form-control" placeholder="Department's Full Name" id="department_full_name" name="department_full_name" value="<?php echo!empty($department->department_full_name) ? $department->department_full_name : "" ?>" required="required">
    </div>
    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Department Acronym</label>
        <input type="text" class="form-control" placeholder="Department's Short Name" name="department_short_name" id="department_short_name" value="<?php echo!empty($department->department_short_name) ? $department->department_short_name : "" ?>" required="required">
    </div>

    <div class="form-group col-md-12 control-group">

        <label class="control-label" for="radios">Description</label>



        <textarea name="department_description" class="summernote" id="department_description" style="display: none" required="required">
                                
            <?php if (!empty($department->department_description)) { ?>
                <?php echo $department->department_description; ?>
            <?php } ?>
        </textarea>


    </div>


    <div class="form-group col-md-10 margin-bottom-40">
        &nbsp;
        <input type="hidden" class="form-control" name="department_id" id="department_id" value="<?php echo!empty($department->department_id) ? $department->department_id : "" ?>">
    </div>
    <div class="col-md-2 margin-bottom-40">
        <button type="submit" class="btn btn-success f-right margin-right-3" id="submit" >Submit</button>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {
//        $("#gvs").prop("disabled", true);
//
//        $("#registered_date").datepicker({
//            format: "yyyy-m-d",
//            startDate: new Date(),
//            orientation: "top auto",
//            autoclose: true
//        });
        $("#form").validate({
            rules: {
                department_short_name: {
                    required: true,
//                    email: true
                },
                department_full_name: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                },
                department_description: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                }
            }
        });

        $('.summernote').summernote({
            height: 50
        });
//        $("#district").on("change", function () {
//            var requestGVS = $.ajax({
//                type: "post",
//                url: "<?php echo base_url("distributors/changeDistrict"); ?>", //Please cahnge this url on deployment
//                data: {"district_id": $("#district").val()}
//
//            });
//
//            requestGVS.done(function (evt) {
//                data = $.parseJSON(evt);
//                if (data.result == 1) {
//                    $("#gvs").html(data.response);
//                    $("#gvs").prop("disabled", false);
//                    $("#submit").prop('disabled', false);
//                } else {
//                    $("#gvs").html("<option value=''>GVS/Municipality</option>");
//                    $("#submit").attr('disabled', 'disabled');
//                    $("#gvs").prop("disabled", true);
//                }
//
//            });
//        });
//        
//         $("#gvs").on("change", function () {
//            var requestDistrict = $.ajax({
//                type: "post",
//                url: "<?php echo base_url("distributors/changeGvs"); ?>", //Please cahnge this url on deployment
//                data: {"gvs_id": $("#gvs").val()}
//
//            });
//
//            requestDistrict.done(function (evt) {
//                data = $.parseJSON(evt);
//                console.log(data);
//                if (data.result == 1) {
//
//                    $('#district option').prop('selected', false)
//                            .filter('[value="' + data.response + '"]')
//                            .prop('selected', true);
//                    $("#gvs").prop("disabled", false);
//                    $("#submit").prop('disabled', false);
//                }
//            });
//
//        });
//        
    });

</script>