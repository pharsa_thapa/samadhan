
<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Designation  Name</label>
    <input type="text" class="form-control" id="designation_full_name" name="designation_full_name" placeholder="Designation Name" value="<?php echo $designation->designation_full_name ?>" required="required">
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Designation Acronym</label>
    <input type="text" class="form-control" id="designation_short_name" name="designation_short_name" placeholder="Designation Acronym" value="<?php echo $designation->designation_short_name ?>" required="required">
</div>
<input type="hidden" class="form-control" name="designation_id" id="designation_id" value="<?php echo $designation->designation_id ?>" required="required">

<div class="form-group col-md-8">
    &nbsp;
</div>
<div class="col-md-4">

    <button type="submit" class="btn btn-success f-right">Submit</button>
</div>

<script type="text/javascript" src="<?php echo base_url("assets/js/custom/clone.js"); ?>"></script>