<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">



                    <div class="table-responsive">
                        <table id="example" class="display table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Department Full Name</th>
                                    <th>Department Short Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>SN</th>
                                    <th>Department Full Name</th>
                                    <th>Department Short Name</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php $counter = 1;
                                foreach ($records as $record) : ?>
                                    <tr>

                                        <td><?php echo $counter; ?></td>
                                        <td><?php echo $record->department_full_name; ?></td>
                                        <td><?php echo $record->department_short_name; ?></td>
                                        <td>
                                            <a href="<?php echo base_url("departments/addDesignations/$record->department_id"); ?>">
                                                Add Designations
                                            </a>
                                            |
                                            <a href="<?php echo base_url("departments/departmentDetail/$record->department_id"); ?>">
                                                Details
                                            </a>
                                            |
                                            <a href="<?php echo base_url("departments/editDepartment/$record->department_id"); ?>">
                                                Edit
                                            </a> 
                                            | 
                                            <a href="<?php echo base_url("departments/deleteDepartment/$record->department_id"); ?>" 
                                               onclick="javascript : return confirm('Are you sure, you want to delete this record permanently?');">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
    <?php $counter++;
endforeach; ?>

                            </tbody>
                        </table>  
                    </div>



                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->