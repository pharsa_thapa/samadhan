
<div class="form-group col-md-12">
    <h3><u>Department Detail </u></h3>
    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Department's Full Name</label>
        <br>
        <span><?php echo!empty($department->department_full_name) ? $department->department_full_name : "" ?></span>
    </div>
    <div class="form-group col-md-6">
        <label for="exampleInputEmail1">Department's Short Name</label>
        <br>
        <span><?php echo!empty($department->department_short_name) ? $department->department_short_name : "" ?></span>
    </div>
    <div class="form-group col-md-12">
        <label for="exampleInputEmail1">Description</label>
        <br>
        <span><?php echo!empty($department->department_description) ? $department->department_description : "" ?></span>
    </div>
</div>

<div class="form-group col-md-12">
    <h3><u>List of available Designations </u></h3>
    <br>


    <div class="table-responsive row">
        <table id="example" class="display table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Designation Name</th>
                    <th>Designation Short Name</th>
                    <!--<th>Status</th>-->
                    <th>Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Designation Name</th>
                    <th>Designation Short Name</th>
                    <!--<th>Status</th>-->
                    <th>Actions</th>
                </tr>
            </tfoot>
            <tbody>
                <?php foreach ($designations as $record): ?>
                    <tr>
                        <td><?php echo $record->designation_full_name ?></td>
                        <td><?php echo $record->designation_short_name ?></td>
    <!--                        <td>
                        <?php
//                            echo ($record->designation_status == 0) ?
//                                    "<a href='#' class='text-danger' data-attr='inactive' data-taget='designations' title='Activate now!' data-id='" . $record->designation_id . "'>Inactive</a>" :
//                                    "<a href='#' class='text-success' data-attr='active' data-target='designations' title='Dectivate now!' data-id='" . $record->designation_id . "'>Active</a>";
                        ?>
                        </td>-->
                        <td>
    <!--                            <a href="#" data-attr="delete" data-target='designations' data-id='<?php echo $record->designation_id; ?>'>
                                Delete 
                            </a>-->

                            <a href="<?php echo base_url("departments/deleteDesignation/$record->designation_id"); ?>" 
                               onclick="javascript : return confirm('Are you sure, you want to delete this record permanently?');">
                                Delete
                            </a>
                            | 

                            <a href="<?php echo base_url("departments/editDesignation/$record->designation_id"); ?>">
                                Edit
                            </a>
<!--                            |
                            <a href="<?php // echo base_url("salary_heads/manageSalaryHeadAllocation/" . $record->designation_id); ?>">
                                Manage Salary Heads
                                <?php // if ($record->heads == 0) { ?>
                                    <sup class="new-record">New</sup>
                                <?php // } ?>
                            </a>  -->
                        </td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>  
    </div>


</div>

