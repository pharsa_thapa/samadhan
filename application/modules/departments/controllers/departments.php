<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Departments extends Frontend_Controller {

    //validation rules initialization for addDepartment and editDepartment functions 
    private $department_validation_rules = array(
        'department_short_name' => array(
            'field' => 'department_short_name',
            'label' => "Department Name",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'department_full_name' => array(
            'field' => 'department_full_name',
            'label' => 'Department Full Name',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'department_description' => array(
            'field' => 'department_description',
            'label' => 'Department Description',
            'rules' => 'trim|xss_clean|max_length[100]'
        )
    );
    //validation rules initialization for addDesignation and editDesignation functions 
    private $designation_validation_rules = array(
        'designation_full_name_0' => array(
            'field' => 'designation_full_name_0',
            'label' => "Designation Name for Record Number 1",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'designation_short_name_0' => array(
            'field' => 'designation_short_name_0',
            'label' => 'Department Full Name for Record 1',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'department_id_0' => array(
            'field' => 'department_id_0',
            'label' => 'Department for record 1',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        )
    );
    //page title for the controller
    private $page_title = 'Departments';

    public function __construct() {
        parent::__construct();
        //setting up default bread crumbs for the controller
        $this->template->set_breadcrumb('Home', base_url());
        $this->template->set_breadcrumb('User Management', "#");
        $this->template->set_breadcrumb("All Departments", site_url('departments'));
        $this->load->model("departments_model");
    }

    /*
     * Listing all the available departments 
     * Default function when the controller is hit
     */

    public function index() {
        //retrieve all the departments

        $this->data['records'] = $this->db->get("departments")
                ->result();
        //setting up bread crumbs for the page
        $this->template->set_breadcrumb("Department's List", site_url('departments'));
        $this->data['page_head'] = "All Department's List";

        $this->template->title($this->page_title)
                ->set_layout('data_table_layout')
                ->build('list_view', $this->data);
    }

    /*
     * addDepartment for adding department 
     * insert department details submitted through form into database
     */

    public function addDepartment() {
        //initializing form validation library
        $this->load->library('form_validation');
        //setting up validation rules
        $this->form_validation->set_rules($this->department_validation_rules);
        if ($this->form_validation->run() == TRUE) {
            // form data are valid
            // preparing for insertion
            foreach ($this->department_validation_rules as $k => $v) {
                $insert_data [$k] = $this->input->post($k);
            }
            if ($this->db->insert("departments", $insert_data)) {
                //department details inserted successfully
                $this->session->set_flashdata('success', 'New Department\'s Details Added Successfully.');
            } else {
                //failed to insert data
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to add the Department\'s details.');
            }
            redirect('departments/addDepartment/', 'refresh');
        } else {
            //error in validation 
            // redirecting for correction with supplied data
            $department = new stdClass();
            // Go through all the known fields and get the post values
            foreach ($this->department_validation_rules as $key => $field) {
                $department->$field['field'] = set_value($field['field']);
            }
            $this->data["department"] = $department;
        }

        $this->data['page_head'] = "Add Department";
        // setting up breadcrumb for this function
        $this->template->set_breadcrumb("Add Department", site_url('departments/addDepartment'));
        $this->template->title('Title')
                ->set_layout('form_layout')
                ->build('form_view', $this->data);
    }

    /*
     * editDepartment details 
     * @param department_id as $department_id to choose department whose record is to be edited
     */

    public function editDepartment($department_id = NULL) {
        // initialization of form validation library
        $this->load->library('form_validation');
        //setting up validation rules
        $this->form_validation->set_rules($this->department_validation_rules);

        if ($this->form_validation->run() == TRUE) {
            // data are genuine
            // preparing for insertion
            foreach ($this->department_validation_rules as $k => $v) {
                $update_data [$k] = $this->input->post($k);
            }
            //updating details
            if ($this->db->update("departments", $update_data, array("department_id" => $this->input->post("department_id")))) {
                //update is successful
                $this->session->set_flashdata('success', 'Department\'s Details updated Successfully.');
            } else {
                //Failed to make updates
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to update the Department\'s details.');
            }
            redirect('departments/index', 'refresh');
        } else {
            if ($department_id == NULL)
            //supplied paramater is not genuine
                redirect(base_url("departments"), "refresh");
            // retrieving department detail if the supplied param is genuine
            $this->data["department"] = $this->db->get_where("departments", array("department_id" => $department_id))->row();
            //  No records found in the database
            if (empty($this->data["department"]))
                redirect(base_url("departments"), "refresh");
            //setting up bread crumb for the function itself
            $this->template->set_breadcrumb("Edit Department", site_url("departments/editDepartment/$department_id"));
            $this->data['page_head'] = "Edit Department's Details - {$this->data["department"]->department_full_name}";

            $this->template->title('Title')
                    ->set_layout('form_layout')
                    ->build('form_view', $this->data);
        }
    }

    /*
     * Delete department
     * @param department_id
     */

    public function deleteDepartment($department_id = NULL) {
        //param is not set
        if ($department_id == NULL)
            redirect(base_url("departments"), "refresh");
        //retrieving details to check if the parram is genuine
        $this->data["department"] = $this->db->get_where("departments", array("department_id" => $department_id))->row();
        // param is not genuine,details not found in db
        if (empty($this->data["department"]))
        //setting up error message
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to delete the department details.');
        else {
            //deleting department record 
            if ($this->db->delete("departments", array("department_id" => $department_id))) {
                //setting up success message
                $this->session->set_flashdata('success', 'Department\'s Details deleted Successfully.');
            }
        }
        redirect(base_url("departments"), "refresh");
    }

    /*
     * detail of the department record
     * Report
     */

    public function departmentDetail($department_id = NULL) {
        if ($department_id == NULL)
            redirect(base_url("departments"), "refresh");

        $this->data['department'] = $this->db->get_where("departments", array("department_id" => $department_id))->row();

        if (empty($this->data['department']) || $department_id = NULL) {
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to retrieve the department\'s Details.');
            redirect(base_url("departments"), "redirect");
        }
        //listing all the designations of the departments
        $this->data['designations'] = $this->departments_model->getDesignations($this->data['department']->department_id);
        $this->template->set_breadcrumb("Department's Details", site_url("departments/departmentDetail/" . $this->data['department']->department_id));
        $this->data['page_head'] = "Department's Details - {$this->data['department']->department_full_name}";
        $this->template->title($this->page_title)
                ->set_layout('detail_view_layout')
                ->build('view', $this->data);
    }

    /*
     * Designation section
     * CRUD for designations for a department
     */

    /*
     * Add department designations
     * @param department_id
     */

    public function addDesignations($department_id = NULL) {
        $this->data['department_detail'] = $this->db->get_where("departments", array("department_id" => $department_id))->row();

        if ($department_id == NULL || empty($this->data['department_detail']))
            redirect(base_url("departments", 'refresh'));
        $this->page_title = "Designations";
        //initializing form validation library
        $this->load->library('form_validation');
        $this->data["department_id"] = $department_id;

        if ($this->input->post()) {
            $this->load->library("bulk_form_validation");
            $this->bulk_form_validation->parseBulkForm($this->input->post(), $this->designation_validation_rules);
            $response = $this->bulk_form_validation->processResponse();

            if (!empty($response["validInputs"])) {
                $this->data["successMessage"] = $response['totalValidInputsCount'] . " of " . $response["totalInputsCount"] . " inserted successfully!";
                $this->db->insert_batch('designations', $response["validInputs"]);
            }

            if (!empty($response["invalidInputs"])) {
                $this->data["errorMessage"] = $response['totalInvalidInputsCount'] . " of " . $response["totalInputsCount"] . " seems to have some errors!";
                $this->data["invalidInputs"] = $response["invalidInputs"];
            }
        }

        $this->data['page_head'] = "Add Designations For Department : {$this->data['department_detail']->department_full_name} ";

        // setting up breadcrumb for this function
        $this->template->set_breadcrumb($this->data['department_detail']->department_full_name, site_url('departments/departmentDetail/' . ($this->data['department_detail']->department_id)));
        $this->template->set_breadcrumb("Add Designations", site_url('departments/addDesignations/' . $this->data['department_detail']->department_id));

        $this->template->title($this->page_title)
                ->set_layout('form_layout')
                ->build('designations_form_view', $this->data);
    }

    /*
     * List of all Designations
     * Do not filters departments
     * SImply query all the all the designations
     */

    public function listDesignations() {
        $this->load->model("designations_model");
        $this->data['records'] = $this->designations_model->getAllDesignations();

        //setting up bread crumbs for the page
        $this->template->set_breadcrumb("All Designation's List", site_url('departments/listDesignations'));
        $this->data['page_head'] = "All Designations' List";

        $this->template->title($this->page_title)
                ->set_layout('data_table_layout')
                ->build('designations_list_view', $this->data);
    }

    /*
     * Delete designation
     */

    public function deleteDesignation($designation_id = null) {
        if (is_null($designation_id))
            redirect(base_url("departments/listDesignations", "refresh"));
        //default menu heads cannot be deleted

        $data = $this->db->get_where("designations", array("designation_id" => $designation_id))->row();
        if (empty($data)) {
            $this->session->set_flashdata("error", "Cannot find the requested designation!");
            redirect(base_url("departments/listDesignations"), "refresh");
        }
        $this->db->delete("designations", array("designation_id" => $designation_id));
        $this->session->set_flashdata("success", "Designation Deleted Successfully!");
        redirect(base_url("departments/listDesignations"), "refresh");
    }

    /*
     * Edit Designation Details
     */

    function editDesignation($designationId = NULL) {
        $validation_rules = array(
            'designation_full_name' => array(
                'field' => 'designation_full_name',
                'label' => "Designation Name",
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'designation_short_name' => array(
                'field' => 'designation_short_name',
                'label' => 'Designation Accronym',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            ),
            'designation_id' => array(
                'field' => 'designation_id',
                'label' => 'Designation Id',
                'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
            )
        );
        $this->load->library('form_validation');
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run() == TRUE) {
            // preparing for insertion
            foreach ($validation_rules as $k => $v) {
                $update_data [$k] = $this->input->post($k);
            }
            unset($update_data['designation_id']);
            if ($this->db->update("designations", $update_data, array("designation_id" => $this->input->post("designation_id")))) {
                $this->session->set_flashdata('success', 'Designations Details updated Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to update the designation details.');
            }
            redirect(base_url("departments/listDesignations"), "refresh");
        } else {
            if ($designationId == NULL)
                redirect(base_url("departments/listDesignations"), "refresh");

            $this->data["designation"] = $this->db->get_where("designations", array("designation_id" => $designationId))->row();

            if (empty($this->data["designation"]))
                redirect(base_url("departments/listDesignations"), "refresh");

            $this->template->set_breadcrumb("All Designations List", base_url("departments/listDesignations"));
            $this->template->set_breadcrumb("Edit Designation Details", site_url("departments/editDesignation/$designationId"));

            $this->data['page_head'] = "Edit Designation Details - {$this->data['designation']->designation_full_name}";
            $this->template->title('Title')
                    ->set_layout('form_layout')
                    ->build('designation_edit_form', $this->data);
        }
    }

}
