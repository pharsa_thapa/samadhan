<?php

class Departments_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    function getDesignations($department_id) {
        $query = "SELECT * "
                . "FROM (`designations`) "
                . "JOIN `departments` "
                . "ON `departments`.`department_id`= `designations`.`department_id` "
                . "AND `designations`.`department_id` = '" . $department_id . "'";

        return $this->db->query($query)->result();
    }

}
