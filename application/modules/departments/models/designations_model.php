<?php

class Designations_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    function getAllDesignations() {
        $query = "SELECT *,(select count(*) from salary_head_designation_allocation "
                . " where designation_id=designations.designation_id )as heads "
                . " FROM (`designations`) JOIN `departments` ON `departments`.`department_id`= `designations`.`department_id` ";
//                . "AND `designations`.`department_id` = '" . $department_id . "'";

        return $this->db->query($query)->result();
    }

}
