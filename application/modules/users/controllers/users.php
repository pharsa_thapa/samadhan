<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends Frontend_Controller {

    public function __construct() {
        parent::__construct();
        $this->template->set_breadcrumb('Home', base_url());
        $this->template->set_breadcrumb("User Management", site_url("#"));

        $this->load->library(array('ion_auth', 'form_validation'));
    }

    /*
     * LIST OF ALL USERS
     */

    public function index() {
        $this->data['records'] = $this->db
                ->select("u.id, u.username, u.first_name, u.last_name, de.department_full_name, dg.designation_full_name, g.name")
                ->join("departments de", "de.department_id = u.department_id", "left")
                ->join("designations dg", "dg.designation_id = u.designation_id", "left")
                ->join("users_groups ug", "ug.user_id = u.id", "left")
                ->join("groups g", "g.id = ug.group_id", "left")
                ->where("u.id !=", "1")
                ->get("users u")
                ->result();

        $this->template->set_breadcrumb("Users", "#");
        $this->template->set_breadcrumb("All Users", site_url("users"));
        $this->data['page_head'] = 'All Users List';
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('list_view', $this->data);
    }

    public function addUser() {
        $user_groups = $this->db->where("id!=", "1", false)->get("groups")->result();
        $user_options = array();
        foreach ($user_groups as $user_group) {
            $user_options[$user_group->id] = ucfirst($user_group->name);
        }
        $departments = $this->db->get("departments")->result();
        foreach ($departments as $department) {
            $department_options[$department->department_id] = ucfirst($department->department_full_name);
        }
        $designations = $this->db->get("designations")->result();
        $designation_options = array();
        foreach ($designations as $designation) {
            $designation_options[$designation->designation_id] = ucfirst($designation->designation_full_name);
        }

        $this->data['user_groups'] = $user_options;
        $this->data['department_options'] = $department_options;
        $this->data['designation_options'] = $designation_options;

        $tables = $this->config->item('tables', 'ion_auth');

//validate form input
        $this->form_validation->set_rules('username', "Username", 'required|xss_clean|is_unique[' . $tables['users'] . '.username]');
        $this->form_validation->set_rules('first_name', "First Name", 'required|xss_clean');
        $this->form_validation->set_rules('last_name', "Last Name", 'required|xss_clean');
        $this->form_validation->set_rules('email', "Email Address", 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'required|xss_clean');
        $this->form_validation->set_rules('address_1', "Permanent Address", 'required|xss_clean');
        $this->form_validation->set_rules('address_2', "Address", 'required|xss_clean');
        $this->form_validation->set_rules('marital_status', "Marital Status", 'required|xss_clean');
        $this->form_validation->set_rules('gender', "Gender", 'required|xss_clean');
        $this->form_validation->set_rules('dob', "Date Of Birth", 'required|xss_clean');
//        $this->form_validation->set_rules('blood_group', "Blood Group", 'xss_clean');
        $this->form_validation->set_rules('next_to_keen', "Next To Keen", 'xss_clean');
        $this->form_validation->set_rules('salary', "Salary", 'xss_clean|numeric');
        $this->form_validation->set_rules('user_group', "User Group", 'required|xss_clean');
        $this->form_validation->set_rules('department_id', "Department", 'required|xss_clean');
        $this->form_validation->set_rules('designation_id', "Designation", 'required|xss_clean');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[re_password]');
        $this->form_validation->set_rules('re_password', "Re-type Password", 'required');
//        $this->form_validation->set_rules('ip_address', "IP Address", 'required');


        if ($this->form_validation->run() == true) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $ip = $this->input->post('ip');
            $user_group_id = $this->input->post("user_group");

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'phone' => $this->input->post('phone'),
                'address_1' => $this->input->post('address_1'),
                'address_2' => $this->input->post('address_2'),
//                'email' => $this->input->post('email'),
                'dob' => $this->input->post('dob'),
                'blood_group' => $this->input->post('blood_group'),
//                'ip_address' => $this->input->post('ip_address'),
                'next_to_keen' => $this->input->post('next_to_keen'),
                'salary' => $this->input->post('salary'),
                'marital_status' => $this->input->post('marital_status'),
                'gender' => $this->input->post('gender'),
                'department_id' => $this->input->post('department_id'),
                'designation_id' => $this->input->post('designation_id'),
//                'active' => 1,
            );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $this->input->post('email'), $additional_data, array($user_group_id))) {
            //check to see if we are creating the user
            $this->session->set_flashdata('success', "User added succefully!");
            redirect("users/addUser", 'refresh');
        } else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['first_name'] = array(
                'name' => 'first_name',
                'id' => 'first_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('first_name'),
                'class' => "form-control",
                "placeholder" => "First Name"
            );
            $this->data['last_name'] = array(
                'name' => 'last_name',
                'id' => 'last_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('last_name'),
                'class' => "form-control",
                " placeholder" => "Last Name"
            );
            $this->data['address_2'] = array(
                'name' => 'address_2',
                'id' => 'address_2',
                'type' => 'text',
                'value' => $this->form_validation->set_value('address_2'),
                'class' => "form-control",
                " placeholder" => "Current Address"
            );

            $this->data['address_1'] = array(
                'name' => 'address_1',
                'id' => 'address_1',
                'type' => 'text',
                'value' => $this->form_validation->set_value('address_1'),
                'class' => "form-control",
                "placeholder" => "Permanent Address"
            );

            $this->data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'email',
                'value' => $this->form_validation->set_value('email'),
                'class' => "form-control",
                "placeholder" => "Primary Email Address"
            );
            $this->data['phone'] = array(
                'name' => 'phone',
                'id' => 'phone',
                'type' => 'text',
                'value' => $this->form_validation->set_value('phone'),
                'class' => "form-control",
                "placeholder" => "Contact Number"
            );

            ///
            //Gender and Marrital Status
            ///
            $this->data['dob'] = array(
                'name' => 'dob',
                'id' => 'dob',
                'type' => 'text',
                'value' => $this->form_validation->set_value('dob'),
                'class' => "form-control datepicker",
                "placeholder" => "Date Of Birth"
            );
//            $this->data['blood_group'] = array(
//                'name' => 'blood_group',
//                'id' => 'blood_group',
//                'type' => 'text',
//                'value' => $this->form_validation->set_value('blood_group'),
//                'class' => "form-control",
//                "placeholder" => "Blood Group"
//            );
            $this->data['next_to_keen'] = array(
                'name' => 'next_to_keen',
                'id' => 'next_to_keen',
                'type' => 'text',
                'value' => $this->form_validation->set_value('next_to_keen'),
                'class' => "form-control",
                "placeholder" => "Guaranteer"
            );
            $this->data['salary'] = array(
                'name' => 'salary',
                'id' => 'salary',
                'type' => 'number',
                'value' => $this->form_validation->set_value('salary'),
                'class' => "form-control",
                "placeholder" => "Salary"
            );

            $this->data['user_group'] = array(
                'name' => 'user_group',
//                'id' => 'change-state',
//                'type' => 'text',
                'value' => $this->form_validation->set_value('user_group'),
                'class' => "form-control",
//                "placeholder" => "State",
            );


            $this->data['username'] = array(
                'name' => 'username',
                'id' => 'username',
                'type' => 'text',
                'value' => $this->form_validation->set_value('username'),
                'class' => "form-control",
                "placeholder" => "Username"
            );

            $this->data['password'] = array(
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'value' => $this->form_validation->set_value('password'),
                'class' => "form-control",
                "placeholder" => "Password"
            );
            $this->data['re_password'] = array(
                'name' => 're_password',
                'id' => 're_password',
                'type' => 'password',
                'value' => $this->form_validation->set_value('re_password'),
                'class' => "form-control",
                "placeholder" => "Confirm Password"
            );
//            $this->data['ip_address'] = array(
//                'name' => 'ip_address',
//                'id' => 'ip_address',
//                'type' => 'text',
//                'value' => $this->form_validation->set_value('ip_address'),
//                'class' => "form-control",
//                "placeholder" => "IP Address"
//            );

            $this->template->set_breadcrumb("Add User", site_url("#"));
            $this->template->set_breadcrumb("New User", site_url("users/addUser"));
            $this->data['page_head'] = 'Add New User';
            $this->template->title('Title')
                    ->set_layout('form_wizard_layout')
                    ->build('form_view', $this->data);
        }
    }

    /*
     * 
     * Edit user details
     */

    function editUser($id = null) {
        if ($id == null)
            redirect(base_url("users"), "refresh");

        $this->data['title'] = "Edit User";

        $user_groups = $this->db->where("id!=", "1", false)->get("groups")->result();
        $user_options = array();
        foreach ($user_groups as $user_group) {
            $user_options[$user_group->id] = ucfirst($user_group->name);
        }
        $departments = $this->db->get("departments")->result();
        foreach ($departments as $department) {
            $department_options[$department->department_id] = ucfirst($department->department_full_name);
        }
        $designations = $this->db->get("designations")->result();
        foreach ($designations as $designation) {
            $designation_options[$designation->designation_id] = ucfirst($designation->designation_full_name);
        }

        $this->data['user_groups'] = $user_options;
        $this->data['department_options'] = $department_options;
        $this->data['designation_options'] = $designation_options;

        $this->data['id'] = $id;

        //user details
        $user = $this->ion_auth->user($id)->row();
        $groups = $this->ion_auth->groups()->result_array();
        $currentGroups = $this->ion_auth->get_users_groups($id)->result();


        $tables = $this->config->item('tables', 'ion_auth');

        //validate form input
        $this->form_validation->set_rules('first_name', "First Name", 'required|xss_clean');
        $this->form_validation->set_rules('last_name', "Last Name", 'required|xss_clean');
        $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'required|xss_clean');
        $this->form_validation->set_rules('address_1', "Permanent Address", 'required|xss_clean');
        $this->form_validation->set_rules('address_2', "Address", 'required|xss_clean');
        $this->form_validation->set_rules('marital_status', "Marital Status", 'required|xss_clean');
        $this->form_validation->set_rules('gender', "Gender", 'required|xss_clean');
        $this->form_validation->set_rules('dob', "Date Of Birth", 'required|xss_clean');
//        $this->form_validation->set_rules('blood_group', "Blood Group", 'xss_clean');
        $this->form_validation->set_rules('next_to_keen', "Next To Keen", 'xss_clean');
        $this->form_validation->set_rules('salary', "Salary", 'xss_clean|numeric');
        $this->form_validation->set_rules('user_group', "User Group", 'required|xss_clean');
        $this->form_validation->set_rules('department_id', "Department", 'required|xss_clean');
        $this->form_validation->set_rules('designation_id', "Designation", 'required|xss_clean');

//        $this->form_validation->set_rules('ip_address', "IP Address", 'required');


        if (isset($_POST) && !empty($_POST)) {

            //update the password if it was posted
            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->form_validation->run() === TRUE) {
                $this->load->model("auth/ion_auth_model");

                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'email' => $this->input->post('email'),
                    'phone' => $this->input->post('phone'),
                    'address_1' => $this->input->post('address_1'),
                    'address_2' => $this->input->post('address_2'),
                    'dob' => $this->input->post('dob'),
//                    'blood_group' => $this->input->post('blood_group'),
//                    'ip_address' => $this->input->post('ip_address'),
                    'next_to_keen' => $this->input->post('next_to_keen'),
                    'salary' => $this->input->post('salary'),
                    'marital_status' => $this->input->post('marital_status'),
                    'gender' => $this->input->post('gender'),
                    'department_id' => $this->input->post('department_id'),
                    'designation_id' => $this->input->post('designation_id'),
                    'active' => 1,
                );

                //update the password if it was posted
                if ($this->input->post('password')) {
                    $data['password'] = $this->input->post('password');
                }

                //check to see if we are updating the user
                if ($this->ion_auth_model->update($this->input->post("id"), $data)) {
                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('success', $this->ion_auth->messages());
                } else {
                    // if non admin
                    $this->session->set_flashdata('error', $this->ion_auth->errors());
                }
                redirect(base_url("users"), "refresh");
            }
            $this->session->set_flashdata('error', validation_errors());
            redirect(base_url("users"), "refresh");
        }

        //display the edit user form
//        $this->data['csrf'] = $this->_get_csrf_nonce();
        //set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        //pass the user to the view
        $this->data['user'] = $user;
        $this->data['groups'] = $groups;
        $this->data['currentGroups'] = $currentGroups;

        $this->data['first_name'] = array(
            'name' => 'first_name',
            'id' => 'first_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('first_name', $user->first_name),
            'class' => "form-control",
            "placeholder" => "First Name"
        );
        $this->data['last_name'] = array(
            'name' => 'last_name',
            'id' => 'last_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('last_name', $user->last_name),
            'class' => "form-control",
            " placeholder" => "Last Name"
        );
        $this->data['address_2'] = array(
            'name' => 'address_2',
            'id' => 'address_2',
            'type' => 'text',
            'value' => $this->form_validation->set_value('address_2', $user->address_2),
            'class' => "form-control",
            " placeholder" => "Current Address"
        );

        $this->data['address_1'] = array(
            'name' => 'address_1',
            'id' => 'address_1',
            'type' => 'text',
            'value' => $this->form_validation->set_value('address_1', $user->address_1),
            'class' => "form-control",
            "placeholder" => "Permanent Address"
        );
        $this->data['email'] = array(
            'name' => 'email',
            'id' => 'email',
            'type' => 'text',
            'value' => $this->form_validation->set_value('email', $user->email),
            'class' => "form-control",
            "placeholder" => "Email Address"
        );


        $this->data['phone'] = array(
            'name' => 'phone',
            'id' => 'phone',
            'type' => 'text',
            'value' => $this->form_validation->set_value('phone', $user->phone),
            'class' => "form-control",
            "placeholder" => "Contact Number"
        );

        ///
        //Gender and Marrital Status
        ///
        $this->data['dob'] = array(
            'name' => 'dob',
            'id' => 'dob',
            'type' => 'text',
            'value' => $this->form_validation->set_value('dob', $user->dob),
            'class' => "form-control datepicker",
            "placeholder" => "Date Of Birth"
        );
//        $this->data['blood_group'] = array(
//            'name' => 'blood_group',
//            'id' => 'blood_group',
//            'type' => 'text',
//            'value' => $this->form_validation->set_value('blood_group', $user->blood_group),
//            'class' => "form-control",
//            "placeholder" => "Blood Group"
//        );
        $this->data['next_to_keen'] = array(
            'name' => 'next_to_keen',
            'id' => 'next_to_keen',
            'type' => 'text',
            'value' => $this->form_validation->set_value('next_to_keen', $user->next_to_keen),
            'class' => "form-control",
            "placeholder" => "Next To Keen"
        );

        $this->data['salary'] = array(
            'name' => 'salary',
            'id' => 'salary',
            'type' => 'number',
            'value' => $this->form_validation->set_value("salary", $user->salary),
            'class' => "form-control",
            "placeholder" => "Salary"
        );
//        $this->data['relationship'] = array(
//            'name' => 'relationship',
//            'id' => 'relationship',
//            'type' => 'text',
//            'value' => $this->form_validation->set_value('relationship', $user->relationship),
//            'class' => "form-control",
//            "placeholder" => "Relationship"
//        );


        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
            'value' => "",
            'class' => "form-control",
            "placeholder" => "Password"
        );
        $this->data['re_password'] = array(
            'name' => 're_password',
            'id' => 're_password',
            'type' => 'password',
            'value' => "",
            'class' => "form-control",
            "placeholder" => "Re-type Password"
        );

//        $this->data['ip_address'] = array(
//            'name' => 'ip_address',
//            'id' => 'ip_address',
//            'type' => 'text',
//            'value' => $this->form_validation->set_value('ip_address', $user->ip_address),
//            'class' => "form-control",
//            "placeholder" => "IP Address"
//        );

        $this->template->set_breadcrumb("Edit User Details", site_url("#"));
        $this->template->set_breadcrumb($user->first_name . " " . $user->last_name, site_url("users/editUser/$id"));

        $this->data['page_head'] = 'Edit User Details - ' . $user->first_name . " " . $user->last_name;
        $this->template->title('Title')
                ->set_layout('form_wizard_layout')
                ->build('edit_form_view', $this->data);
    }

    /*
     * Delete record
     */

    public function deleteUser($id = NULL) {

        if ($id == NULL)
            redirect(base_url("users"), "refresh");


        if ($id == $this->ion_auth->get_user_id() || $id == 1) {
            $this->session->set_flashdata("error", "Sorry! You cannot delete this user!");
            redirect(base_url("users"), "refresh");
        }
        $this->data["user"] = $this->db->get_where("users", array("id" => $id))->row();

        if (empty($this->data["user"]))
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to delete the user  details.');
        else {
            if ($this->db->delete("users", array("id" => $id))) {
                $this->session->set_flashdata('success', 'User Details deleted Successfully.');
            }
        }
        redirect(base_url("users"), "refresh");
    }

    /*
     * 
     * functions by ion_auth library
     */

    function _get_csrf_nonce() {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    function _valid_csrf_nonce() {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
                $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')
        ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
