<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <?php echo form_open_multipart(uri_string(), array('id' => 'wizardForm', "method" => "POST")); ?>

                    <div class="form-group col-md-12 ">
                        <?php
                        if (validation_errors())
                            echo "<div class='alert alert-danger alert-dismissible'> <strong>Aw Snap! Something went wrong!</strong><br>" . validation_errors() . "</div>";
                        if (@$file_upload_error)
                            echo "<div class='alert alert-danger alert-dismissible'> <strong>Aw Snap! Something went wrong!</strong><br>" . $file_upload_error . "</div>";

                        if ($this->session->flashdata('success'))
                            echo "<div class='alert alert-success alert-dismissible'>" . $this->session->flashdata('success') . "</div>";

                        if ($this->session->flashdata('error'))
                            echo "<div class='alert alert-danger alert-dismissible'><strong>Aw Snap! Something went wrong!</strong><br>" . $this->session->flashdata('error') . "</div>";
                        ?>
                    </div>


                    <div class="col-md-12">
                        <div id="rootwizard">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Personal Info</a></li>
                                <li role="presentation"><a href="#tab2" data-toggle="tab"><i class="fa fa-user-plus m-r-xs"></i> Employee Group</a></li>
                                <li role="presentation"><a href="#tab3" data-toggle="tab"><i class="fa fa-key m-r-xs"></i>Credentials</a></li>
                                <!--<li role="presentation"><a href="#tab4" data-toggle="tab"><i class="fa fa-check m-r-xs"></i>Documents</a></li>-->
                                <li role="presentation"><a href="#tab5" data-toggle="tab"><i class="fa fa-check m-r-xs"></i>Finish Up</a></li>
                            </ul>


                            <div class="progress progress-sm m-t-sm">
                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                </div>
                            </div>
                            <form id="wizardForm">
                                <div class="tab-content">

                                    <input type="hidden" value="<?php echo $id; ?>" name="id">
                                    <div class="tab-pane active fade in" id="tab1">
                                        <div class="row m-b-lg">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputName">First Name</label>
                                                        <?php echo form_input($first_name); ?>
                                                    </div>
                                                    <div class="form-group  col-md-6">
                                                        <label for="exampleInputName2">Last Name</label>
                                                        <?php echo form_input($last_name); ?>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail">Address</label>
                                                        <?php echo form_input($address_2); ?>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail">Permanent Address</label>
                                                        <?php echo form_input($address_1); ?>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail">Email address</label>
                                                        <?php echo form_input($email); ?>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail">Contact Number</label>
                                                        <?php echo form_input($phone); ?>
                                                    </div>

                                                    <div class="form-group col-md-6 control-group">

                                                        <label class="control-label" for="radios">Gender</label>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <label class="radio inline " for="radios-0">
                                                                    <input type="radio" name="gender" id="radios-0" value="1" checked="checked">
                                                                    Male
                                                                </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label class="radio inline" for="radios-1">
                                                                    <input type="radio" name="gender" id="radios-1" value="0">
                                                                    Female
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="form-group col-md-6 control-group">

                                                        <label class="control-label" for="radios">Marital Status</label>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <label class="radio inline " for="radios-0">
                                                                    <input type="radio" name="marital_status" id="radios-3" value="1" checked="checked">
                                                                    Single
                                                                </label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label class="radio inline" for="radios-1">
                                                                    <input type="radio" name="marital_status" id="radios-4" value="0">
                                                                    Married
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputName">Date Of Birth</label>
                                                        <?php echo form_input($dob); ?>
                                                    </div>
                                                    <!--                                                    <div class="form-group  col-md-6">
                                                                                                            <label for="exampleInputName2">Blood Group</label>
                                                    <?php // echo form_input($blood_group); ?>
                                                                                                        </div>-->

                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputName">Next To Keen</label>
                                                        <?php echo form_input($next_to_keen); ?>
                                                    </div>
                                                    <!--                                                    <div class="form-group  col-md-6">
                                                                                                            <label for="exampleInputName2">Relationship</label>
                                                    <?php // echo form_input($relationship); ?>
                                                                                                        </div>-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab2">
                                        <div class="row m-b-lg">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputName">User Type</label>
                                                        <!--<select class="form-control" name="user_group">-->

                                                        <?php echo form_dropdown('user_group', $user_groups, '', "class='form-control'"); ?>

                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputName">Department Name</label>

                                                        <?php echo form_dropdown('department_id', $department_options, '', "class='form-control' id='department'"); ?>
                                                    </div>
                                                    <div class="form-group  col-md-6">
                                                        <label for="exampleInputName2">Designation</label>

                                                        <?php echo form_dropdown('designation_id', $designation_options, '', "class='form-control' id='designation'"); ?>

                                                    </div>


                                                    <div class="form-group  col-md-6">
                                                        <label for="exampleInputName2">Salary</label>
                                                        <?php echo form_input($salary); ?>
                                                    </div>




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row"> 

                                                    <div class="col-md-12">

                                                        <div class="col-md-6">
                                                            <label for="exampleInputName2">Password</label>
                                                            <?php echo form_input($password); ?>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="col-md-6">
                                                                <label for="exampleInputName2">Re-Password</label>
                                                                <?php echo form_input($re_password); ?>

                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                                <!--                                                <div class="row">
                                                                                                    <div class="col-md-12">
                                                                                                        <div class="form-group  col-md-6">
                                                                                                            <label for="exampleInputName2">IP Address</label>
                                                <?php echo form_input($ip_address); ?>
                                                                                                        </div> 
                                                                                                    </div>
                                                                                                </div>-->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab5">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="col-md-3 center">


                                                    <div class="login-box" style="margin:80px auto; text-align: center;">
                                                        <h3>Save Changes Now!</h3>
                                                        <button  class="btn btn-success" type="submit">Finish Up</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="pager wizard">
                                        <li class="previous"><a href="#" class="btn btn-default">Previous</a></li>
                                        <li class="next"><a href="#" class="btn btn-default">Next</a></li>
                                    </ul>

                                </div>
                            </form>

                            <script type="text/javascript" src="<?php echo base_url("assets/js/custom/ajaxCalls.js"); ?>"></script>
                            <script type="text/javascript" src="<?php echo base_url("assets/js/custom/addEmployee.js"); ?>"></script>
                            <script type="text/javascript">

                                $(document).ready(function (fn) {
                                    $("#wizardForm").validate({
                                        rules: {
                                            first_name: {
                                                required: true
                                            },
                                            last_name: {
                                                required: true
                                            },
                                            address_2: {
                                                required: true
                                            },
                                            address_1: {
                                                required: true
                                            },
                                            email_1: {
                                                required: true,
                                                email: true
                                            },
                                            email_2: {
                                                email: true
                                            },
                                            password: {
                                                required: fals,
                                            },
                                            re_password: {
                                                required: false,
                                                equalTo: '#password'
                                            },
                                            dob: {
                                                required: true,
                                            },
                                        }
                                    });
                                    $('.summernote').summernote({
                                        height: 50
                                    });

                                    $('.datepicker').datepicker({
                                        format: "yyyy-m-d",
                                        orientation: "top auto",
                                        autoclose: true
                                    });
                                });
                            </script>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->