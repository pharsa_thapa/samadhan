<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="example" class="display table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>User Full Name</th>
                                    <th>Username</th>
                                    <th>Group</th>
                                    <th>Department</th>
                                    <th>Designation</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>User Full Name</th>
                                    <th>Username</th>
                                    <th>Group</th>
                                    <th>Department</th>
                                    <th>Designation</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($records as $record) : ?>
                                    <tr>

                                        <td><?php echo $record->first_name . ' ' . $record->last_name; ?></td>
                                        <td><?php echo $record->username; ?></td>
                                        <td><?php echo $record->name; ?></td>
                                        <td><?php echo $record->department_full_name; ?></td>
                                        <td><?php echo $record->designation_full_name ?></td>
                                        <td>

                                            <a href="<?php echo base_url("users/editUser/$record->id"); ?>">
                                                Edit
                                            </a> 
                                            | 
                                            <a href="<?php echo base_url("users/deleteUser/$record->id"); ?>" 
                                               onclick="javascript : return confirm('Are you sure, you want to delete this user permanently?');">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table> 
                    </div>

                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->