<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class UserGroups_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    public function getAllGroups() {
        $query = "SELECT *,(select count(*) from permissions "
                . " where group_id=groups.id )as permissionCount "
                . " FROM (`groups`) WHERE groups.id !=1 ";

        return $this->db->query($query)->result();
    }

}
