<div class="table-responsive">
    <table id="example" class="display table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="col-md-5">User Group Name</th>
                <th class="col-md-4">User Group Title</th>
<!--                <th>Effective To</th>
                <th>Status</th>-->
                <th class="col-md-3">Actions</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>User Group Name</th>
                <th>User Group Title</th>
<!--                <th>Effective To</th>
                <th>Status</th>-->
                <th>Actions</th>
            </tr>
        </tfoot>
        <tbody>

            <?php foreach ($records as $record) : ?>
                <tr>
                    <td><?php echo $record->name; ?></td>
                    <td><?php echo $record->description; ?></td>
    <!--                    <td><?php // echo $record->effective_to;      ?></td>-->
                    <!--<td><?php // echo $record->scheme_status == 1 ? "Active" : "Inactive";      ?></td>-->
                    <td> 

                        <a href="<?php echo base_url("userGroups/editGroup/$record->id") ?>">Edit</a>
                        |
                        <a href="<?php echo base_url("userGroups/deleteUserGroup/$record->id"); ?>" 
                           onclick="javascript : return confirm('Are you sure, you want to delete this record permanently?');">
                            Delete
                        </a>
                        |
                        <a href="<?php echo base_url("rbac/managePermission/$record->id") ?>">Manage Permissions
                            <?php echo ($record->permissionCount <= 0 ) ? "<sup class='color-red'>New</sup>" : '' ?> 

                        </a>
                        
                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>  
</div>