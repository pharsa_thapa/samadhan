

<div class="form-group col-md-6">
    <?php echo lang('create_group_name_label', 'group_name'); ?> <br />
    <?php echo form_input($group_name); ?>
</div>
<div class="form-group col-md-6">
    <?php echo lang('create_group_desc_label', 'description'); ?> <br />
    <?php echo form_input($description); ?>
</div>
<div class="form-group col-md-10 margin-bottom-40">
    &nbsp;
</div>
<div class="col-md-2 margin-bottom-40">
    <button type="submit" class="btn btn-success f-right margin-right-3" >Submit</button>

</div>