<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserGroups extends Frontend_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
        $this->load->model("userGroups_model");

        $this->template->set_breadcrumb("Home", site_url());
        $this->template->set_breadcrumb("User Management ", site_url("#"));
        $this->template->set_breadcrumb("User Groups ", site_url("userGroups"));
    }

    function index() {
        $this->data['records'] = $this->userGroups_model->getAllGroups();

        $this->data["page_head"] = "All User Group List";

        $this->template->set_breadcrumb("User Groups List ", site_url("userGroups/index"));
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('list_view', $this->data);
    }

    // create a new group
    function createGroup() {
        $this->data['title'] = $this->lang->line('create_group_title');

        //validate form input
        $this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'required|xss_clean');
        $this->form_validation->set_rules('description', $this->lang->line('create_group_validation_desc_label'), 'xss_clean');

        if ($this->form_validation->run() == TRUE) {
            $new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
            if ($new_group_id) {
                // check to see if we are creating the group
                // redirect them back to the admin page
                $this->session->set_flashdata('success', $this->ion_auth->messages());
                redirect(base_url("userGroups"), 'refresh');
            }
        } else {
            //display the create group form
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['group_name'] = array(
                'name' => 'group_name',
                'id' => 'group_name',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('group_name'),
            );
            $this->data['description'] = array(
                'name' => 'description',
                'id' => 'description',
                'class' => 'form-control',
                'type' => 'text',
                'value' => $this->form_validation->set_value('description'),
            );

            $this->data["page_head"] = "Add User Group";


            $this->template->set_breadcrumb("Add User Group", site_url("userGroups/createGroup"));
            $this->template->title($this->data['title'])
                    ->set_layout('form_layout')
                    ->build('create_group', $this->data);
        }
    }

//edit a group
    function editGroup($id) {
        // bail if no group id given
        if (!$id || empty($id)) {
            redirect(base_url(), 'refresh');
        }

        $this->data['title'] = $this->lang->line('edit_group_title');

        $group = $this->ion_auth->group($id)->row();

        //validate form input
        $this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|xss_clean');
        $this->form_validation->set_rules('group_description', $this->lang->line('edit_group_validation_desc_label'), 'xss_clean');

        if (isset($_POST) && !empty($_POST)) {
            if ($this->form_validation->run() === TRUE) {
                $group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

                if ($group_update) {
                    $this->session->set_flashdata('success', $this->lang->line('edit_group_saved'));
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->errors());
                }
                redirect(base_url("userGroups"), 'refresh');
            }
        }

        //set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        //pass the user to the view
        $this->data['group'] = $group;

        $this->data['group_name'] = array(
            'name' => 'group_name',
            'id' => 'group_name',
            "class" => "form-control",
            'type' => 'text',
            'value' => $this->form_validation->set_value('group_name', $group->name),
        );
        $this->data['group_description'] = array(
            'name' => 'group_description',
            'id' => 'group_description',
            "class" => "form-control",
            'type' => 'text',
            'value' => $this->form_validation->set_value('group_description', $group->description),
        );

        $this->data["page_head"] = "Edit User Group - " . ucfirst($group->name);


        $this->template->set_breadcrumb("Edit User Group", site_url("userGroups/editGroup/$id"));
        $this->template->title($this->data['title'])
                ->set_layout('form_layout')
                ->build('edit_group', $this->data);
    }
    
    public function deleteUserGroup($id = NULL) {
        if ($id == NULL)
            redirect(base_url("schemes"), "refresh");

        $this->data["group"] = $this->db->get_where("groups", array("id" => $id))->row();

        if (empty($this->data["group"]))
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to delete the Scheme details.');
        else {
            if ($this->db->delete("groups", array("id" => $id))) {
                $this->session->set_flashdata('success', 'User Group deleted Successfully.');
            }
        }
        redirect(base_url("userGroups"), "refresh");
    }


}
