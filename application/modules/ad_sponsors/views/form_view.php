

<div class="form-group col-md-12">
    <label for="exampleInputEmail1">Sponsor's VAT/PAN</label>
    <input type="text" class="form-control" placeholder="Sponsor's VAT/PAN" name="sponsor_vat" id="sponsor_vat" value="<?php echo !empty($sponsor->sponsor_vat) ? $sponsor->sponsor_vat : "" ?>" required="required">
</div>
<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Sponsor's Name</label>
    <input type="text" class="form-control" placeholder="Sponsor's Name" name="sponsor_name" id="sponsor_name" value="<?php echo !empty($sponsor->sponsor_name) ? $sponsor->sponsor_name : "" ?>" required="required">
</div>

<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Sponsor's Address</label>
    <input type="text" class="form-control" placeholder="Sponsor's Address" name="address" id="address" value="<?php echo !empty($sponsor->address) ? $sponsor->address : "" ?>" required="required">
</div>


<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Registered Date</label>
    <input type="text" class="form-control date-picker" placeholder="Registered Date" name="registered_date" id="registered_date" value="<?php echo !empty($sponsor->registered_date) ? $sponsor->registered_date : "" ?>" required="required">
</div>


<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Contact</label>
    <input type="text" class="form-control" placeholder="Sponsors's contact" id="contact" name="contact" value="<?php echo !empty($sponsor->contact) ? $sponsor->contact : "" ?>" required="required">
</div> 
<div class="form-group col-md-10 margin-bottom-40">
    &nbsp;
    <input type="hidden" class="form-control" name="sponsor_id" id="sponsor_id" value="<?php echo!empty($sponsor->sponsor_id) ? $sponsor->sponsor_id : "" ?>">
</div>
<div class="col-md-2 margin-bottom-40">
    <button type="submit" class="btn btn-success f-right margin-right-3" >Submit</button>

</div>



<script type="text/javascript">

    $(document).ready(function () {
        $("#form").validate({
            rules: {
                sponsor_name: {
                    required: true,
//                    email: true
                },
                address: {
                    required: true,
//                    email: true
                },
                registered_date: {
                    required: true,
//                    email: true
                },
                contact: {
                    required: true,
//                    equalTo: '#exampleInputPassword1'
                }
            }
        });
        
        $(".date-picker").datepicker({
            format: "yyyy-m-d",
            //startDate: new Date(),
            orientation: "top auto",
            autoclose: true
        });


    });

</script>
