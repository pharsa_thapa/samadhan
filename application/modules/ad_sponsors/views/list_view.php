<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">



                    <div class="table-responsive">
                        <table id="example" class="display table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Sponsor's Name</th>
                                    <th>Sponsor's Address</th>
                                    <th>Registered Date</th>
                                    <th>Sponsor's Phone</th>
                                    <th>VAT/PAN</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                <tr>
                                    <th>Sponsor's Name</th>
                                    <th>Sponsor's Address</th>
                                    <th>Registered Date</th>
                                    <th>Sponsor's Phone</th>
                                    <th>VAT/PAN</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($records as $record) : ?>
                                    <tr>

                                        <td><?php echo $record->sponsor_name; ?></td>
                                        <td><?php echo $record->address; ?></td>
                                        <td><?php echo $record->registered_date; ?></td>
                                        <td><?php echo $record->contact?></td>
                                        <td><?php echo $record->sponsor_vat?></td>
                                        <td>
                                            
                                            <a href="<?php echo base_url("ad_sponsors/edit/$record->sponsor_id"); ?>">
                                                Edit
                                            </a> 
                                            | 
                                            <a href="<?php echo base_url("ad_sponsors/delete/$record->sponsor_id"); ?>" 
                                               onclick="javascript : return confirm('Are you sure, you want to delete this record permanently?');">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table> 
                    </div>



                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->