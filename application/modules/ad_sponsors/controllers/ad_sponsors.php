<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ad_sponsors extends Frontend_Controller {

    public $validation_rules = array(
        'sponsor_vat' => array(
            'field' => 'sponsor_vat',
            'label' => "Sponsor's VAT Or PAN",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'sponsor_name' => array(
            'field' => 'sponsor_name',
            'label' => "Sponsor's Name",
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        
        'registered_date' => array(
            'field' => 'registered_date',
            'label' => 'Registered Date',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'address' => array(
            'field' => 'address',
            'label' => 'Sponsor\'s Address',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
        'contact' => array(
            'field' => 'contact',
            'label' => 'Contact',
            'rules' => 'trim|htmlspecialchars|required|xss_clean|max_length[100]'
        ),
    );

    public function __construct() {
        parent::__construct();
        $this->template->set_breadcrumb('Home', base_url());
        $this->template->set_breadcrumb("Subscribers", site_url("clients"));
    }

    public function index() {
        $this->data['records'] = $this->db->get("ad_sponsors")->result();
        
        $this->template->set_breadcrumb("Advertisement Sponsors", site_url("ad_sponsors"));
        $this->data['page_head'] = "All Advertisement Sponsors' List";
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('list_view', $this->data);
    }

    public function add() {
//        $this->data['districts'] = $this->db->get("districts")->result();
//        $this->data['gvs'] = $this->db->get("gvs")->result();

        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validation_rules);
        if ($this->form_validation->run() == TRUE) {

            // preparing for insertion
            foreach ($this->validation_rules as $k => $v) {
                $insert_data [$k] = $this->input->post($k);
            }
            if ($this->db->insert("ad_sponsors", $insert_data)) {
                $this->session->set_flashdata('success', 'New Sponsor\'s Details Added Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to add the Sponsor\'s details.');
            }
            redirect('ad_sponsors/add/', 'refresh');
        } else {
            $sponsor = new stdClass();
            // Go through all the known fields and get the post values
            foreach ($this->validation_rules as $key => $field) {
                $sponsor->$field['field'] = set_value($field['field']);
            }
            $this->data["sponsor"] = $sponsor;
        }

        $this->template->set_breadcrumb("Add Advertisement Sponsor", site_url("ad_sponsors/add"));
        $this->data['page_head'] = "Add Advertisement Sponsors' Details";
        $this->template->title('Title')
                ->set_layout('form_layout')
                ->build('form_view', $this->data);
    }

    public function edit($id = NULL) {
//        $this->data['districts'] = $this->db->get("districts")->result();
//        $this->data['gvss'] = $this->db->get("gvs")->result();

        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->validation_rules);

        if ($this->form_validation->run() == TRUE) {
            // preparing for insertion
            foreach ($this->validation_rules as $k => $v) {
                $update_data [$k] = $this->input->post($k);
            }
            if ($this->db->update("ad_sponsors", $update_data, array("sponsor_id" => $this->input->post("sponsor_id")))) {
                $this->session->set_flashdata('success', 'Sponsor\'s Details updated Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to update the Sponsor\'s details.');
            }

            redirect('ad_sponsors/index', 'refresh');
        } else {
            if ($id == NULL)
                redirect(base_url("ad_sponsors"), "refresh");

            $this->data["sponsor"] = $this->db->get_where("ad_sponsors", array("sponsor_id" => $id))->row();

            if (empty($this->data["sponsor"]))
                redirect(base_url("ad_sponsors"), "refresh");
        
            $this->template->set_breadcrumb("Edit Advertisement Sponsor", site_url("ad_sponsors/edit/$id"));
            $this->data['page_head'] = "Edit Advertisement Sponsors' Details -- {$this->data["sponsor"]->sponsor_name} ";
            $this->template->title('Title')
                    ->set_layout('form_layout')
                    ->build('form_view', $this->data);
        }
    }

    /*
     * Delete record
     */

    public function delete($id = NULL) {
        if ($id == NULL)
            redirect(base_url("ad_sponsors"), "refresh");

        $this->data["sponsor"] = $this->db->get_where("ad_sponsors", array("sponsor_id" => $id))->row();

        if (empty($this->data["sponsor"]))
            $this->session->set_flashdata('error', 'Sorry, Something went wrong!! <br> Unable to delete the Advertisement Sponsor\'s details.');
        else {
            if ($this->db->delete("ad_sponsors", array("sponsor_id" => $id))) {
                $this->session->set_flashdata('success', 'Advertisement Sponsor\'s Details deleted Successfully.');
            }
        }
        redirect(base_url("ad_sponsors"), "refresh");
    }

    /*
      detail of the records
      Report
     */

    /*
     * single status change
     */

    public function changestatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->uri->segment(4);

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

    /*
     * for bulk status change
     */

    public function bulkstatus() {
        $task = $this->uri->segment(3);
        $status = ($task == 'activate') ? '1' : '0';
        $id = $this->input->post('id');

        $flag = $this->updates_model->changeStatus($id, $status);

        if ($flag) {
            $this->session->set_flashdata('success', 'Record ' . ucfirst($task) . 'd' . ' Successfully.');
        } else
            $this->session->set_flashdata('error', 'Status operation failed. Please try again later.');

        redirect('updates/index', 'refresh'); //. $uripart
    }

}
