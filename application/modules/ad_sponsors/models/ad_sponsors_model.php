<?php

class Ad_sponsors_model extends MY_Model {

    public $_table_name = 'ad_sponsors';
    protected $_primary_key = 'ad_sponsor_id';
    protected $_primary_filter = 'intval';

    public function __construct() {
        parent::__construct();
    }

    public function allSponsors() {
        $query = "SELECT *, ("
                . " SELECT sum(tx.remaining_amount) "
                . "FROM transactions tx "
                . "WHERE tx.client_sponsor_id = t.client_sponsor_id"
                . ")"
                . "AS remaining_total "
                . "FROM (`ad_sponsors` a) "
                . "JOIN `transactions` t "
                . "ON `t`.`client_sponsor_id`=`a`.`sponsor_id` "
                . "WHERE `t`.`reference_type` =  '2'"
                . " GROUP BY a.sponsor_id"
                . "";
        return $this->db->query($query)->result();
    }

}
