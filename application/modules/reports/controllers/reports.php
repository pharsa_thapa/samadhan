<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends Frontend_Controller {

    public function __construct() {
        parent::__construct();
        $this->template->set_breadcrumb('Home', base_url());
        $this->template->set_breadcrumb("Reports", site_url('#'));
        $this->load->model("reports_model");
    }

    /*
     * location wise listing and filter form
     */

    public function index() {

        $this->data['districts'] = $this->db->get("districts")->result();
        $this->data['gvss'] = $this->db->get("gvs")->result();
        $this->data['records'] = $this->reports_model->allClients();
        $this->data['distributors'] = $this->db->get("distributors")->result();


        $retrieve = new stdClass();
        $retrieve->district = 0;
        $retrieve->gvs = 0;
        $retrieve->ward = 0;
        $retrieve->flag = 0;
        $this->data['retrieval'] = $retrieve;

        $this->template->set_breadcrumb("All Subscritpions", site_url('reports'));
        $this->data['page_head'] = "All Subscribers Report";
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('location_list_view', $this->data);
    }

    public function renewableSubscribers() {
        $this->data['records'] = $this->reports_model->allRenewableClients();
        $this->data['distributors'] = $this->db->get("distributors")->result();
        $this->data['page_head'] = "All Renewable Subscribers Report";

        $this->template->set_breadcrumb("All Renewable Subscritpions", site_url('reports/renewableSubscribers'));
        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('distributor_list_view', $this->data);
    }

    /*
     * category is all clients or renewable clients 
     * all for all clients
     * renewable for renewable clients
     */

    public function generateReport($category) {
        if (!$this->input->post() || !in_array($category, array("locationWise", "distributorWise")))
            redirect(base_url("reports", "refresh"));

        $distributor = $this->input->post("distributor_id");

        if ($category == "distributorWise") {
            if ($distributor != 0) {
                $flag = $this->input->post("flag");
                $this->subscribersByDistributor($distributor, $flag);
            }
            if ($distributor == 0) {
                $flag = $this->input->post("flag");
                $this->subscribersByDistributor(NULL, $flag);
            }
        }
        if ($category == "locationWise") {
            $district = $this->input->post("district");
            $gvs = $this->input->post("gvs");
            $ward = $this->input->post("ward");
            $flag = $this->input->post("flag");

            $this->subscribersByLocation($district, $gvs, $ward, $flag);
        }
    }

    /*
     * Distributor wise listing and filter form
     */

    public function subscribersByDistributor($distributor = NULL, $flag = NULL) {

        $this->template->set_breadcrumb("Distributorwise Subscritpions", site_url('reports/subscribersByDistributor'));
        $this->data['page_head'] = "Distributor Wise All Subscribers' Report";

        if ($flag == 0) {
            $this->data['records'] = $this->reports_model->allClientsByDistributor($distributor);

            $this->template->set_breadcrumb("Distributorwise All Subscritpions", site_url("reports/subscribersByDistributor/$distributor/0"));
            $this->data['page_head'] = "Distributor Wise All Subscribers' Report - All  subscribers ";
            $this->data['page_head'] .= $distributor != 0 ? ('-' . $this->data['records'][0]->distributor_name) : "";
        }

        if ($flag == 1) {
            $this->data['records'] = $this->reports_model->allRenewableClientsByDistributor($distributor);
            $this->template->set_breadcrumb("Distributorwise All Renewable Subscritpions", site_url("reports/subscribersByDistributor/$distributor/$flag"));
            $this->data['page_head'] = "Distributor Wise All Subscribers' Report - Renewable  subscribers ";
            $this->data['page_head'] .= $distributor != 0 ? ('-' . @$this->data['records'][0]->distributor_name) : "";
        }

        if ($flag == NULL && $distributor == NULL) {
            $this->data['records'] = $this->reports_model->allClients();
        }


        $retrieve = new stdClass();
        $retrieve->distributor_id = $distributor;
        $retrieve->flag = $flag;

        $this->data["retrieval"] = $retrieve;

        $this->data['distributors'] = $this->db->get("distributors")->result();


        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('distributor_list_view', $this->data);
    }

    public function subscribersByLocation($district_id = 0, $gvs_id = 0, $ward_no = 0, $flag = 0) {
        $this->data['districts'] = $this->db->get("districts")->result();
        $this->data['gvss'] = $this->db->get("gvs")->result();

//        if ($district_id == 0 && $gvs_id == 0 && $ward_no == 0 && $flag == 0) {
//            redirect("reports", "refresh");
//        }

        $this->data['page_head'] = "Location Wise All Subscribers' Report";

        if ($flag == 0) {
            $this->data['records'] = $this->reports_model->allSubscribersByLocation($district_id, $gvs_id, $ward_no);
            $this->data['page_head'] = "Location Wise All Subscribers' Report";
        }

        if ($flag == 1) {
            $this->data['records'] = $this->reports_model->allRenewableSubscribersByLocation($district_id, $gvs_id, $ward_no);
            $this->data['page_head'] = "Location Wise Renewable Subscribers' Report ";
        }

        if ($district_id != 0 && !empty($this->data['records'])) {
            $this->data['page_head'] .= ("--" . @$this->data['records'][0]->district_name);
        }
        if ($district_id != 0 && $gvs_id != 0 && !empty($this->data['records'])) {
            $this->data['page_head'] .= ("-" . @$this->data['records'][0]->gvs_name);
        }
        if ($district_id != 0 && $gvs_id != 0 && $ward_no != 0 && !empty($this->data['records'])) {
            $this->data['page_head'] .= ("-" . @$this->data['records'][0]->ward_no);
        }

        $retrieve = new stdClass();
        $retrieve->district = $district_id;
        $retrieve->gvs = $gvs_id;
        $retrieve->ward = $ward_no;
        $retrieve->flag = $flag;

        $this->data["retrieval"] = $retrieve;

        $this->template->title('Title')
                ->set_layout('data_table_layout')
                ->build('location_list_view', $this->data);
    }

    public function exportExcelByLocation($flag = 0, $district_id = 0, $gvs_id = 0, $ward_no = 0) {
        $district_name = ' All Districts ';
        $gvs_name = ' All GVS ';
        $ward_name = ' All wards ';

        if ($district_id == 0) {
            $district_name == " All Districts ";
        } else {
            $district_info = $this->db->get_where("districts", array("district_id" => $district_id))->row();
            $district_name = !empty($district_info) ? $district_info->district_name . " District " : " All Districts ";
        }

        if ($gvs_id == 0) {
            $gvs_name == " All GVS ";
        } else {
            $gvs_info = $this->db->get_where("gvs", array("gvs_id" => $gvs_id))->row();
            $gvs_name = !empty($gvs_info) ? $gvs_info->gvs_name . " GVS/Municipality " : " All GVS ";
        }

        if ($ward_no == 0) {
            $ward_name == " All wards ";
        } else {
            $ward_name = " $ward_no ward_no ";
        }

        if ($flag == 0) {
            $filename = " All Subscribers @ " . $district_name . $gvs_name . $ward_name;
            $this->exportExcel($this->reports_model->exportAllSubscribersByLocation($district_id, $gvs_id, $ward_no), $filename);
        }

        if ($flag == 1) {

            $filename = " Renewable Subscribers @ " . $district_name . $gvs_name . $ward_name;
            $this->exportExcel($this->reports_model->exportAllRenewableSubscribersByLocation($district_id, $gvs_id, $ward_no), $filename);
        }
    }

    public function exportExcelByDistributor($flag = 0, $distributor_id = 0) {
        $distributor_name = ' All Distributors ';

        if ($distributor_id == 0) {
            $distributor_id == " All Distributors ";
        } else {
            $distributor_info = $this->db->get_where("distributors", array("distributor_id" => $distributor_id))->row();
            $distributor_name = !empty($distributor_info) ? $distributor_info->distributor_name . " Distributor " : " All Distributors ";
        }

        if ($flag == 0) {
            $filename = " All Subscribers @ " . $distributor_name;
            $this->exportExcel($this->reports_model->exportAllClientsByDistributor($distributor_id), $filename);
        }

        if ($flag == 1) {
            $filename = " Renewable Subscribers @ " . $distributor_name;
            $this->exportExcel($this->reports_model->exportAllRenewableClientsByDistributor($distributor_id), $filename);
        }

        if ($flag == 0 && $distributor == 0) {
            $filename = " All Subscribers @ All Distributors ";
            $this->exportExcel($this->reports_model->exportAllClients(), $filename);
        }
    }

    public function exportExcel($queryObj, $file_name) {
        include_once './application/libraries/PHPExcel.php';
        include_once './application/libraries/PHPExcel/IOFactory.php';

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("DYNAMIC TITLE")->setDescription("SOME DEsCRIPTION");

        $objPHPExcel->setActiveSheetIndex(0);

        // Field names in the first row
        $fields = $queryObj->list_fields();
        $col = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }
        // Fetching the table data
        $row = 2;
        foreach ($queryObj->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
            $row++;
        }
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory :: createWriter($objPHPExcel, 'Excel5');

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $file_name . '_' . date("Y-m-d") . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }
    
    
   

}
