<?php

class Reports_model extends MY_Model {

    public $_table_name = 'clients';
    protected $_primary_key = 'client_id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'client_id';
    protected $_timestamps = FALSE;

    public function __construct() {
        parent::__construct();
    }

    public function allClients($distributor = 0) {
        $date = date('Y-m-d');
        $query = 'SELECT *, '
                . '(SELECT DATEDIFF( '
                . '(SELECT sc.subscription_to FROM subscriptions sc WHERE sc.client_id=s.client_id order by sc.subscription_id desc limit 1 ), "' . $date . '")'
                . ') AS expiry_days  FROM (`clients`) '
                . 'JOIN `districts` ON `districts`.`district_id` = `clients`.`district_id` '
                . 'JOIN `gvs` ON `gvs`.`gvs_id`= `clients`.`gvs_id` '
                . 'JOIN `distributors` ON `distributors`.`distributor_id`= `clients`.`distributor_id`'
                . 'JOIN subscriptions s ON s.client_id = clients.client_id '
                . ' AND s.client_id IS NOT NULL ';

        if ($distributor != 0) {
            $query.= " AND distributors.distributor_id = $distributor ";
        };
        $query.= 'GROUP BY s.client_id ';

        return $this->db->query($query)->result();
    }

    public function exportAllClients($distributor = 0) {
        $date = date('Y-m-d');
        $query = 'SELECT c.client_first_name as FIRST_NAME, c.client_last_name as LAST_NAME,c.phone as PHONE, districts.district_name as DISTRICT, gvs.gvs_name as GVS_MUNICIPALITY, distributors.distributor_name as DISTRIBUTOR,  c.ward_no as WARD_NO, '
                . '(SELECT DATEDIFF( '
                . '(SELECT sc.subscription_to FROM subscriptions sc WHERE sc.client_id=s.client_id order by sc.subscription_id desc limit 1 ), "' . $date . '")'
                . ') AS EXPIRY_DAYS  FROM clients c '
                . 'JOIN `districts` ON `districts`.`district_id` = c.`district_id` '
                . 'JOIN `gvs` ON `gvs`.`gvs_id`= c.`gvs_id` '
                . 'JOIN `distributors` ON `distributors`.`distributor_id`= c.`distributor_id`'
                . 'JOIN subscriptions s ON s.client_id = c.client_id '
                . ' AND s.client_id IS NOT NULL ';

        if ($distributor != 0) {
            $query.= " AND distributors.distributor_id = $distributor ";
        };
        $query.= 'GROUP BY s.client_id ';

        return $this->db->query($query);
    }

    public function allRenewableClients() {
        $date = date('Y-m-d');
        $query = 'SELECT * FROM (SELECT '
                . 'c.client_first_name, c.client_last_name,c.phone, districts.district_name, gvs.gvs_name, distributors.distributor_name, c.client_id, '
                . '(SELECT DATEDIFF( '
                . '(SELECT sc.subscription_to FROM subscriptions sc WHERE sc.client_id=s.client_id order by sc.subscription_id desc limit 1 ), "' . $date . '")'
                . ') AS expiry_days  FROM clients c '
                . 'JOIN `districts` ON `districts`.`district_id` = `c`.`district_id` '
                . 'JOIN `gvs` ON `gvs`.`gvs_id`= `c`.`gvs_id` '
                . 'JOIN `distributors` ON `distributors`.`distributor_id`= c.distributor_id '
                . 'JOIN subscriptions s ON s.client_id = c.client_id '
                . ' AND s.client_id IS NOT NULL '
                . 'GROUP BY s.client_id) AS temp_table where expiry_days <= 15 ';

        return $this->db->query($query)->result();
    }

    public function allClientsByDistributor($distributor) {
        $date = date('Y-m-d');
        $query = 'SELECT *, '
                . '(SELECT DATEDIFF( '
                . '(SELECT sc.subscription_to FROM subscriptions sc WHERE sc.client_id=s.client_id order by sc.subscription_id desc limit 1 ), "' . $date . '")'
                . ') AS expiry_days  FROM (`clients`) '
                . 'JOIN `districts` ON `districts`.`district_id` = `clients`.`district_id` '
                . 'JOIN `gvs` ON `gvs`.`gvs_id`= `clients`.`gvs_id` '
                . 'JOIN `distributors` ON `distributors`.`distributor_id`= `clients`.`distributor_id`'
                . 'JOIN subscriptions s ON s.client_id = clients.client_id '
                . ' AND s.client_id IS NOT NULL ';

        if ($distributor != 0) {
            $query.= " AND distributors.distributor_id = $distributor ";
        };
        $query.= 'GROUP BY s.client_id ';

        return $this->db->query($query)->result();
    }

    public function exportAllClientsByDistributor($distributor) {
        $date = date('Y-m-d');
        $query = 'SELECT c.client_first_name as FIRST_NAME, c.client_last_name as LAST_NAME,c.phone as PHONE, districts.district_name as DISTRICT, gvs.gvs_name as GVS_MUNICIPALITY, distributors.distributor_name as DISTRIBUTOR,  c.ward_no as WARD_NO, '
                . '(SELECT DATEDIFF( '
                . '(SELECT sc.subscription_to FROM subscriptions sc WHERE sc.client_id=s.client_id order by sc.subscription_id desc limit 1 ), "' . $date . '")'
                . ') AS EXPIRY_DAYS  FROM clients c '
                . 'JOIN `districts` ON `districts`.`district_id` = c.`district_id` '
                . 'JOIN `gvs` ON `gvs`.`gvs_id`= c.`gvs_id` '
                . 'JOIN `distributors` ON `distributors`.`distributor_id`= c.`distributor_id`'
                . 'JOIN subscriptions s ON s.client_id = c.client_id '
                . ' AND s.client_id IS NOT NULL ';

        if ($distributor != 0) {
            $query.= " AND distributors.distributor_id = $distributor ";
        };
        $query.= 'GROUP BY s.client_id ';

        return $this->db->query($query);
    }

    public function allRenewableClientsByDistributor($distributor) {
        $date = date('Y-m-d');
        $query = 'SELECT * FROM (SELECT '
                . 'c.client_first_name, c.client_last_name,c.phone, districts.district_name, gvs.gvs_name, distributors.distributor_name, c.client_id, '
                . '(SELECT DATEDIFF( '
                . '(SELECT sc.subscription_to FROM subscriptions sc WHERE sc.client_id=s.client_id order by sc.subscription_id desc limit 1 ), "' . $date . '")'
                . ') AS expiry_days  FROM clients c '
                . 'JOIN `districts` ON `districts`.`district_id` = `c`.`district_id` '
                . 'JOIN `gvs` ON `gvs`.`gvs_id`= `c`.`gvs_id` '
                . 'JOIN `distributors` ON `distributors`.`distributor_id`= c.distributor_id '
                . 'JOIN subscriptions s ON s.client_id = c.client_id '
                . ' AND s.client_id IS NOT NULL ';
        if ($distributor != 0) {
            $query.= " AND distributors.distributor_id = $distributor ";
        };
        $query.= 'GROUP BY s.client_id) AS temp_table where expiry_days <= 15 ';

        return $this->db->query($query)->result();
    }

    public function exportAllRenewableClientsByDistributor($distributor) {
        $date = date('Y-m-d');
        $query = 'SELECT * FROM (SELECT '
                . 'c.client_first_name as FIRST_NAME, c.client_last_name as LAST_NAME,c.phone as PHONE, districts.district_name as DISTRICT, gvs.gvs_name as GVS_MUNICIPALITY, distributors.distributor_name as DISTRIBUTOR,  c.ward_no as WARD_NO, '
                . '(SELECT DATEDIFF( '
                . '(SELECT sc.subscription_to FROM subscriptions sc WHERE sc.client_id=s.client_id order by sc.subscription_id desc limit 1 ), "' . $date . '")'
                . ') AS EXPIRY_DAYS  FROM clients c '
                . 'JOIN `districts` ON `districts`.`district_id` = `c`.`district_id` '
                . 'JOIN `gvs` ON `gvs`.`gvs_id`= `c`.`gvs_id` '
                . 'JOIN `distributors` ON `distributors`.`distributor_id`= c.distributor_id '
                . 'JOIN subscriptions s ON s.client_id = c.client_id '
                . ' AND s.client_id IS NOT NULL ';
        if ($distributor != 0) {
            $query.= " AND distributors.distributor_id = $distributor ";
        };
        $query.= 'GROUP BY s.client_id) AS temp_table where EXPIRY_DAYS <= 15 ';

        return $this->db->query($query);
    }

    public function allSubscribersByLocation($district_id, $gvs_id, $ward_no) {

        $date = date('Y-m-d');
        $query = 'SELECT c.client_first_name, c.client_last_name,c.phone, districts.district_name, gvs.gvs_name, distributors.distributor_name, c.client_id,c.ward_no, '
                . '(SELECT DATEDIFF( '
                . '(SELECT sc.subscription_to FROM subscriptions sc WHERE sc.client_id=s.client_id order by sc.subscription_id desc limit 1 ), "' . $date . '")'
                . ') AS expiry_days  FROM clients c '
                . 'JOIN `districts` ON `districts`.`district_id` = `c`.`district_id` '
                . 'JOIN `gvs` ON `gvs`.`gvs_id`= `c`.`gvs_id` '
                . 'JOIN `distributors` ON `distributors`.`distributor_id`= `c`.`distributor_id`'
                . 'JOIN subscriptions s ON s.client_id = c.client_id '
                . ' AND s.client_id IS NOT NULL ';

        if ($district_id != 0) {
            $query.= " AND c.district_id = $district_id ";
        };
        if ($gvs_id != 0) {
            $query.= " AND c.gvs_id = $gvs_id ";
        };
        if ($ward_no != 0) {
            $query.= " AND c.ward_no = $ward_no ";
        };
        $query.= 'GROUP BY s.client_id ';

        return $this->db->query($query)->result();
    }

    public function allRenewableSubscribersByLocation($district_id, $gvs_id, $ward_no) {
        $date = date('Y-m-d');
        $query = 'SELECT * FROM (SELECT '
                . 'c.client_first_name, c.client_last_name,c.phone, districts.district_name, gvs.gvs_name, distributors.distributor_name, c.client_id, c.ward_no, '
                . '(SELECT DATEDIFF( '
                . '(SELECT sc.subscription_to FROM subscriptions sc WHERE sc.client_id=s.client_id order by sc.subscription_id desc limit 1 ), "' . $date . '")'
                . ') AS expiry_days  FROM clients c '
                . 'JOIN `districts` ON `districts`.`district_id` = `c`.`district_id` '
                . 'JOIN `gvs` ON `gvs`.`gvs_id`= `c`.`gvs_id` '
                . 'JOIN `distributors` ON `distributors`.`distributor_id`= c.distributor_id '
                . 'JOIN subscriptions s ON s.client_id = c.client_id '
                . ' AND s.client_id IS NOT NULL ';
        if ($district_id != 0) {
            $query.= " AND c.district_id = $district_id ";
        };
        if ($gvs_id != 0) {
            $query.= " AND c.gvs_id = $gvs_id ";
        };
        if ($ward_no != 0) {
            $query.= " AND c.ward_no = $ward_no ";
        };
        $query.= 'GROUP BY s.client_id) AS temp_table where expiry_days <= 15 ';

        return $this->db->query($query)->result();
    }

    public function exportAllRenewableSubscribersByLocation($district_id, $gvs_id, $ward_no) {
        $date = date('Y-m-d');
        $query = 'SELECT * FROM (SELECT '
                . 'c.client_first_name as FIRST_NAME, c.client_last_name as LAST_NAME,c.phone as PHONE, districts.district_name as DISTRICT, gvs.gvs_name as GVS_MUNICIPALITY, distributors.distributor_name as DISTRIBUTOR,  c.ward_no as WARD_NO, '
                . '(SELECT DATEDIFF( '
                . '(SELECT sc.subscription_to FROM subscriptions sc WHERE sc.client_id=s.client_id order by sc.subscription_id desc limit 1 ), "' . $date . '")'
                . ') AS EXPIRE_DAYS,  FROM clients c '
                . 'JOIN `districts` ON `districts`.`district_id` = `c`.`district_id` '
                . 'JOIN `gvs` ON `gvs`.`gvs_id`= `c`.`gvs_id` '
                . 'JOIN `distributors` ON `distributors`.`distributor_id`= c.distributor_id '
                . 'JOIN subscriptions s ON s.client_id = c.client_id '
                . ' AND s.client_id IS NOT NULL ';
        if ($district_id != 0) {
            $query.= " AND c.district_id = $district_id ";
        };
        if ($gvs_id != 0) {
            $query.= " AND c.gvs_id = $gvs_id ";
        };
        if ($ward_no != 0) {
            $query.= " AND c.ward_no = $ward_no ";
        };
        $query.= 'GROUP BY s.client_id) AS temp_table where EXPIRE_DAYS <= 15 ';

        return $this->db->query($query);
    }

    public function exportAllSubscribersByLocation($district_id, $gvs_id, $ward_no) {

        $date = date('Y-m-d');
        $query = 'SELECT c.client_first_name as FIRST_NAME, c.client_last_name as LAST_NAME,c.phone as PHONE, districts.district_name as DISTRICT, gvs.gvs_name as GVS_MUNICIPALITY, distributors.distributor_name as DISTRIBUTOR,  c.ward_no as WARD_NO, '
                . '(SELECT DATEDIFF( '
                . '(SELECT sc.subscription_to FROM subscriptions sc WHERE sc.client_id=s.client_id order by sc.subscription_id desc limit 1 ), "' . $date . '")'
                . ') AS EXPIRE_DAYS  FROM clients c '
                . 'JOIN `districts` ON `districts`.`district_id` = `c`.`district_id` '
                . 'JOIN `gvs` ON `gvs`.`gvs_id`= `c`.`gvs_id` '
                . 'JOIN `distributors` ON `distributors`.`distributor_id`= `c`.`distributor_id`'
                . 'JOIN subscriptions s ON s.client_id = c.client_id '
                . ' AND s.client_id IS NOT NULL ';

        if ($district_id != 0) {
            $query.= " AND c.district_id = $district_id ";
        };
        if ($gvs_id != 0) {
            $query.= " AND c.gvs_id = $gvs_id ";
        };
        if ($ward_no != 0) {
            $query.= " AND c.ward_no = $ward_no ";
        };
        $query.= 'GROUP BY s.client_id ';

        return $this->db->query($query);
    }

    public function countAlerts() {
        $date = date('Y-m-d');
        $query = 'SELECT count(*) as count FROM (SELECT '
                . ' c.client_id , '
                . '(SELECT DATEDIFF( '
                . '(SELECT sc.subscription_to FROM subscriptions sc WHERE sc.client_id=s.client_id order by sc.subscription_id desc limit 1 ), "' . $date . '")'
                . ') AS EXPIRE_DAYS FROM clients c '
                . 'JOIN `districts` ON `districts`.`district_id` = `c`.`district_id` '
                . 'JOIN `gvs` ON `gvs`.`gvs_id`= `c`.`gvs_id` '
                . 'JOIN `distributors` ON `distributors`.`distributor_id`= c.distributor_id '
                . 'JOIN subscriptions s ON s.client_id = c.client_id '
                . ' AND s.client_id IS NOT NULL ';
        $query.= 'GROUP BY s.client_id) AS temp_table where EXPIRE_DAYS <= 15 ';

        return $this->db->query($query)->row();
    }

    public function recentClients() {
        $query = 'SELECT c.client_first_name, c.client_last_name,c.phone, districts.district_name, gvs.gvs_name, distributors.distributor_name, c.client_id, c.registered_date '
                . ' FROM clients c '
                . 'JOIN `districts` ON `districts`.`district_id` = `c`.`district_id` '
                . 'JOIN `gvs` ON `gvs`.`gvs_id`= `c`.`gvs_id` '
                . 'JOIN `distributors` ON `distributors`.`distributor_id`= `c`.`distributor_id`'
                . 'JOIN subscriptions s ON s.client_id = c.client_id '
                . ' AND s.client_id IS NOT NULL '
                . 'GROUP BY s.client_id order by c.client_id desc limit 10 ';
        return $this->db->query($query)->result();
    }

}
