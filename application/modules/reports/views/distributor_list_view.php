<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <form method="post" action="<?php echo base_url("reports/generateReport/distributorWise") ?>" id="form" style="display: block !important;">
                    <div class="col-md-12 col-sm-12">    
                        <div class="form-group col-md-5">
                            <select class="form-control" name="distributor_id">
                                <option value="0" >All Distributors</option>
                                <?php
                                foreach ($distributors as $distributor) {
                                    ?>
                                    <option value="<?php echo $distributor->distributor_id ?>" <?php echo @$retrieval->distributor_id == $distributor->distributor_id ? "selected='selected'" : '' ?> >
                                        <?php echo $distributor->distributor_name; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-5">
                            <select class="form-control" name="flag">
                                <option value="0" <?php echo @$retrieval->flag == 0 ? "selected='selected'" : '' ?>>All Subscribers</option>
                                <option value="1" <?php echo @$retrieval->flag == 1 ? "selected='selected'" : '' ?>>Renewable Subscribers</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-2 col-md-12 col-sm-12">
                            <button  type="submit" form="form" value="Submit" class="btn btn-success"> &emsp; &emsp;&emsp;  Filter  &emsp; &emsp; &emsp;</button>
                        </div>

                    </div>
                </form>

                <div class="col-md-12 col-sm-12 margin-bottom-5">
                    <div class="col-md-9">&emsp;</div>
                    <div class="col-md-3">
                        <div class="col-md-12">
                            <a href="<?php echo base_url("reports/exportExcelByDistributor/" . @$retrieval->flag . '/' . @$retrieval->distributor_id); ?>" class="col-md-6">
                                <span class="btn btn-info">
                                    Export Excel
                                </span>
                            </a>

                            <a href="#" class="col-lg-6">
                                <span class="btn btn-info ">
                                    &nbsp;&nbsp;Export PDF &nbsp;
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-white">

                <div class="panel-body">

                    <div class="table-responsive">
                        <table id="example" class="display table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Clients Full Name</th>
                                    <th>Phone Number</th>
                                    <th>District</th>
                                    <th>GVS/Municipality</th>
                                    <!--<th>Other Address</th>-->
                                    <th>Distributor's Name</th>
                                    <th>Subscription Expires In</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Clients Full Name</th>
                                    <th>Phone Number</th>
                                    <th>District</th>
                                    <th>GVS/Municipality</th>
                                    <!--<th>Other Address</th>-->
                                    <th>Distributor's Name</th>
                                    <th>Subscription Expires In</th>
                                    <th>Actions</th>

                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($records as $record) : ?>
                                    <tr>

                                        <td><?php echo $record->client_first_name . ' ' . $record->client_last_name ?> </td>
                                        <td><?php echo $record->phone ?></td>
                                        <td><?php echo $record->district_name; ?></td>
                                        <td><?php echo $record->gvs_name ?></td>
                                        <!--<td><?php // echo $record->address                                ?></td>-->
                                        <!--<td><?php // echo $record->registered_date                                 ?></td>-->
                                        <td><?php echo $record->distributor_name ?></td>
                                        <td <?php if ($record->expiry_days <= 15) { ?>style="color:red" <?php } ?>><?php echo $record->expiry_days > 0 ? $record->expiry_days . " days" : "Subscription Expired!" ?></td>
                                        <td>

                                            <a href="<?php echo base_url("clients/detail/$record->client_id"); ?>">
                                                View Report
                                            </a> 
                                            |
                                            <a href="<?php echo base_url("clients/renewSubscription/reports/$record->client_id"); ?>">
                                                Renew
                                            </a> 

                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->