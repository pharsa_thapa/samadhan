<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Client's Name</label>
    <br>
    <span><?php echo $client->client_first_name." ".$client->client_last_name ?></span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Contact</label>
    <br>
    <span><?php echo $client->phone ?></span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputEmail1">Registered Date</label>
    <br>
    <span> <?php echo!empty($client->registered_date) ? $client->registered_date : "" ?> </span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Address</label>
    <br>
    <span>
        <?php echo $client->gvs_name . " - " . $client->ward_no . ", " . $client->district_name; ?> 
    </span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Other Address</label>
    <br>
    <span><?php echo ($client->address) ?></span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Distributor</label>
    <br>
    <span><?php echo ($client->distributor_name) ?></span>
</div>


<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Scheme Name</label>
    <br>
    <span><?php echo !empty($client->scheme_name) ? $client->scheme_name: "----------"?></span>
</div>
<div class="form-group col-md-6">
    <label for="exampleInputPassword1">Scheme Claimed Date</label>
    <br>
    <span><?php echo !empty($client->scheme_claimed_date) ? $client->scheme_claimed_date: "----------"?></span>
</div>

<div class="form-group col-md-6">
    <h3>Subscription Details</h3>
</div>