<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <form method="post" action="<?php echo base_url("reports/generateReport/locationWise") ?>" id="form" style="display: block !important;">
                    <div class="col-md-12 col-sm-12">    

                        <div class="form-group col-md-3">
                            <select class="form-control" name="district" id="district">
                                <option value="0" >All Districts</option>
                                <?php foreach ($districts as $district) {
                                    ?>
                                    <option value="<?php echo $district->district_id ?>" <?php echo @$retrieval->district == $district->district_id ? "selected='selected'" : '' ?>><?php echo $district->district_name; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <select class="form-control" name="gvs" id="gvs">
                                <option value="0" >All GVS/Municipality</option>
                                <?php foreach ($gvss as $gvs) : ?>
                                    <option value='<?php echo $gvs->gvs_id ?>' <?php echo @$retrieval->gvs == $gvs->gvs_id ? "selected='selected'" : '' ?>>
                                        <?php echo $gvs->gvs_name ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>

                            </select>
                        </div>
                        <div class="form-group col-md-2">   
                            <select class="form-control" name="ward" id="ward">
                                <option value="0" >All Wards</option>
                                <?php for ($counter = 1; $counter <= 20; $counter++) : ?>
                                    <option value="<?php echo $counter; ?> " <?php echo @$retrieval->ward == $counter ? "selected='selected'" : '' ?>>
                                        <?php echo $counter; ?>
                                    </option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <select class="form-control" name="flag">
                                <option value="0" <?php echo @$retrieval->flag == 0 ? "selected='selected'" : '' ?>>All Subscribers</option>
                                <option value="1" <?php echo @$retrieval->flag == 1 ? "selected='selected'" : '' ?>>Renewable Subscribers</option>
                            </select>
                        </div>


                        <div class="form-group col-lg-2 col-md-12 col-sm-12">
                            <button  type="submit" form="form" value="Submit" class="btn btn-success"> &emsp; &emsp;&emsp;  Filter  &emsp; &emsp; &emsp;</button>
                        </div>

                    </div>
                </form>

                <div class="col-md-12 col-sm-12 margin-bottom-5">
                    <div class="col-md-9">&emsp;</div>
                    <div class="col-md-3">
                        <div class="col-md-12">
                            <a href="<?php
                            $url = $retrieval->flag . '/';
                            if ($retrieval->district != 0) {
                                $url.= ($retrieval->district . '/');
                                if ($retrieval->gvs != 0) {
                                    $url.= ($retrieval->gvs . '/');
                                    if ($retrieval->ward != 0) {
                                        $url.= ($retrieval->ward . '/');
                                    }
                                }
                            }

                            echo base_url("reports/exportExcelByLocation/$url");
                            ?>" class="col-md-6">
                                <span class="btn btn-info">
                                    Export Excel  
                                </span>
                            </a>

                            <a href="#" class="col-lg-6">
                                <span class="btn btn-info ">
                                    &nbsp;&nbsp;Export PDF &nbsp;
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-white">

                <div class="panel-body">

                    <div class="table-responsive">
                        <table id="example" class="display table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Clients Full Name</th>
                                    <th>Phone Number</th>
                                    <th>District</th>
                                    <th>GVS/Municipality</th>
                                    <th>Ward No</th>
                                    <th>Distributor's Name</th>
                                    <th>Subscription Expires In</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Clients Full Name</th>
                                    <th>Phone Number</th>
                                    <th>District</th>
                                    <th>GVS/Municipality</th>
                                    <th>Ward No</th>
                                    <th>Distributor's Name</th>
                                    <th>Subscription Expires In</th>
                                    <th>Actions</th>

                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($records as $record) : ?>
                                    <tr>

                                        <td><?php echo $record->client_first_name . ' ' . $record->client_last_name ?> </td>
                                        <td><?php echo $record->phone ?></td>
                                        <td><?php echo $record->district_name; ?></td>
                                        <td><?php echo $record->gvs_name ?></td>
                                        <td><?php echo $record->ward_no ?></td>

                                        <td><?php echo $record->distributor_name ?></td>
                                        <td <?php if ($record->expiry_days <= 15) { ?>style="color:red" <?php } ?>><?php echo $record->expiry_days > 0 ? $record->expiry_days . " days" : "Subscription Expired!" ?></td>
                                        <td>

                                            <a href="<?php echo base_url("clients/detail/$record->client_id"); ?>">
                                                View Report
                                            </a> 
                                            |
                                            <a href="<?php echo base_url("clients/renewSubscription/reports/$record->client_id"); ?>">
                                                Renew
                                            </a> 

                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Row -->
</div><!-- Main Wrapper -->



<script type="text/javascript">

    $(document).ready(function () {

        $("#district").on("change", function () {
            var requestGVS = $.ajax({
                type: "post",
                url: "<?php echo base_url("distributors/changeDistrict"); ?>", //Please cahnge this url on deployment
                data: {"district_id": $("#district").val()}

            });

            requestGVS.done(function (evt) {
                data = $.parseJSON(evt);
                if (data.result == 1) {
                    $("#gvs").html("<option value='0' >All GVS/Municipality</option>" + data.response);
                    $("#gvs").prop("disabled", false);
                    $("#submit").prop('disabled', false);
                } else {
                    $("#gvs").prop('selected', false)
                            .filter('[value="0"]')
                            .prop('selected', true);
                    $("#ward").prop('selected', false)
                            .filter('[value="0"]')
                            .prop('selected', true);
                    $("#submit").attr('disabled', 'disabled');
//                    $("#gvs").prop("disabled", true);
                }

            });

        });


        $("#gvs").on("change", function () {
            var requestDistrict = $.ajax({
                type: "post",
                url: "<?php echo base_url("distributors/changeGvs"); ?>", //Please cahnge this url on deployment
                data: {"gvs_id": $("#gvs").val()}

            });

            requestDistrict.done(function (evt) {
                data = $.parseJSON(evt);
                if (data.result == 1) {

                    $('#district option').prop('selected', false)
                            .filter('[value="' + data.response + '"]')
                            .prop('selected', true);
                    $("#gvs").prop("disabled", false);
                    $("#submit").prop('disabled', false);
                }
            });

        });



    });

</script>