<?php

/*
 * receives a bulk form input
 * validate each set of inputs
 * parse response
 * 
 */

Class Bulk_Form_Validation {

    //attributes for the class
    public $validation_rules = array();
    public $post_array;
    private $response = array();
    private $validationCounter;

    public function __construct() {
//        initialize validationCounter
        $this->validationCounter = 0;
    }

    /*
     * parse inputs called from the controller
     */

    public function parseBulkForm($input, $validation_rules) {
        //input is not set
        if (empty($input)) {
            return false;
        }

        $this->validation_rules = $validation_rules;
        //initialize an array to store parsed input
        $parsedArray = array();
        //loop all the inputs to change the looping name of input
        foreach ($input as $k => $v) {
            //save $k as array
            $tempVar = explode("_", $k);
            //store same array for further use
            $tempVar2 = $tempVar;
            //manipulate tempVar
            //pop last element
            $key = array_pop($tempVar);
            //push new key for validation rule
            $parsedArray[$tempVar2[sizeof($tempVar2) - 1]][implode("_", $tempVar)] = $v;
        }
        //set array value in post array
        $this->post_array = $parsedArray;
    }

    /*
     * get validation rule for regular name expression 
     */

    private function getValidationRules() {
        $newRule = array();
        $newIndex = '';
        //loop for each validation rule
        foreach ($this->validation_rules as $k => $v) {

            $indexArr = explode("_", $k);
            //remove last element indexArr
            array_pop($indexArr);
            //push looping counter for generating new validation index
            array_push($indexArr, $this->validationCounter);
            //generate new validation index
            $newIndex = implode("_", $indexArr);
            $newRuleSet = array();
            //iterate over the v in the outer loop
            foreach ($v as $key => $val) {
                //if the key is field
                if ($key == 'field') {

                    $tempArr = explode("_", $val);
                    array_pop($tempArr);
                    $index = $this->validationCounter;
                    //generate new array index field  value for the new rule
                    $newRule[$newIndex][$key] = implode("_", $tempArr) . '_' . $index;
                }
                if ($key == 'rules') {
                    $newRule[$newIndex][$key] = $val;
                }
                if ($key == "label") {
                    $newRule[$newIndex][$key] = $val;
                }
            }
            $tempArr = array();
        }
        //set validation rules
        $this->validation_rules = $newRule;
        $this->validationCounter++;
    }

    /*
     * validate this->input
     */

    private function validateInput() {
        $validInput = array();
        $invalidInput = array();
        $totalInput = sizeof($this->post_array);
        //loop over the post array
        foreach ($this->post_array as $instanceKey => $instanceVal) {
            //intanciate a new CI class
            $CI = &get_instance();
            //load form validation library
            $CI->load->library('form_validation');
            //reseting validation errors
            $CI->form_validation->resetValidationErrors();

            //get validation rules
            $this->getValidationRules();

            //set validation rule
            $CI->form_validation->set_rules($this->validation_rules);
            
            //run form validation
            $CI->form_validation->run();
            
            //check for validation errors
            if (validation_errors() == '') {
                //validation errors not found
                //push this set of input to valid inputs
                $validInput[] = $this->post_array[$instanceKey];
            } else {
                //validation errors found
                //push this set of input in invalid inputs
                $invalidInput[] = $this->post_array[$instanceKey] + array("validationError" => validation_errors());
                reset_validation_errors();
                
            }
        }

        $invalidCount = 0;
        //calculate invalid sets count
        if (sizeof($invalidInput) != 0) {
            $invalidCount = sizeof($invalidInput);
        }
        //encode all the processed inputs in a single array
        $response = [
            "totalInputsCount" => $totalInput,
            "totalValidInputsCount" => $totalInput - $invalidCount,
            "totalInvalidInputsCount" => $invalidCount,
            "validInputs" => $validInput,
            "invalidInputs" => $invalidInput,
        ];
        //set in attribute response
        $this->response = $response;
    }

    /*
     * return processed inputs
     */

    public function processResponse() {
        $this->validateInput();
        return $this->response;
    }

}

?>