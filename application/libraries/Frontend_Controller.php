<?php

Class Frontend_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library("ion_auth");

        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('auth/index', 'refresh');
        }

        $this->data['admin'] = false;

        if ($this->ion_auth->is_admin()) {
            $this->data['admin'] = true;
        }

        $app_name = $this->db->get_where("settings", array("settings_name" => "application_name"))->row();
        $com_name = $this->db->get_where("settings", array("settings_name" => "company_name"))->row();
        $app_logo = $this->db->get_where("settings", array("settings_name" => "application_logo"))->row();

        $this->data['settings']['application_title'] = $app_name->settings_value;
        $this->data['settings']['company_name'] = $com_name->settings_value;
        $this->data['settings']['application_logo'] = $app_logo->settings_value;

        $this->data['totalRoles'] = 566;
        $this->data['totalUsers'] = 566;

        $userDetail = $this->db->get_where("users", array("id" => $this->ion_auth->get_user_id()))->row();
        //Get menus accessible for the user
        $this->data["menus"] = $this->getMenus();
//        $this->data['menus'] = array();
        $this->data["username_header"] = $userDetail->username;
//checking permission for the logged in user
        $this->checkPermission();
    }

    private function getMenus() {
        $menus = array();
        $menuHeads = $this->db->order_by("default_menu_head", "ASC")->get("menu_group_head")->result();
        foreach ($menuHeads as $head) {
            $index = empty($head->menu_head) ? $head->menu_head : $head->default_menu_head;

            //join with permissions to retrieve all the related urls only
            $groupDetail = $this->db->get_where("users_groups", array("user_id" => $this->ion_auth->get_user_id()))->row();

            $menus[$index] = $this->db
                    ->join("permissions p", "al.acl_list_id=p.acl_id")
                    ->get_where("acl_list al", array("al.menu_group_head" => $head->menu_group_head_id, "al.include_in_menu" => "1", "p.group_id" => $groupDetail->group_id))
                    ->result();
        }
        return $menus;
    }

    private function checkPermission() {

        $segment1 = $this->uri->segment(1);
        $segment2 = $this->uri->segment(2);

        $url = $segment1;
        $url .= ($segment2 == "") ? "/index" : "/$segment2";

        if ($this->ion_auth->is_admin()) {
            $aclArr = array("error", "auth", "dashboard", "rbac", "userGroups", '');
            if (!in_array($segment1, $aclArr))
                redirect(base_url("error/accessDenied", "refresh"));
        }
        if (!$this->ion_auth->is_admin()) {
            if (!empty($segment1)) {
                $reject = array("error", "auth", "dashboard", '');

                $checkArray = array("ajaxCalls", "attendance", "departments", 'knowledgeBase', 'leaveApplication', "leaveManagement", "rbac", 'reports', 'salary_heads', "userGroups", "users", "settings");
                if (in_array($segment1, $checkArray)) {
                    $groupDetail = $this->db->get_where("users_groups", array("user_id" => $this->ion_auth->get_user_id()))->row();

                    $acl_detail = $this->db->where("acl_list_url", $url)->get("acl_list")->row("acl_list_id");

                    if (empty($acl_detail)) {
                        redirect(base_url("error/accessDenied"), "refresh");
                    }
                    $permission = $this->db
                            ->get_where("permissions", array("group_id" => $groupDetail->group_id, "acl_id" => $acl_detail))
                            ->row();

                    if (empty($permission)) {
                        redirect(base_url("error/accessDenied"), "refresh");
                    }
                }
            }
        }
    }

    public function deactivateExpiredSubscriptions() {
        $this->db->update("subscriptions", array("subscription_status" => '0'), array("subscription_to <" => date("Y-m-d")));
    }

    public function deactivatePreviousSubscriptions($subscription_id, $client_id) {
        $this->db->update("subscriptions", array("subscription_status" => '0'), array("subscription_to <" => $subscription_id, "client_id" => $client_id));
    }

    public function effectivePayAmount($totalAmount, $vat, $discount) {
//        calculate vat amount
        $vatAmount = ($vat / 100) * $totalAmount;
//        add vat amount to the original amount
        $newAmount = $totalAmount + $vatAmount;
//calculat discount amount from new amount after vat
        $discountAmount = ($discount / 100) * $newAmount;
//       deduct discount amount
        $amount = $newAmount - $discountAmount;

        return floor($amount);
    }

    /*
     * save transaction log 
     * applicable for classified advertisement and subscription payments
     * 
     */

    public function insertTransactionLog($transaction = array(), $vat, $discount, $particular = "") {
        $effective_payable_amount = $this->effectivePayAmount($transaction['payable_amount'], key_exists("vat", $transaction) ? $transaction['vat'] : 0, key_exists("discount", $transaction) ? $transaction['discount'] : 0);
        $transaction['effective_payable_amount'] = $effective_payable_amount;
        //total payments for now
        //later may be changed
        $transaction['remaining_amount'] = 0;
        $transaction["transaction_date"] = date('Y-m-d');

        $this->db->insert("transactions", $transaction);
        $transId = $this->db->insert_id();
        //insert Payment Log
        $invoiceID = $this->insertPaymentLog($transId, $particular);
        return $invoiceID;
    }
    
    public function insertgGENERAL_AD_TransactionLog($transaction, $particular, $effective_payable_amount){
       
        $transaction['effective_payable_amount'] = $effective_payable_amount;
        //total payments for now
        //later may be changed
        $transaction['remaining_amount'] = 0;
        $transaction["transaction_date"] = date('Y-m-d');

        $this->db->insert("transactions", $transaction);
        $transId = $this->db->insert_id();
        //insert Payment Log
        $invoiceID = $this->insertPaymentLog($transId, $particular);
        return $invoiceID;
    }
  
    /*
     * save payment log
     * applicable for all payment process
     */

    public function insertPaymentLog($transactionId, $particular) {

        $transDetail = $this->db->get_where("transactions", array("transaction_id" => $transactionId))->row();

        $paymentDetail = array(
            "transaction_id" => $transactionId,
            "amount" => $transDetail->effective_payable_amount,
            "payment_date" => date("Y-m-d"),
            "received_by" => $this->ion_auth->get_user_id()
        );

        $this->db->insert("payment_logs", $paymentDetail);

        $paymentLogId = $this->db->insert_id();

        $invoiceId = $this->insertInvoiceLog($transDetail, $paymentLogId, $particular);
        return $invoiceId;
    }

    /*
     * save invoice details 
     * applicable for all transactions
     */

    public function insertInvoiceLog($transaction, $paymentLogId, $particular) {
        $clientType = $transaction->reference_type;
        $frequency = 0;
        if ($clientType != 3) {
            $todaysRecords = $this->db->get_where("transactions", array("reference_type" => "$clientType", "client_sponsor_id" => $transaction->client_sponsor_id, "transaction_date" => date('Y-m-d')))->result();
            $frequency = sizeof($todaysRecords);
        }

        $invoiceCode = $this->generateInvoiceNumber($frequency, $this->getCustomerCode($clientType), $transaction->transaction_id);

        $invoiceHeads = $this->insertInvoiceHeads($invoiceCode, $paymentLogId, $transaction->reference_id, $transaction->reference_type, $transaction->client_sponsor_id, $transaction->effective_payable_amount);
        //items to be inserted in invoice details
        $invoiceItems = array(
            array(
                "invoice_id" => $invoiceCode,
                "particular" => $particular,
                "particular_type" => 1,
                "percent" => '',
                "amount" => $transaction->effective_payable_amount
            )
        );

        if ($transaction->vat != 0) {
            array_push($invoiceItems, array(
                "invoice_id" => $invoiceCode,
                "particular" => "VAT",
                "particular_type" => 2,
                "percent" => $transaction->vat,
                "amount" => '',
            ));
        }
        if ($transaction->discount != 0) {
            $invoiceItems[] = array(
                "invoice_id" => $invoiceCode,
                "particular" => "Discount",
                "particular_type" => 2,
                "percent" => $transaction->discount,
                "amount" => '',
            );
        }
        $this->db->insert_batch("invoice_details", $invoiceItems);

        return $invoiceCode;
    }

    public function getCustomerCode($referenceType) {
        if ($referenceType == 1) {
            $code = "SUB";
        } else if ($referenceType == 2) {
            $code = "GAD";
        } else {
            $code = "CAD";
        }
        return $code;
    }

    /*
     * generate invoice number for the created invoice 
     * first chars before T = frequency of transaction the current day
     * T separator
     * 8 chars of date without separator
     * 3 chars for transaction type, SUB for subscription, CAD for classified Advertisement, GAD for general Advertisement
     * remaining all chars for subscriber id or advertisement sponsor or CAD id
     */

    public function generateInvoiceNumber($frequency = 0, $clientCategory, $transactionId) {
        $newDate = date('Ymd');

        //like query to search the record if saved today
        $invoiceCode = $clientCategory . $frequency . "T" . $transactionId . "D" . $newDate;

        return $invoiceCode;
    }

    private function insertInvoiceHeads($invoiceCode, $paymentLogId, $referenceId, $referenceType, $clientSponsorId, $grandTotal) {
        $customer_name = '';
        $VAT = '';
        $customer_Address = '';

        if ($referenceType == 3) {
            $det = $this->db->get_where("classified_advertisements", array("c_advertisement_id" => $referenceId))->row();
            $customer_name = $det->sponsor_name;
            $customer_Address = $det->sponsor_address;
        }

        if ($referenceType == 2) {
            $det = $this->db->get_where("ad_sponsors", array("sponsor_id" => $clientSponsorId))->row();
            $customer_name = $det->sponsor_name;
            $customer_Address = $det->address;
            $VAT = $det->sponsor_vat;
        }
        if ($referenceType == 1) {
            $det = $this->db->get_where("clients", array("client_id" => $clientSponsorId))->row();
            $customer_name = $det->client_first_name . " " . $det->client_last_name;
            $customer_Address = $det->address;
//            $VAT= $det->sponsor_vat;
        }
        $insertArr = array(
            "customer_name" => $customer_name,
            "customer_address" => $customer_Address,
            "vat" => $VAT,
            "grand_total" => $grandTotal,
            "payment_log_id" => $paymentLogId,
            "invoice_id" => $invoiceCode,
            "created_date" => date('Y-m-d'),
            "created_by" => $this->ion_auth->get_user_id(),
        );

        $this->db->insert("invoice", $insertArr);
    }

    /*
     * create a financial account
     * for advertisement and subscription billing and logs
     * not applied this time
     */

    public function createAccount($category = "SUB", $clientId = "0001") {
        $accountId = $this->generateAccountKey($category, $clientId);
        echo $accountId;
        die;

        $this->db->insert("accounts", array("account_id" => $accountId, "client_id" => $clientId, "created_date" => date("Y-m-d")));
        return $accountId;
    }

    public function generateAccountKey($category, $clientId) {
        return $category . "-" . $clientId;
    }

}

?>
