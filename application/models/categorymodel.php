<?php

if (!defined('BASEPATH'))
    die('Not allowed');

class Categorymodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getRootCategories() {

        //$this->db->order_by( 'cat_order asc, pcname asc' ); 
        $this->db->order_by('pcname asc');
        $this->db->where(array('parentid' => '0'));
        $q = $this->db->get('pcategory');
        if ($q->num_rows() > 0)
            return $q->result();
        $q->free_result();
    }

    public function getCategoryByID($id) {
        $this->db->where('pcid', $id);
        $q = $this->db->get('pcategory');

        if ($q->num_rows() > 0)
            return $q->row();

        $q->free_result();
    }

    public function getActiveRootcategory($limit = false) {

        $this->db->order_by( 'pcorder asc' ); 
        $this->db->where(array('parentid' => '0' , 'pcstatus'=>1));
        $q = $this->db->get('pcategory');
        
        if($limit)
            $this->db->limit($limit);
        
        if ($q->num_rows() > 0)
            return $q->result();
        
        $q->free_result();
    }

    ## store product categories

    public function getStoreProductCategories($pcategories) {
        $pcids = unserialize($pcategories);
        //gc_debug($pcids);
        $this->db->where('pcstatus', '1');
        //$this->db->where_in('pcid' , implode(',' , $pcids));
        $this->db->where_in('pcid', $pcids);

        $q = $this->db->get('pcategory');

        if ($q->num_rows() > 0)
            return $q->result();

        $q->free_result();
    }

    ## get store product categores

    public function getAssociatedCategories($businessmodel) {
        //die('sfsfs');
        $merchantdetail = $businessmodel->getMerchantById();
        $categoryids = unserialize($merchantdetail->pcategories);
        //gc_debug($categoryids , 1);
        //if($categoryids)
        //$ids = join("," , $categoryids);
        //echo $ids; 
        //$this->db->order_by( 'cat_order asc, pcname asc' ); 
        $this->db->order_by('pcname asc');
        //$this->db->where_in( 'pcid' , $ids ); // SELECT * FROM pcategory WHERE `pcid` IN ($ids)
        $this->db->where_in('pcid', $categoryids); // SELECT * FROM pcategory WHERE `pcid` IN ($ids)
        $q = $this->db->get('pcategory');
        //echo $q->num_rows(); 
        //echo $this->db->last_query();die();
        if ($q->num_rows() > 0)
            return $q->result();
        $q->free_result();
    }

}

?>