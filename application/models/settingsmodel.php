<?php

if (!defined('BASEPATH')) die('Direct access is Not allowed');

class SettingsModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function getValueByName($setting_name) {
        $this->db->where('settings_name' , $setting_name);
        $q = $this->db->get('settings');
        
        if($q->num_rows() > 0)
            return $q->row()->settings_value;
        else
            return false;
        $q->free_result();
    }
    
}
