<?php

if (!defined('BASEPATH'))
    die('Not allowed');

class Wishlistmodel extends CI_Model {

    public $orderdate;

    public function __construct() {
        parent::__construct();
    }

    public function save_wishlist($memid, $wishlistids) {
        if ($wishlistids) {
            $serialized_ids = serialize($wishlistids);

            $data = array(
                'memid' => $memid,
                'wl_data' => $serialized_ids,
                'wl_cdate' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('wishlist', $data);
            return true;
        }
        else
            return false;
    }

    public function getLastWishList($memid) {
        $this->db->where('memid', $memid);
        $this->db->order_by('wl_cdate', 'desc');
        $this->db->limit(1);
        $q = $this->db->get('wishlist');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else
            return false;

        $q->free_result();
    }

    public function getMemberWishList($memid) {
        $this->db->where('memid', $memid);
        $this->db->order_by('wl_cdate', 'desc');

        $q = $this->db->get('wishlist');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        else
            return false;

        $q->free_result();
    }

    public function getMemberWishListById($wl_id) {
        $this->db->where('wl_id', $wl_id);
        $this->db->limit(1);
        $q = $this->db->get('wishlist');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        else
            return false;

        $q->free_result();
    }

    public function delete_wishlist($memid, $wl_id) {
        $this->db->where(array(
            'memid' => $memid,
            'wl_id' => $wl_id,
        ));

        if ($this->db->delete('wishlist'))
            return true;
        else
            return false;
    }

}