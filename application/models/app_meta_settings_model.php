<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class App_Meta_Settings_Model extends CI_Model {

    public function __construct() {
        parent::__construct();

       
    }

    public function get_meta_settings($param) {
        $this->db->select('meta_value');
        $this->db->where(
                array(
                    'meta_app' => $param['meta_app'],
                    'meta_key' => $param['meta_key']
                )
        );
        $q = $this->db->get('app_meta_settings');

        if ($q->num_rows() > 0)
            return $q->row();

        $q->free_result();
    }
    
    
}