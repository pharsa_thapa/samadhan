<?php

if (!defined('BASEPATH'))
    die('Not allowed');

class Productmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function add() {
        $merchantuserdata = $this->session->userdata(APP_PFIX . 'business');
        $data = array(
            'pcid' => $this->input->post('pcid', TRUE),
            'mid' => $merchantuserdata['businessid'],
            'pname' => $this->input->post('pname', TRUE),
            'pprice' => $this->input->post('pprice', TRUE),
            'pdesc' => $this->input->post('pdesc', TRUE),
            'pdescshort' => $this->input->post('pdescshort', TRUE),
            'pstatus' => $this->input->post('pstatus', TRUE),
            'cdate' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('product', $data);

        return $this->db->insert_id();
    }

    public function edit($pid) {
        $merchantuserdata = $this->session->userdata(APP_PFIX . 'business');
        $data = array(
            'pcid' => $this->input->post('pcid', TRUE),
            'mid' => $merchantuserdata['businessid'],
            'pname' => $this->input->post('pname', TRUE),
            'pprice' => $this->input->post('pprice', TRUE),
            'pdesc' => $this->input->post('pdesc', TRUE),
            'pdescshort' => $this->input->post('pdescshort', TRUE),
            'pstatus' => $this->input->post('pstatus', TRUE),
            'mdate' => date('Y-m-d H:i:s'),
        );

        $this->db->where('pid', $pid);
        $this->db->update('product', $data);

        //return $this->db->insert_id();
    }

    ############## image operations

    public function add_image($id, $img_name) {
        $pistatus = $this->input->post('pistatus', TRUE);

        ## if pidefault is selected as active reset all available old images as non default image
        if ($this->input->post('pidefault')) {
            $data = array('pidefault' => '0');
            $this->db->where('pid', $id);
            $this->db->update('pimage', $data);

            $pistatus = 1;
        }

        ## insert image record
        $data = array(
            'pid' => $this->input->post('pid', TRUE),
            'pititle' => $this->input->post('pititle', TRUE),
            'pialt' => $this->input->post('pialt', TRUE),
            'piname' => $img_name,
            'pidefault' => $this->input->post('pidefault', TRUE),
            'cdate' => date('Y-m-d H:i:s'),
            'pistatus' => $pistatus,
        );

        $status = $this->db->insert('pimage', $data);

        return $status;
    }

    public function changeImageStatus($pid, $piid) {
        if ($pid && $piid) {
            $this->db->where('pid', $pid);
            $this->db->from('pimage');
            $noofrecord = $this->db->count_all_results();

            # update only if there are more than 1 record
            if ($noofrecord > 1) {
                // find that status of current image
                // if it is active, change to inactive else vice versa
                $this->db->select('pistatus,pidefault');
                $this->db->where('piid', $piid);
                $q = $this->db->get('pimage');
                $rec = $q->row();

                if ($rec->pistatus) { // deactivate 
                    // deactivate only if atleast one of other images are active
                    // check for other active images if exists
                    $this->db->where(array('pid' => $pid, 'pistatus' => 1, 'piid !=' => $piid));
                    //$this->db->where_not('piid !=' , $piid); // count all except current image
                    $this->db->from('pimage');
                    $active_noofrecord = $this->db->count_all_results();

                    // deactivate the current image if there is atleast one another image that is active
                    if ($active_noofrecord > 0) {
                        $pistatus = 0;

                        // check if this image is default display image
                        // if yes, change default image before deactivating this image
                        // select the random image that is to be made default 
                        // from among the active images except the current one
                        if ($rec->pidefault) { // if default
                            $this->db->select('piid');
                            $this->db->where(array('pid' => $pid, 'pistatus' => 1, 'piid !=' => $piid));
                            $this->db->order_by('piid', 'random');
                            $q_default = $this->db->get('pimage');
                            $rec_default = $q_default->row();

                            // make default
                            $this->changeDefaultImage($pid, $rec_default->piid);
                        }
                    }
                    else
                        $pistatus = 1; // else leave the image as it is ie active
                }
                else // activate
                    $pistatus = 1;

                // update status
                $data = array(
                    'pistatus' => $pistatus
                );
                $this->db->where(array('pid' => $pid, 'piid' => $piid));
                $stat = $this->db->update('pimage', $data);
                //echo $this->db->last_query();

                if ($this->db->affected_rows() == 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }
        else {
            return false;
        }
    }

    public function changeDefaultImage($pid, $piid) {
        # check if the current image to be made default is active or not
        # change to default only if the image is active
        $this->db->select('pistatus');
        $this->db->where('piid', $piid);
        $q = $this->db->get('pimage');
        if ($q->num_rows > 0) {
            if ($q->row()->pistatus == 1) { // if the image is active
                # first change all images to non default
                $data = array(
                    'pidefault' => 0
                );
                $this->db->where(array('pid' => $pid));
                $this->db->update('pimage', $data);

                # now change current image to default
                $data = array(
                    'pidefault' => 1
                );
                $this->db->where(array('pid' => $pid, 'piid' => $piid));
                $this->db->update('pimage', $data);
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }

    public function delete_image($piid) {
        if ($piid) {
            # find the product id of this image
            $this->db->select('pid,pidefault,piname');
            $this->db->where('piid', $piid);
            $q_p = $this->db->get('pimage');
            $pid = $q_p->row()->pid;
            $pidefault = $q_p->row()->pidefault;
            $piname = $q_p->row()->piname;

            if ($pid) {
                // if this is not default image delete straight away.
                if ($pidefault == 0) {
                    $this->db->where('piid', $piid);
                    $this->db->delete('pimage');
                    return true;
                } else {
                    # check if there are more than 1 active images related to this product apart from this current image that is to be deleted
                    $this->db->where('pid', $pid);
                    $this->db->where('pistatus', 1);
                    $this->db->where('piid !=', $piid);
                    $this->db->from('pimage');
                    $noofrecord = $this->db->count_all_results();

                    # if yes, change default image to another active image
                    if ($noofrecord > 0) {
                        # select the random image that is to be handed the new default role
                        $this->db->select('piid');
                        $this->db->where(array('pid' => $pid, 'pistatus' => 1, 'piid !=' => $piid));
                        $this->db->order_by('piid', 'random');
                        $q_tobedefault = $this->db->get('pimage');
                        $rec_tobedefault = $q_tobedefault->row();

                        // make default
                        $this->changeDefaultImage($pid, $rec_tobedefault->piid);
                        
                        # delete the image record from image table
                        $this->db->where('piid', $piid);
                        $this->db->delete('pimage');
                        
                        # delete image file
                        unlink('./assets/uploads/product/' . @$piname); // big image
                        //unlink('./assets/uploads/product/' . @$piname); // thumb image (function to delete thumbnail image)
                        return true;
                    }
                    else
                        return false;
                }
            }
            else
                return false;
        } else {
            return false;
        }
    }

    public function get_image_list($id, $status = '', $order_by = '') {
        $this->db->where('pid', $id);

        if ($status)
            $this->db->where('pistatus', $status);

        if ($order_by)
            $this->db->order_by($order_by);

        $q = $this->db->get('pimage');

        if ($q->num_rows() > 0)
            return $q->result();

        $q->free_result();
    }

    public function getProduct() {

        //$this->db->order_by( 'cat_order asc, pcname asc' ); 
        $this->db->order_by('pcname asc');
        $this->db->where(array('parentid' => '0'));
        $q = $this->db->get('pcategory');
        if ($q->num_rows() > 0)
            return $q->result();
        $q->free_result();
    }

    public function getProductById($pid) {
        $this->db->where('pid', $pid);

        $q = $this->db->get('product');

        if ($q->num_rows() > 0)
            return $q->row();

        $q->free_result();
    }
    
    public function getProductByImageId($piid) {
        $this->db->where('piid', $piid);

        $q = $this->db->get('pimage');

        if ($q->num_rows() > 0)
            return $q->row();

        $q->free_result();
    }

    public function getStoreProduct($mid, $pcid) {
        $this->db->where('product.mid', $mid);
        $this->db->where('product.pcid', $pcid);

        $this->db->from('product');
        $this->db->join('pimage', 'product.pid = pimage.pid', 'left');

        $this->db->where('pimage.pidefault', 1);

        //$q = $this->db->get('product');
        $q = $this->db->get();

        if ($q->num_rows() > 0)
            return $q->result();

        $q->free_result();
    }

    public function getMyStoreProduct($mid) {

        $this->db->where('product.mid', $mid);
        //$this->db->where('product.pcid' , $pcid);

        $this->db->from('product');
        $this->db->join('pimage', 'product.pid = pimage.pid', 'left');

        $this->db->where('pimage.pidefault', 1);

        //$q = $this->db->get('product');
        $q = $this->db->get();
        //echo $q->num_rows();
        if ($q->num_rows() > 0)
            return $q->result();

        $q->free_result();
    }

    /**
     * for emall
     * retrieve product list
     */
    public function getProductList($pcid = '', $mid = '', $order_by = '') {
        if ($mid)
            $this->db->where('product.mid', $mid);
        if ($pcid)
            $this->db->where('product.pcid', $pcid);

        $this->db->from('product');
        $this->db->join('pimage', 'product.pid = pimage.pid', 'left');

        $this->db->where('product.pstatus', 1);
        $this->db->where('pimage.pidefault', 1);

        if ($order_by) {
            $this->db->order_by($order_by);
        }

        //$q = $this->db->get('product');
        $q = $this->db->get();

        if ($q->num_rows() > 0)
            return $q->result();

        $q->free_result();
    }

    public function getProductBySearch($pcid = '', $searchkey = '', $order_by = '', $format = 'object') {
        if (is_numeric($pcid))
            $this->db->where('product.pcid', $pcid);
        if ($searchkey)
            $this->db->like('product.pname', $searchkey);

        $this->db->from('product');
        $this->db->join('pimage', 'product.pid = pimage.pid', 'left');

        $this->db->where('product.pstatus', 1);
        $this->db->where('pimage.pidefault', 1);

        if ($order_by) {
            $this->db->order_by($order_by);
        }

        //$q = $this->db->get('product');
        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            if ($format == 'object')
                return $q->result();
            else
                return $q->result_array();
        }

        $q->free_result();
    }
    
    public function getProductByAdvancedSearch($pcid, $searchkey, $pricemin, $pricemax, $order_by = '', $format = 'object') {
        if (is_numeric($pcid))
            $this->db->where('product.pcid', $pcid);
        if (@$searchkey)
            $this->db->like('product.pname', $searchkey);

        $this->db->from('product');
        $this->db->join('pimage', 'product.pid = pimage.pid', 'left');

        $this->db->where('product.pstatus', 1);
        $this->db->where('pimage.pidefault', 1);

        if ($order_by) {
            $this->db->order_by($order_by);
        }

        //$q = $this->db->get('product');
        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            if ($format == 'object')
                return $q->result();
            else
                return $q->result_array();
        }

        $q->free_result();
    }

    public function getStoreNewArrivalProduct($mid, $format = 'object') {
        $this->db->where('product.mid', $mid);

        $this->db->from('product');
        $this->db->join('pimage', 'product.pid = pimage.pid', 'left');

        $this->db->where('pimage.pidefault', 1);
        $this->db->where('product.pstatus', 1);
        $this->db->order_by('product.pid DESC');
        $this->db->limit(12);

        //$q = $this->db->get('product');
        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            if ($format == 'object')
                return $q->result();
            else
                return $q->result_array();
        }

        $q->free_result();
    }

    public function getStoreFeaturedProduct($mid, $format = 'object') {
        $this->db->where('product.mid', $mid);

        $this->db->from('product');
        $this->db->join('pimage', 'product.pid = pimage.pid', 'left');

        $this->db->where('pimage.pidefault', 1);
        $this->db->where('product.pstatus', 1);
        $this->db->where('product.pfeatured', 1);
        $this->db->order_by('product.pid DESC');
        $this->db->limit(12);

        //$q = $this->db->get('product');
        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            if ($format == 'object')
                return $q->result();
            else
                return $q->result_array();
        }

        $q->free_result();
    }

    public function getStoreDealProduct($mid, $format = 'object') {
        $this->db->where('product.mid', $mid);

        $this->db->from('product');
        $this->db->join('pimage', 'product.pid = pimage.pid', 'left');

        $this->db->where('pimage.pidefault', 1);
        $this->db->where('product.pstatus', 1);
        $this->db->where('product.pdiscount >', 0);
        $this->db->order_by('product.pid DESC');
        $this->db->limit(12);

        //$q = $this->db->get('product');
        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            if ($format == 'object')
                return $q->result();
            else
                return $q->result_array();
        }

        $q->free_result();
    }

    public function getTemporaryWishlist($ids, $format = 'object') {
        //echo $pids_comma_separated = join(',' , $ids);
        //gc_debug($ids);
        //gc_debug(array(1,3));
        $this->db->where('product.pstatus', 1);
        $this->db->where('pimage.pidefault', 1);

        $this->db->where_in('product.pid', $ids);

        $this->db->from('product');
        $this->db->join('pimage', 'product.pid = pimage.pid', 'left');

        $q = $this->db->get();

        if ($q->num_rows() > 0)
            if ($format == 'object')
                return $q->result();
            else
                return $q->result_array();

        $q->free_result();
    }

    /**
     *
     * @param type $pid
     * @return int | boolean 
     */
    public function getMerchantIdByProductId($pid) {
        $this->db->where('pid', $pid);
        $q = $this->db->get('product');

        if ($q->num_rows() > 0) {
            return $q->row()->mid;
        }
        else
            return false;

        $q->free_result();
    }

}

?>