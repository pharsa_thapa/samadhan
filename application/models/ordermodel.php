<?php

if (!defined('BASEPATH'))
    die('Not allowed');

class Ordermodel extends MY_Model {

    public $orderdate;

    public function __construct() {
        parent::__construct();
    }

    public function makeOrder($membermodel, $credit , $arr_param) {
        $this->orderdate = date('Y-m-d H:i:s');

        if ($this->session->userdata(APP_PFIX . 'guest')) {
            ## insert guest info into member table (if guest)
            $memid = $membermodel->addGuest();
        }
        if ($this->session->userdata(APP_PFIX . 'member')) {
            $memberdata = $this->session->userdata(APP_PFIX . 'member'); // if exists
            # assign session member id (if member)
            $memid = $memberdata['memberid'];
        }

        ## insert into order table
        list($oid, $invoice, $credit) = $this->insertOrder($memid, $credit, $arr_param);

        ## insert into order detail table
        $this->insertOrderDetail($oid);

        ## send notification
        $this->notification($memid, $oid, $invoice, $credit);
    }

    ## insert into order table

    public function insertOrder($memid, $credit, $arr_param) {
        // retrieve last order id
        /*
          $this->db->order_by('oid' , 'DESC');
          $q = $this->db->get('order');

          if($q->num_rows() > 0)
          {
          $oid = $q->row()->oid;
          }
          $input = $oid + 1;//24;
          $oinvoice = str_pad($input, 12, "0", STR_PAD_LEFT); // 000000000024
         */

        //$oinvoice = rand(111111111, 999999999); // generate unique invoice
        // insert into invoice table to generate latest invoice id
        $data = array('id' => '');
        $this->db->insert('invoice', $data);
        $lastid = mysql_insert_id();
        $oinvoice = str_pad($lastid, 12, "0", STR_PAD_LEFT);


        // commision should be calculated from ordertotal
        // that should not include shipping charge
        // if credit is used, then total should be the amount subtracted with credit used
        $ototal_forcommision_calculation = $this->cart->total() - $credit;

        $oshippingcharge = 0;
        //$ototal = $this->cart->total() + 44; // 44 = shipping cost (dummy number), later on should be replaced by real shipping cost

        $ototal = $this->cart->total() + $oshippingcharge; // 44 = shipping cost (dummy number), later on should be replaced by real shipping cost


        if (($excess_credit = $credit - $ototal) > 0) {
            $credit = $excess_credit;
        }

        // insert into order table
        $data = array(
            'memid' => $memid,
            'shipping_address_id'=>$arr_param['shipping_address_id'],
            'oinvoice' => $oinvoice,
            'osubtotal' => $this->cart->total(),
            'oshippingcharge' => $oshippingcharge,
            'cpn_discount' => $this->session->userdata(APP_PFIX . 'cpn_discount'),
            'ototal' => $ototal,
            'credit' => $credit,
            'odate' => $this->orderdate,
            'ostatus' => 'pending',
            'cpn_code' => $this->session->userdata(APP_PFIX . 'cpn_code'),
        );

        $this->db->insert('order', $data);

        $oid = mysql_insert_id();

//        $this->subtractCommission($memid, $credit);
//
//        $referrer = $this->mylib->get_field(
//                'referrer_id', array('memid' => $memid), 'member'
//        );
//
//        if ($referrer) {
//            if ($ototal_forcommision_calculation > 1) {
//                $this->saveCommission($referrer, $memid, $oid, $ototal_forcommision_calculation);
//            }
//        }

        return array($oid, $oinvoice, $credit);
    }

    ## insert into order table

    public function insertOrderDetail($oid) {


        // loop data to insert individual products from cart into order detail table
        foreach ($this->cart->contents() as $items) {
            $data = array(
                'oid' => $oid,
                'pid' => $items['id'],
                'pname' => $items['name'],
                'pprice' => $items['price'],
                'pqty' => $items['qty'],
                'ptotal' => $items['subtotal'],
                'odstatus' => '0',
            );
            $this->db->insert('orderdetail', $data);
        }

        //return $this->db->insert('orderdetail', $data);
    }

    /**
     * send notification to merchants
     */
    public function notification($memid, $oid, $invoice, $credit) {
        $CI = & get_instance();
        $CI->load->model('product/productmodel');
        $CI->load->model('settingsmodel');
        $CI->load->model('business/businessmodel');
        $CI->load->model('member/membermodel');

        # email library
        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $this->email->set_mailtype("html");

        # member/guest detail
        $memberdetail = $this->membermodel->getMemberById($memid);

        ## sort merchant and their corresponding products
        //$merchantproduct[] = array();
        $merchantproduct = array();
        $merchantproductdetail = array();
        foreach ($this->cart->contents() as $items) {
            # retrieve merchant id
            // get merchant id with reference to product id from productmodel
            // inside common porductmodel class in main model folder
            $merid = $this->productmodel->getMerchantIdByProductId($items['id']);

            if (array_key_exists($merid, $merchantproduct)) {
                array_push($merchantproduct[$merid], $items['id']);
            }
            else
                $merchantproduct[$merid] = array($items['id']);

            if (array_key_exists($merid, $merchantproductdetail)) {
                array_push($merchantproductdetail[$merid], $items);
            } else {
                $merchantproductdetail[$merid] = array($items);
            }
        }

        # notification and email
        $site_email_info = $this->settingsmodel->getValueByName('site_email_info');
        $site_mall_name = $this->settingsmodel->getValueByName('site_mall_name');

        foreach ($merchantproduct as $merchantid => $pids) {
            # retrieve merchant detail
            $merchantdetail = $this->businessmodel->getMerchantById($merchantid);

            ## insert into notification table
            // notification format:
            //        $array = array(
            //            'from' => 'member',
            //            'fromid' => '1',
            //            'subject' => 'You have a new product order',
            //            'message' => 'Sagar has ordered your product. Proper formating of order and product',
            //            'order' => array('2342342342' => array(1, 2)),
            //        );
            //        echo serialize($array);
            $notiarray = array(
                'from' => 'member',
                'fromid' => $memid,
                'subject' => 'You have a new product order',
                'message' => $memberdetail->fname . ' has ordered your product.',
                'order' => array($invoice => $pids),
            );
            $ser_notification = serialize($notiarray);

            $data = array(
                'nottype' => 'order',
                'to' => $merchantid,
                'content' => $ser_notification,
                'status' => 0,
                'cdate' => $this->orderdate,
            );
            $this->db->insert('notification', $data);

            ## email notificaton to merchants
            ob_start();
            include_once (ROOT_PATH . 'assets/email_template/email_template_order_details_to_merchant.php');
            $merchantmessage = ob_get_contents();
            $this->email->from($site_email_info, $site_mall_name);
            $this->email->to($merchantdetail->memail);
            $this->email->subject('New Order! You have an order.');
            $this->email->message($merchantmessage);
            $this->email->send();
            ob_end_clean();
            //echo $this->email->print_debugger();
        }

        # send email to administrator
        ob_start();
        include_once (ROOT_PATH . 'assets/email_template/email_template_order_details_to_admin.php');
        $adminmessage = ob_get_contents();
        $this->email->from($site_email_info, $site_mall_name);
        $this->email->to($site_email_info);
        $this->email->subject('New Sale! An order has been made.');
        $this->email->message($adminmessage);
        $this->email->send();
        ob_end_clean();
        //echo $this->email->print_debugger();
        # send email to buyer
        ob_start();
        include_once (ROOT_PATH . 'assets/email_template/email_template_order_details_to_member.php');
        $adminmessage = ob_get_contents();
        $this->email->from($site_email_info, $site_mall_name);
        $this->email->to($memberdetail->email);
        $this->email->subject('Thank you for your order.');
        $this->email->message($adminmessage);
        $this->email->send();
        ob_end_clean();
        //echo $this->email->print_debugger();
    }

    // get merchant id with reference to product id from productmodel inside
    // common porductmodel class in main model folder
//    public function merchant_id($pid) {
//        $CI = & get_instance();
//        $CI->load->model('productmodel');
//
//        return $this->productmodel->getMerchantIdByProductId($pid);
//    }

    /**
     * send invoice detail to ordering member as an email
     */
    public function invoiceDetail() {

    }

    public function emailme() {
        $this->load->library('email');
        ob_start();
        include_once (ROOT_PATH . 'assets/email_template/email_template_order_details_to_admin.php');
        $adminmessage = ob_get_contents();



        $config['protocol'] = 'smtp';
        //$config['smtp_host'] = 'ssl://smtp.googlemail.com';
        //$config['smtp_port'] = '465';
        //$config['smtp_timeout'] = '30';
        //$config['smtp_user'] = 'your gmail email';
        //$config['smtp_pass'] = 'your gmail password';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        //$config['mailtype']  = 'html';
        $this->email->set_mailtype("html");

        $this->email->from('info@mall.com', 'estore mall shopping');
        $this->email->to('bluebrain11@gmail.com');

        $this->email->subject('An order has been made.');
        $this->email->message($adminmessage);
        $this->email->send();
        echo $this->email->print_debugger();
        ob_end_clean();
        echo $this->email->print_debugger();
    }

    // retrieve member last order
    public function member_last_order($mid) {
        $this->db->select('*');
        $this->db->where('memid', $mid);
        $this->db->order_by('oid' , 'DESC');
        $this->db->limit(1);
        $q = $this->db->get('order');

        if ($q->num_rows > 0) {
            return $q->row();
        }
        else
            return false;

        $q->free_result();
    }

    // retrieve member last order
    public function member_order_history($mid) {
        $this->db->select('*');
        $this->db->where('memid', $mid);
        $this->db->order_by('oid', 'desc');
        $q = $this->db->get('order');

        if ($q->num_rows > 0) {
            return $q->result();
        }
        else
            return false;

        $q->free_result();
    }

    // order detail
    public function member_order_by_id($oid) {
        $this->db->select('*');
        $this->db->where('oid', $oid);
        $this->db->limit(1);
        $q = $this->db->get('order');

        if ($q->num_rows > 0) {
            return $q->row();
        }
        else
            return false;

        $q->free_result();
    }

    // retrieve member order product detail
    public function member_order_detail($oid) {
        $this->db->select('*');
        $this->db->where('oid', $oid);
        $q = $this->db->get('orderdetail');

        if ($q->num_rows > 0) {
            return $q->result();
        }
        else
            return false;

        $q->free_result();
    }

    private function saveCommission($memid, $referral, $oid, $total_order) {
        $commission = $this->calcCommission($total_order);

        if ($commission) {
            $this->db->insert(
                    'commission_history', array(
                'memid' => $memid,
                'referral_id' => $referral,
                'oid' => $oid,
                'commission' => $commission,
                'datetime' => $this->orderdate
                    )
            );

            $this->saveTotalCredit($memid, $commission);
        }
    }

    private function calcCommission($total_order = NULL) {
        $rate = $this->mylib->get_field(
                'settings_value', array('settings_name' => 'referral_commission'), 'settings'
        );

        if ($total_order) {
            return (($total_order) * ($rate / 100));
        } else {
            return false;
        }
    }

    private function saveTotalCredit($memid, $commission) {
        if ($old_commission = $this->existingCommission($memid)) {
            $new_commission = ($old_commission + $commission);
            $this->db
                    ->where(array('memid' => $memid))
                    ->update('commission', array('commission' => $new_commission));
        } else {
            $this->db->insert(
                    'commission', array(
                'memid' => $memid,
                'commission' => $commission
                    )
            );
        }
    }

    private function existingCommission($memid) {
        $this->db
                ->select('commission')
                ->where(array('memid' => $memid));
        $query = $this->db->get('commission');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->commission;
        }
        $query->free_result();
    }

    private function subtractCommission($memid, $credit) {
        $commission = $this->existingCommission($memid);
        if ($commission) {
            $commission = ($commission - $credit);
            $this->db
                    ->where(array('memid' => $memid))
                    ->update('commission', array('commission' => $commission));
        }
    }

    // retrieve tracking info
    public function tracking_info($memid , $tracking_code) {
        $this->db->select('comment');
        $this->db->where('memid', $memid);
        $this->db->where('tracking_code', $tracking_code);
        $q = $this->db->get('order');
//echo $this->db->last_query();
        if ($q->num_rows > 0) {
            return $q->row();
        }
        else
            return false;

        $q->free_result();
    }
}

?>