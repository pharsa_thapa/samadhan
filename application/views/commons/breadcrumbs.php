<div class="page-title">
    <h3><?php echo @$page_head; ?></h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">

            <?php foreach ($template['breadcrumbs'] as $crumb) : ?>
                <li><a href="<?php echo $crumb['uri'] ?>"><?php echo $crumb['name']; ?></a></li>
            <?php endforeach; ?>

        </ol>
    </div>
</div>