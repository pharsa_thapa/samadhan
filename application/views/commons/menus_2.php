<ul class="menu accordion-menu">

    <li><a href="<?php echo base_url(); ?>"><span class="menu-icon glyphicon glyphicon-home"></span>
            <p>Dashboard</p>
        </a>
    </li>
	<li class="droplink">
        <a href="#">
            <span class="menu-icon glyphicon glyphicon-list"></span>
            <p>Subscribers</p>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li><a href="<?php echo base_url("clients/add"); ?>">New Subscriber</a></li>
            <li><a href="<?php echo base_url("clients"); ?>">All Subscribers</a></li>

        </ul>
    </li>


    <li class="droplink">
        <a href="#">
            <span class="menu-icon glyphicon glyphicon-list"></span>
            <p>Distributors</p>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li><a href="<?php echo base_url("distributors/add"); ?>">Add Distributor</a></li>
            <li><a href="<?php echo base_url("distributors"); ?>">All Distributors</a></li>
        </ul>
    </li>

    
    <li class="droplink">
        <a href="#">
            <span class="menu-icon glyphicon glyphicon-list"></span>
            <p>Advertisements</p>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li><a href="<?php echo base_url("ad_sponsors/add"); ?>">Add Sponsor</a></li>
            <li><a href="<?php echo base_url("ad_sponsors"); ?>">All Sponsors</a></li>
            <li><a href="<?php echo base_url("ad_pages_cost/add"); ?>">Add Advertisement</a></li>
            <li><a href="<?php echo base_url("ad_pages_cost"); ?>">All Advertisements</a></li>
        </ul>
    </li>

    <li class="droplink">
        <a href="#">
            <span class="menu-icon glyphicon glyphicon-list"></span>
            <p>
                Schemes
            </p>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li><a href="<?php echo base_url("schemes/add"); ?>">Add Scheme</a></li>
            <li><a href="<?php echo base_url("schemes"); ?>">All Schemes</a></li>
        </ul>
    </li>
    <li class="droplink">
        <a href="#">
            <span class="menu-icon glyphicon glyphicon-list"></span>
            <p>
                Reports
            </p>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li><a href="<?php echo base_url("reports"); ?>">All Subscriber's Report</a></li>
            <li><a href="<?php echo base_url("reports/renewableSubscribers"); ?>">All Renewable Subscriber's Report</a></li>
            <li><a href="<?php echo base_url("reports/subscribersByLocation"); ?>">Location Wise Subscriber's Report</a></li>
            <li><a href="<?php echo base_url("reports/subscribersByDistributor"); ?>">Distributor Wise Subscriber's Report</a></li>
        </ul>
    </li>

</ul>
