<ul class="menu accordion-menu">

    <li><a href="<?php echo base_url(); ?>"><span class="menu-icon glyphicon glyphicon-home"></span>
            <p>Dashboard</p>
        </a>
    </li>

    <?php if (@$admin) { ?>

        <li class="droplink">
            <a href="#">
                <span class="menu-icon glyphicon glyphicon-list"></span>
                <p>ACL Management</p>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li><a href="<?php echo base_url('rbac/setMenuGroupHead'); ?>">Add Menu</a></li>
                <li><a href="<?php echo base_url('rbac/listMenuHeadList'); ?>">View Menu List</a></li>
                <li><a href="<?php echo base_url('rbac/listAllACLItems'); ?>">All ACL Items</a></li>
            </ul>
            
            
        </li>
        
       <li class="droplink">
            <a href="#">
                <span class="menu-icon glyphicon glyphicon-list"></span>
                <p>User Management</p>
                <span class="arrow"></span>
            </a>
            
            <ul class="sub-menu">
                <li><a href="<?php echo base_url('userGroups/createGroup'); ?>">Add User Group</a></li>
                <li><a href="<?php echo base_url('userGroups/index'); ?>">All User Groups</a></li>
            </ul>
        </li>
        
        
    <?php } else { ?>
        <?php
        foreach ($menus as $key => $value) :
            if (!empty($value)) {
                ?>
                <li class="droplink">
                    <a href="#">
                        <span class="menu-icon glyphicon glyphicon-list"></span>
                        <p><?php echo $key; ?></p>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <?php foreach ($value as $item) {
                            ?>

                            <li><a href="<?php echo base_url($item->acl_list_url); ?>"><?php echo $item->acl_list_name; ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <?php
            }
        endforeach;
    }
    ?>



</ul>