<!DOCTYPE html>
<html>
    <head>

        <!-- Title -->
        <title><?php echo $settings['application_title']; ?></title>

        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="<?php echo $settings['application_title']; ?>" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Pharsa Bahadur Thapa" />
        <meta name="author" content="suresh.thapa.mgr.45@gmail.com" />

        <!-- Styles -->
        <link href='<?php echo base_url(); ?>/fonts.googleapis.com/csse092.css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url() ?>assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/plugins/datatables/css/jquery.datatables.min.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/plugins/datatables/css/jquery.datatables_themeroller.css" rel="stylesheet" type="text/css"/>	

        <!-- Theme Styles -->
        <link href="<?php echo base_url() ?>assets/css/modern.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/css/themes/white.css" class="theme-color" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" type="text/css"/>

        <script src="<?php echo base_url("assets"); ?>/plugins/jquery/jquery-2.1.3.min.js"></script>
        <script src="<?php echo base_url("assets/plugins/print/printThis.js") ?>"></script>
        <script src="<?php echo base_url() ?>assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/offcanvasmenueffects/js/snap.svg-min.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <?php $this->load->view("commons/head") ?>
    <?php $this->load->view("commons/menus") ?>




</div><!-- Page Sidebar Inner -->
</div><!-- Page Sidebar -->



<div class="page-inner">





    <?php $this->load->view("commons/breadcrumbs"); ?>

    <div id="main-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">

                    <div class="panel-body">


                        <?php
                        if ($this->session->flashdata('success'))
                            echo "<div class='alert alert-success alert-dismissible'>" . $this->session->flashdata('success') . "</div>";

                        if ($this->session->flashdata('error'))
                            echo "<div class='alert alert-danger alert-dismissible'><strong>Aw Snap! Something went wrong!</strong><br>" . $this->session->flashdata('error') . "</div>";
                        ?>

                        <?php echo $template['body']; ?>

                    </div>
                </div>
            </div>
        </div><!-- Row -->
    </div><!-- Main Wrapper -->


    <div id="main-wrapper">
        <div class="row">
        </div><!-- Row -->
    </div><!-- Main Wrapper -->
    <div class="page-footer">
        <p class="no-s">2015 &copy; <?php echo $settings['company_name']; ?>.</p>
    </div>
</div><!-- Page Inner -->





</main><!-- Page Content -->
<nav class="cd-nav-container" id="cd-nav">
    <header>
        <h3>Navigation</h3>
        <a href="#0" class="cd-close-nav">Close</a>
    </header>
    <ul class="cd-nav list-unstyled">
        <li class="cd-selected" data-menu="index">
            <a href="javsacript:void(0);">
                <span>
                    <i class="glyphicon glyphicon-home"></i>
                </span>
                <p>Dashboard</p>
            </a>
        </li>
        <li data-menu="profile">
            <a href="javsacript:void(0);">
                <span>
                    <i class="glyphicon glyphicon-user"></i>
                </span>
                <p>Profile</p>
            </a>
        </li>
        <li data-menu="inbox">
            <a href="javsacript:void(0);">
                <span>
                    <i class="glyphicon glyphicon-envelope"></i>
                </span>
                <p>Mailbox</p>
            </a>
        </li>
        <li data-menu="#">
            <a href="javsacript:void(0);">
                <span>
                    <i class="glyphicon glyphicon-tasks"></i>
                </span>
                <p>Tasks</p>
            </a>
        </li>
        <li data-menu="#">
            <a href="javsacript:void(0);">
                <span>
                    <i class="glyphicon glyphicon-cog"></i>
                </span>
                <p>Settings</p>
            </a>
        </li>
        <li data-menu="calendar">
            <a href="javsacript:void(0);">
                <span>
                    <i class="glyphicon glyphicon-calendar"></i>
                </span>
                <p>Calendar</p>
            </a>
        </li>
    </ul>
</nav>
<div class="cd-overlay"></div>


<!-- Javascripts -->
<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url("assets/plugins/print/printThis.js") ?>"></script>
<script src="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/pace-master/pace.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/switchery/switchery.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/offcanvasmenueffects/js/classie.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/offcanvasmenueffects/js/main.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/waves/waves.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/3d-bold-navigation/js/main.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/modern.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/pages/table-data.js"></script>

</body>
</html>