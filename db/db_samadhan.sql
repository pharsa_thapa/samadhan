-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 20, 2016 at 08:31 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_samadhan`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_list`
--

CREATE TABLE IF NOT EXISTS `acl_list` (
  `acl_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `acl_list_name` varchar(100) NOT NULL,
  `acl_list_url` varchar(100) NOT NULL,
  `menu_group_head` int(11) NOT NULL,
  `include_in_menu` enum('0','1') NOT NULL,
  PRIMARY KEY (`acl_list_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=130 ;

--
-- Dumping data for table `acl_list`
--

INSERT INTO `acl_list` (`acl_list_id`, `acl_list_name`, `acl_list_url`, `menu_group_head`, `include_in_menu`) VALUES
(6, 'List Departments', 'departments/index', 4, '1'),
(7, 'Add Department', 'departments/addDepartment', 4, '1'),
(8, 'Edit Department', 'departments/editDepartment', 4, '0'),
(9, 'Delete Department', 'departments/deleteDepartment', 4, '0'),
(13, 'Add Menu Head', 'rbac/setMenuGroupHead', 6, '1'),
(14, 'View List Of Menus', 'rbac/listMenuHeadList', 6, '1'),
(15, 'Process Menu Head Details', 'rbac/addMenuGroupHead', 6, '0'),
(16, 'Edit Menu Detail', 'rbac/editMenuHead', 6, '0'),
(17, 'View Menu Detail', 'rbac/detailMenuHead', 6, '0'),
(18, 'Delete Menu Head', 'rbac/deleteMenuHead', 6, '0'),
(19, 'Add ACL Item Inside Menu Head', 'rbac/addACLItem', 6, '0'),
(20, 'View All Menu Related ACL Items', 'rbac/viewACLItems', 6, '0'),
(21, 'Manage Permission For User Group', 'rbac/managePermission', 6, '0'),
(22, 'View All ACL Items', 'rbac/listAllACLItems', 6, '1'),
(23, 'Delete ACL Item', 'rbac/deleteACLItem', 6, '0'),
(24, 'Edit ACL Detail', 'rbac/editACLItem', 6, '0'),
(33, 'View All ACL List', 'rbac/index', 6, '0'),
(34, 'Department Detail', 'departments/departmentDetail', 4, '0'),
(35, 'Add Designations Under Department', 'departments/addDesignations', 4, '0'),
(36, 'List All Designations', 'departments/listDesignations', 4, '1'),
(43, 'List  User Groups', 'userGroups/index', 4, '1'),
(44, 'Add User Group', 'userGroups/createGroup', 4, '1'),
(45, 'Edit User Group', 'userGroups/editGroup', 4, '0'),
(46, 'Delete User Group', 'userGroups/deleteUserGroup', 4, '0'),
(47, 'List All Users', 'users/index', 4, '1'),
(48, 'Add User Details', 'users/addUser', 4, '1'),
(49, 'Edit User Details', 'users/editUser', 4, '0'),
(50, 'Delete User Details', 'users/deleteUser', 4, '0'),
(53, 'Save Permissions', 'rbac/savePermissions', 6, '0'),
(54, 'Delete Designation', 'departments/deleteDesignation', 4, '0'),
(55, 'Edit Designation Details', 'departments/editDesignation', 4, '0'),
(65, 'Change Software Settings', 'settings/index', 15, '1'),
(66, 'Save Software Settings', 'settings/updateSettings', 15, '0'),
(67, 'Add  New Subscriber', 'clients/add', 10, '1'),
(68, 'Edit Subscriber Detail', 'clients/edit', 10, '0'),
(69, 'Renew Subscription of Subscriber', 'clients/renewSubscription', 10, '0'),
(70, 'View Subscriber Details', 'clients/detail', 10, '0'),
(71, 'Delete Subscriber Detail', 'clients/delete', 10, '0'),
(72, 'List All Subscribers', 'clients/index', 10, '1'),
(73, 'Add New Distributor', 'distributors/add', 14, '1'),
(74, 'Edit Distributor''s Detail', 'distributors/edit', 14, '0'),
(75, 'Delete Distributor''s Detail', 'distributors/delete', 14, '0'),
(76, 'List All Distributors', 'distributors/index', 14, '1'),
(77, 'Add New Classified Advertisement Record', 'advertisements/addClassifiedAdvertisement', 12, '1'),
(78, 'Add New General Advertisement Record', 'advertisements/addGeneralAdvertisement', 12, '1'),
(79, 'List All Classified Advertisements', 'advertisements/allClassifiedAdvertisements', 12, '1'),
(80, 'List All General Advertisements', 'advertisements/index', 12, '1'),
(81, 'Delete Classified Advertisement', 'advertisements/deleteClassified', 12, '0'),
(82, 'Delete General Advertisement', 'advertisement/deleteGeneral', 12, '0'),
(83, 'Add New Scheme', 'schemes/add', 13, '1'),
(84, 'List All Schemes', 'schemes/index', 13, '1'),
(85, 'Delete Schemes', 'schemes/delete', 13, '0'),
(86, 'View Scheme Details', 'schemes/detail', 13, '0'),
(87, 'Edit Scheme Details', 'schemes/edit', 13, '0'),
(88, 'List Clients For Renew ', 'payments/index', 16, '1'),
(89, 'View Invoice', 'payments/paymentView', 16, '0'),
(90, 'Add New Subscription Category', 'subscription_category/add', 17, '1'),
(91, 'List All Subscription Categories', 'subscription_category/index', 17, '1'),
(92, 'Edit Subscription Category Details', 'subscription_category/edit', 17, '0'),
(93, 'Delete Subscription Category', 'subscription_category/delete', 17, '0'),
(94, 'All Subscribers Report', 'reports/index', 3, '1'),
(95, 'All Renewable Subscribers Report', 'reports/renewableSubscribers', 3, '1'),
(96, 'Distributor Wise All Subscribers', 'reports/subscribersByDistributor', 3, '1'),
(97, 'Location Wise All Subscribers', 'reports/subscribersByLocation', 3, '1'),
(98, 'Location Wise Excel Report', 'reports/exportExcelByLocation', 3, '0'),
(99, 'Distributor Wise Excel Report', 'reports/exportExcelByDistributor', 3, '0'),
(100, 'Export Excel Reports', 'reports/exportExcel', 3, '0'),
(101, 'District And Vdc Management', 'settings/listDistricts', 15, '1'),
(102, 'List Advertisement Sponsors', 'ad_sponsors/index', 12, '1'),
(103, 'Add Advertisement Sponsor', 'ad_sponsors/add', 12, '1'),
(104, 'Edit Advertisement Sponsor', 'ad_sponsors/edit', 12, '0'),
(105, 'Delete Advertisement Sponsor', 'ad_sponsors/delete', 12, '0'),
(126, 'Add New Collector', 'collectors/add', 14, '1'),
(127, 'Edit Collector''s Detail', 'collectors/edit', 14, '0'),
(128, 'Delete Collector''s Detail', 'collectors/delete', 14, '0'),
(129, 'List All Collectors', 'collectors/index', 14, '1');

-- --------------------------------------------------------

--
-- Table structure for table `ad_pages_cost`
--

CREATE TABLE IF NOT EXISTS `ad_pages_cost` (
  `ad_pages_cost_id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsor_id` int(11) DEFAULT NULL,
  `advertisement_title` varchar(100) DEFAULT NULL,
  `page_no` int(11) DEFAULT NULL,
  `effective_from` date DEFAULT NULL,
  `effective_to` date DEFAULT NULL,
  `payable_amount` double(10,2) DEFAULT NULL,
  `payment_status` enum('1','0') DEFAULT NULL,
  PRIMARY KEY (`ad_pages_cost_id`),
  KEY `sponsor_id` (`sponsor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ad_sponsors`
--

CREATE TABLE IF NOT EXISTS `ad_sponsors` (
  `sponsor_id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsor_name` varchar(100) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `registered_date` date NOT NULL,
  `address` varchar(100) NOT NULL,
  `sponsor_vat` varchar(100) NOT NULL,
  PRIMARY KEY (`sponsor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `ad_sponsors`
--

INSERT INTO `ad_sponsors` (`sponsor_id`, `sponsor_name`, `contact`, `registered_date`, `address`, `sponsor_vat`) VALUES
(4, 'SUrya Nepal', '9y4567890090', '2016-03-10', 'Kathmandu', '567856856'),
(5, 'some', '9807656', '2016-06-17', 'kathmandu', 'iuyu'),
(6, 'CG Group', '01-6372864', '2016-09-12', 'Nayabazar, Pokhara', '93746284720'),
(7, 'kathmandu Mall', '014578556', '2016-10-19', 'kathmandu', '1248852');

-- --------------------------------------------------------

--
-- Table structure for table `classified_advertisements`
--

CREATE TABLE IF NOT EXISTS `classified_advertisements` (
  `c_advertisement_id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsor_name` varchar(100) DEFAULT NULL,
  `sponsor_address` varchar(100) NOT NULL,
  `advertisement_title` varchar(100) DEFAULT NULL,
  `effective_from` date DEFAULT NULL,
  `effective_to` date DEFAULT NULL,
  `payable_amount` double(10,2) DEFAULT NULL,
  `contact_number` varchar(20) NOT NULL,
  `advertisement_collector` mediumint(9) NOT NULL COMMENT 'reference of users user id',
  PRIMARY KEY (`c_advertisement_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `classified_advertisements`
--

INSERT INTO `classified_advertisements` (`c_advertisement_id`, `sponsor_name`, `sponsor_address`, `advertisement_title`, `effective_from`, `effective_to`, `payable_amount`, `contact_number`, `advertisement_collector`) VALUES
(1, 'dfhdfghdfg', '', 'ffsgfdg drs gd', '2016-03-18', '2016-03-16', 123.00, 'fghsgfsdgsfdg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_first_name` varchar(100) DEFAULT NULL,
  `client_last_name` varchar(100) DEFAULT NULL,
  `registered_date` date DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `gvs_id` int(11) DEFAULT NULL,
  `ward_no` int(11) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `distributor_id` int(11) DEFAULT NULL,
  `scheme_id` int(11) DEFAULT NULL,
  `scheme_claimed_date` date DEFAULT NULL,
  `map_pic` varchar(100) DEFAULT NULL,
  `collector_id` int(11) NOT NULL,
  PRIMARY KEY (`client_id`),
  KEY `distributor_id` (`distributor_id`),
  KEY `scheme_id` (`scheme_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`client_id`, `client_first_name`, `client_last_name`, `registered_date`, `phone`, `district_id`, `gvs_id`, `ward_no`, `address`, `distributor_id`, `scheme_id`, `scheme_claimed_date`, `map_pic`, `collector_id`) VALUES
(49, 'Pharsha', 'Thapa', '2016-08-16', '9860168995', 6, 21, 5, 'Baglung Bazaar', 1, NULL, NULL, '8913d4c25204faa098fb10a44e1cc01a.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `collectors`
--

CREATE TABLE IF NOT EXISTS `collectors` (
  `collector_id` int(11) NOT NULL AUTO_INCREMENT,
  `collector_name` varchar(50) NOT NULL,
  `registered_date` date NOT NULL,
  `collector_phone` varchar(20) NOT NULL,
  `district` varchar(10) NOT NULL,
  `gvs` varchar(10) NOT NULL,
  `ward_no` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  PRIMARY KEY (`collector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `collectors`
--

INSERT INTO `collectors` (`collector_id`, `collector_name`, `registered_date`, `collector_phone`, `district`, `gvs`, `ward_no`, `address`) VALUES
(2, 'abcd', '2016-07-08', '111111111112', '2', '4', 1, 'abcd');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_short_name` varchar(32) NOT NULL,
  `department_full_name` varchar(32) NOT NULL,
  `department_description` text NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`department_id`, `department_short_name`, `department_full_name`, `department_description`) VALUES
(10, 'FNC', 'Finance', 'asdfks kasdfhkadsfsd');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE IF NOT EXISTS `designations` (
  `designation_id` int(11) NOT NULL AUTO_INCREMENT,
  `designation_full_name` varchar(20) NOT NULL,
  `designation_short_name` varchar(10) NOT NULL,
  `designation_status` enum('1','0') NOT NULL,
  `department_id` int(11) NOT NULL,
  PRIMARY KEY (`designation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`designation_id`, `designation_full_name`, `designation_short_name`, `designation_status`, `department_id`) VALUES
(5, 'Finance Manager', 'FMGR', '1', 10);

-- --------------------------------------------------------

--
-- Table structure for table `distributors`
--

CREATE TABLE IF NOT EXISTS `distributors` (
  `distributor_id` int(11) NOT NULL AUTO_INCREMENT,
  `distributor_name` varchar(50) NOT NULL,
  `registered_date` date NOT NULL,
  `distributor_phone` varchar(20) NOT NULL,
  `district` varchar(20) NOT NULL,
  `gvs` varchar(10) NOT NULL,
  `ward_no` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  PRIMARY KEY (`distributor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `distributors`
--

INSERT INTO `distributors` (`distributor_id`, `distributor_name`, `registered_date`, `distributor_phone`, `district`, `gvs`, `ward_no`, `address`) VALUES
(1, 'Distributor 2', '2016-03-14', '98765456789', '2', '2', 1, 'Gorkha');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE IF NOT EXISTS `districts` (
  `district_id` int(11) NOT NULL AUTO_INCREMENT,
  `district_name` varchar(20) NOT NULL,
  PRIMARY KEY (`district_id`),
  UNIQUE KEY `district_name` (`district_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`district_id`, `district_name`) VALUES
(6, 'Baglung'),
(2, 'Gorkha'),
(5, 'Gulmi'),
(4, 'Kaski'),
(3, 'Mustang');

-- --------------------------------------------------------

--
-- Table structure for table `general_advertisements`
--

CREATE TABLE IF NOT EXISTS `general_advertisements` (
  `g_advertisement_id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsor_id` int(11) DEFAULT NULL,
  `advertisement_title` varchar(100) DEFAULT NULL,
  `page` int(11) DEFAULT NULL,
  `payable_amount` double(10,2) DEFAULT NULL,
  `effective_payable_amount` int(11) NOT NULL,
  `size_length` int(11) DEFAULT NULL,
  `size_breadth` int(11) DEFAULT NULL,
  `effective_dates` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`g_advertisement_id`),
  KEY `sponsor_id` (`sponsor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `general_advertisements`
--

INSERT INTO `general_advertisements` (`g_advertisement_id`, `sponsor_id`, `advertisement_title`, `page`, `payable_amount`, `effective_payable_amount`, `size_length`, `size_breadth`, `effective_dates`) VALUES
(1, 6, 'Surya Golf Tournament', 1, 2000.00, 2000, 10, 2, '2016-09-15');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'Super Users', 'Super Users');

-- --------------------------------------------------------

--
-- Table structure for table `gvs`
--

CREATE TABLE IF NOT EXISTS `gvs` (
  `gvs_id` int(11) NOT NULL AUTO_INCREMENT,
  `gvs_name` varchar(20) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`gvs_id`),
  KEY `district_id` (`district_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `gvs`
--

INSERT INTO `gvs` (`gvs_id`, `gvs_name`, `district_id`) VALUES
(17, 'VDC 1', 3),
(18, 'VDC 2', 3),
(19, 'VDC 3', 3),
(20, 'Baglung VDC One', 6),
(21, 'Baglung VDC Two', 6);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
  `invoice_id` varchar(1000) NOT NULL,
  `payment_log_id` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `vat` varchar(100) NOT NULL,
  `customer_address` varchar(255) NOT NULL,
  `grand_total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_id`, `payment_log_id`, `created_date`, `created_by`, `customer_name`, `vat`, `customer_address`, `grand_total`) VALUES
('SUB1T1D20160304', 1, '2016-03-04', 1, 'Pharsa Thapa', '', 'Address Kathmandu', 13153),
('SUB2T2D20160304', 2, '2016-03-04', 1, 'Pharsa Thapa', '', 'Address Kathmandu', 13153),
('CAD0T3D20160305', 3, '2016-03-05', 2, 'dfhdfghdfg', '', '', 123),
('SUB1T4D20160704', 4, '2016-07-04', 2, 'Pharsa Thapa', '', 'Address Kathmandu', 13560),
('SUB1T5D20160705', 5, '2016-07-05', 2, 'Client firstname last name', '', 'address line', 12204),
('SUB1T6D20160705', 6, '2016-07-05', 2, 'asdfasdf asdfadsfasd', '', 'address line', 13424),
('SUB1T7D20160710', 7, '2016-07-10', 2, 'adsf asdf asdfasdf', '', 'asdf asdf asdf sd', 13560),
('SUB1T8D20160710', 8, '2016-07-10', 2, 'Suresh Thapa', '', 'Baarpaak', 13560),
('SUB1T9D20160710', 9, '2016-07-10', 2, 'asdfasdf adsfasdfasd', '', 'adf adsf asf asd', 12000),
('SUB1T10D20160710', 10, '2016-07-10', 2, 'Bhusan Pokharel', '', 'dfads fadsf asf', 12000),
('SUB1T11D20160710', 11, '2016-07-10', 2, 'dasfasdf asdf asdfas', '', 'adfadsfadsf adsf ads', 12000),
('SUB1T12D20160710', 12, '2016-07-10', 2, 'dasfasdf asdf asdfas', '', 'adfadsfadsf adsf ads', 12000),
('SUB1T13D20160710', 13, '2016-07-10', 2, 'dasfasdf asdf asdfas', '', 'adfadsfadsf adsf ads', 12000),
('SUB1T14D20160710', 14, '2016-07-10', 2, 'adfadsfasd asdfasdfads', '', 'adsfadsf asdf asd', 12000),
('SUB1T15D20160710', 15, '2016-07-10', 2, 'adfadsfasd asdfasdfads', '', 'adsfadsf asdf asd', 12000),
('SUB1T16D20160710', 16, '2016-07-10', 2, 'adfadsfasd asdfasdfads', '', 'adsfadsf asdf asd', 12000),
('SUB1T17D20160710', 17, '2016-07-10', 2, 'Someone someon', '', '34dsfdsfasdf', 12000),
('SUB1T18D20160710', 18, '2016-07-10', 2, 'Randi Ko Baan', '', 'ads fadsf', 12000),
('SUB1T19D20160710', 19, '2016-07-10', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB2T20D20160710', 20, '2016-07-10', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB1T21D20160712', 21, '2016-07-12', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB2T22D20160712', 22, '2016-07-12', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB3T23D20160712', 23, '2016-07-12', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB4T24D20160712', 24, '2016-07-12', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB5T25D20160712', 25, '2016-07-12', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB6T26D20160712', 26, '2016-07-12', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB1T27D20160714', 27, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB2T28D20160714', 28, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB3T29D20160714', 29, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB4T30D20160714', 30, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB5T31D20160714', 31, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB6T32D20160714', 32, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB7T33D20160714', 33, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB8T34D20160714', 34, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB9T35D20160714', 35, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB10T36D20160714', 36, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB11T37D20160714', 37, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB1T38D20160714', 38, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB2T39D20160714', 39, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB3T40D20160714', 40, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB1T41D20160714', 41, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB2T42D20160714', 42, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB1T43D20160714', 43, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB1T44D20160714', 44, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB2T45D20160714', 45, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB3T46D20160714', 46, '2016-07-14', 2, 'Pharsa Thapa', '', 'adfsasdf asdf sdf', 12000),
('SUB1T47D20160714', 47, '2016-07-14', 2, 'Pharsa Thapa', '', 'maitiiasf', 12000),
('SUB1T48D20160714', 48, '2016-07-14', 2, 'Golchha ', '', 'mainroad', 12000),
('GAD1T1D20160914', 49, '2016-09-14', 2, 'CG Group', '93746284720', 'Nayabazar, Pokhara', 2000);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE IF NOT EXISTS `invoice_details` (
  `invoice_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(255) NOT NULL,
  `particular` varchar(255) NOT NULL,
  `particular_type` enum('1','2') NOT NULL COMMENT '1 for main and 2 for sub particuular',
  `percent` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`invoice_detail_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `invoice_details`
--

INSERT INTO `invoice_details` (`invoice_detail_id`, `invoice_id`, `particular`, `particular_type`, `percent`, `amount`) VALUES
(1, 'SUB1T1D20160304', 'Subscription For 3 Months (Regular)', '1', 0, 13153),
(2, 'SUB1T1D20160304', 'VAT', '2', 13, 0),
(3, 'SUB1T1D20160304', 'Discount', '2', 3, 0),
(4, 'SUB2T2D20160304', 'Subscription For 3 Months (Regular)', '1', 0, 13153),
(5, 'SUB2T2D20160304', 'VAT', '2', 13, 0),
(6, 'SUB2T2D20160304', 'Discount', '2', 3, 0),
(7, 'CAD0T3D20160305', 'Classified Advertisement ', '1', 0, 123),
(8, 'SUB1T4D20160704', 'Subscription For 3 Months (Regular)', '1', 0, 13560),
(9, 'SUB1T4D20160704', 'VAT', '2', 13, 0),
(10, 'SUB1T5D20160705', 'Subscription For 3 Months (Regular)', '1', 0, 12204),
(11, 'SUB1T5D20160705', 'VAT', '2', 13, 0),
(12, 'SUB1T5D20160705', 'Discount', '2', 10, 0),
(13, 'SUB1T6D20160705', 'Subscription For 3 Months (Regular)', '1', 0, 13424),
(14, 'SUB1T6D20160705', 'VAT', '2', 13, 0),
(15, 'SUB1T6D20160705', 'Discount', '2', 1, 0),
(16, 'SUB1T7D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 13560),
(17, 'SUB1T7D20160710', 'VAT', '2', 13, 0),
(18, 'SUB1T8D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 13560),
(19, 'SUB1T8D20160710', 'VAT', '2', 13, 0),
(20, 'SUB1T9D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(21, 'SUB1T10D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(22, 'SUB1T11D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(23, 'SUB1T12D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(24, 'SUB1T13D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(25, 'SUB1T14D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(26, 'SUB1T15D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(27, 'SUB1T16D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(28, 'SUB1T17D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(29, 'SUB1T18D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(30, 'SUB1T19D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(31, 'SUB2T20D20160710', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(32, 'SUB1T21D20160712', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(33, 'SUB2T22D20160712', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(34, 'SUB3T23D20160712', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(35, 'SUB4T24D20160712', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(36, 'SUB5T25D20160712', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(37, 'SUB6T26D20160712', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(38, 'SUB1T27D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(39, 'SUB2T28D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(40, 'SUB3T29D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(41, 'SUB4T30D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(42, 'SUB5T31D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(43, 'SUB6T32D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(44, 'SUB7T33D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(45, 'SUB8T34D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(46, 'SUB9T35D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(47, 'SUB10T36D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(48, 'SUB11T37D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(49, 'SUB1T38D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(50, 'SUB2T39D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(51, 'SUB3T40D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(52, 'SUB1T41D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(53, 'SUB2T42D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(54, 'SUB1T43D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(55, 'SUB1T44D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(56, 'SUB2T45D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(57, 'SUB3T46D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(58, 'SUB1T47D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(59, 'SUB1T48D20160714', 'Subscription For 3 Months (Regular)', '1', 0, 12000),
(60, 'GAD1T1D20160914', 'General Advertisement In Front Page [10 X 2]', '1', 0, 2000);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_group_head`
--

CREATE TABLE IF NOT EXISTS `menu_group_head` (
  `menu_group_head_id` int(11) NOT NULL AUTO_INCREMENT,
  `default_menu_head` varchar(50) NOT NULL,
  `menu_head` varchar(50) NOT NULL,
  `menu_head_description` text NOT NULL,
  PRIMARY KEY (`menu_group_head_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `menu_group_head`
--

INSERT INTO `menu_group_head` (`menu_group_head_id`, `default_menu_head`, `menu_head`, `menu_head_description`) VALUES
(3, 'Reports', 'Reports', 'All the reports are enlisted under this head'),
(4, 'User Management', 'User Management', 'User Management menus are enlisted under this group menu'),
(6, 'RBAC', 'Role Based Access Control', 'This section includes the menus to manage user groups'),
(10, 'Subscribers', 'Suscribers', 'this snsdajf asdf adsnfds asdfas &lt;br&gt;'),
(12, 'Advertisements', 'Advertisements', 'this sd sd'),
(13, 'Schemes', 'Schemes', '&lt;p&gt;Rhiasfa adsfbdasf&lt;/p&gt;'),
(14, 'Distributors And Collectors', 'Distributors And Collectors', 'a skjasdf&amp;nbsp;'),
(15, 'Software Settings', 'Software Settings', '&lt;p&gt;Software setting menu are under this menu head&lt;/p&gt;'),
(16, 'Payments', 'Payments', 'Pay,ents&amp;nbsp;'),
(17, 'Subscription Categories', 'Subscription Categories', 'Create Update and Delete Subscription categories');

-- --------------------------------------------------------

--
-- Table structure for table `payment_logs`
--

CREATE TABLE IF NOT EXISTS `payment_logs` (
  `payment_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `received_by` int(11) NOT NULL,
  PRIMARY KEY (`payment_log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `payment_logs`
--

INSERT INTO `payment_logs` (`payment_log_id`, `transaction_id`, `amount`, `payment_date`, `received_by`) VALUES
(1, 1, 13153, '2016-03-04', 1),
(2, 2, 13153, '2016-03-04', 1),
(3, 3, 123, '2016-03-05', 2),
(4, 4, 13560, '2016-07-04', 2),
(5, 5, 12204, '2016-07-05', 2),
(6, 6, 13424, '2016-07-05', 2),
(7, 7, 13560, '2016-07-10', 2),
(8, 8, 13560, '2016-07-10', 2),
(9, 9, 12000, '2016-07-10', 2),
(10, 10, 12000, '2016-07-10', 2),
(11, 11, 12000, '2016-07-10', 2),
(12, 12, 12000, '2016-07-10', 2),
(13, 13, 12000, '2016-07-10', 2),
(14, 14, 12000, '2016-07-10', 2),
(15, 15, 12000, '2016-07-10', 2),
(16, 16, 12000, '2016-07-10', 2),
(17, 17, 12000, '2016-07-10', 2),
(18, 18, 12000, '2016-07-10', 2),
(19, 19, 12000, '2016-07-10', 2),
(20, 20, 12000, '2016-07-10', 2),
(21, 21, 12000, '2016-07-12', 2),
(22, 22, 12000, '2016-07-12', 2),
(23, 23, 12000, '2016-07-12', 2),
(24, 24, 12000, '2016-07-12', 2),
(25, 25, 12000, '2016-07-12', 2),
(26, 26, 12000, '2016-07-12', 2),
(27, 27, 12000, '2016-07-14', 2),
(28, 28, 12000, '2016-07-14', 2),
(29, 29, 12000, '2016-07-14', 2),
(30, 30, 12000, '2016-07-14', 2),
(31, 31, 12000, '2016-07-14', 2),
(32, 32, 12000, '2016-07-14', 2),
(33, 33, 12000, '2016-07-14', 2),
(34, 34, 12000, '2016-07-14', 2),
(35, 35, 12000, '2016-07-14', 2),
(36, 36, 12000, '2016-07-14', 2),
(37, 37, 12000, '2016-07-14', 2),
(38, 38, 12000, '2016-07-14', 2),
(39, 39, 12000, '2016-07-14', 2),
(40, 40, 12000, '2016-07-14', 2),
(41, 41, 12000, '2016-07-14', 2),
(42, 42, 12000, '2016-07-14', 2),
(43, 43, 12000, '2016-07-14', 2),
(44, 44, 12000, '2016-07-14', 2),
(45, 45, 12000, '2016-07-14', 2),
(46, 46, 12000, '2016-07-14', 2),
(47, 47, 12000, '2016-07-14', 2),
(48, 48, 12000, '2016-07-14', 2),
(49, 1, 2000, '2016-09-14', 2);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `acl_id` int(11) NOT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`permission_id`, `group_id`, `acl_id`) VALUES
(2, 2, 7),
(3, 2, 8),
(4, 2, 34),
(5, 2, 9),
(6, 2, 35),
(7, 2, 36),
(8, 2, 43),
(9, 2, 44),
(10, 2, 45),
(11, 2, 46),
(12, 2, 47),
(13, 2, 49),
(14, 2, 48),
(15, 2, 50),
(16, 2, 54),
(17, 2, 55),
(18, 2, 21),
(19, 2, 53),
(20, 2, 65),
(21, 2, 66),
(22, 2, 68),
(23, 2, 67),
(24, 2, 69),
(25, 2, 70),
(26, 2, 71),
(28, 2, 72),
(33, 2, 73),
(34, 2, 74),
(35, 2, 75),
(36, 2, 76),
(37, 2, 77),
(40, 2, 78),
(41, 2, 79),
(42, 2, 80),
(43, 2, 81),
(44, 2, 82),
(45, 2, 83),
(46, 2, 84),
(47, 2, 85),
(48, 2, 86),
(49, 2, 87),
(50, 2, 88),
(51, 2, 89),
(52, 2, 90),
(53, 2, 91),
(54, 2, 93),
(55, 2, 92),
(56, 2, 94),
(57, 2, 95),
(58, 2, 96),
(60, 2, 99),
(61, 2, 100),
(62, 2, 98),
(63, 2, 97),
(64, 2, 6),
(65, 2, 101),
(66, 2, 102),
(67, 2, 103),
(68, 2, 104),
(69, 2, 105);

-- --------------------------------------------------------

--
-- Table structure for table `schemes`
--

CREATE TABLE IF NOT EXISTS `schemes` (
  `scheme_id` int(11) NOT NULL AUTO_INCREMENT,
  `scheme_name` varchar(20) NOT NULL,
  `effective_from` date NOT NULL,
  `effective_to` date NOT NULL,
  `description` text NOT NULL,
  `scheme_status` tinyint(1) NOT NULL DEFAULT '1',
  `discount` double NOT NULL,
  `other` varchar(100) NOT NULL,
  PRIMARY KEY (`scheme_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `schemes`
--

INSERT INTO `schemes` (`scheme_id`, `scheme_name`, `effective_from`, `effective_to`, `description`, `scheme_status`, `discount`, `other`) VALUES
(1, 'Scheme Test', '2016-03-10', '2016-03-26', 'dfhjdasf jdashfidas', 1, 3, '');

-- --------------------------------------------------------

--
-- Table structure for table `scheme_clients`
--

CREATE TABLE IF NOT EXISTS `scheme_clients` (
  `scheme_client_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `scheme_id` int(11) NOT NULL,
  `scheme_claimed_date` date NOT NULL,
  `effective_from` date NOT NULL,
  PRIMARY KEY (`scheme_client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `scheme_clients`
--

INSERT INTO `scheme_clients` (`scheme_client_id`, `client_id`, `scheme_id`, `scheme_claimed_date`, `effective_from`) VALUES
(1, 27, 1, '2016-03-04', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `settings_id` int(11) NOT NULL AUTO_INCREMENT,
  `settings_name` varchar(150) DEFAULT NULL,
  `settings` varchar(150) DEFAULT NULL,
  `settings_value` varchar(150) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `description` varchar(100) NOT NULL,
  `module` varchar(100) NOT NULL,
  `setting_default` varchar(255) NOT NULL,
  `settings_status` tinyint(1) DEFAULT '1',
  `settings_order` int(2) NOT NULL,
  PRIMARY KEY (`settings_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`settings_id`, `settings_name`, `settings`, `settings_value`, `type`, `description`, `module`, `setting_default`, `settings_status`, `settings_order`) VALUES
(12, 'application_name', 'Application Name', 'Samadhan Information Management System', 'text', 'Title of the web page', 'meta_data', '', 0, 0),
(13, 'company_name', 'Company Name', 'Samadhan National Daily', 'text', 'Company Name displayed at the footer copyright section', 'meta_data', '', 0, 1),
(17, 'application_logo', 'Application Logo Text', 'Samadhan', 'text', 'Logo Text of the Application', 'meta_data', '', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE IF NOT EXISTS `subscriptions` (
  `subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_from` date DEFAULT NULL,
  `subscription_to` date DEFAULT NULL,
  `subscription_status` enum('1','0') DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `bill_number` varchar(100) NOT NULL,
  `receipt_number` varchar(100) NOT NULL,
  `subscription_category_id` int(11) NOT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`subscription_id`, `subscription_from`, `subscription_to`, `subscription_status`, `client_id`, `bill_number`, `receipt_number`, `subscription_category_id`) VALUES
(42, '2017-04-14', '2017-07-14', '1', NULL, '11111', '22222', 1),
(43, '2016-08-16', '2016-11-16', '1', 49, '2125', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subscription_categories`
--

CREATE TABLE IF NOT EXISTS `subscription_categories` (
  `subscription_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_category_label` varchar(100) NOT NULL,
  `subscription_category_span` int(11) NOT NULL COMMENT 'explicitly in months',
  `subscription_category_rate` double NOT NULL,
  `subscription_category_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`subscription_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `subscription_categories`
--

INSERT INTO `subscription_categories` (`subscription_category_id`, `subscription_category_label`, `subscription_category_span`, `subscription_category_rate`, `subscription_category_status`) VALUES
(1, 'Subscription For 3 Months (Regular)', 3, 12000, 1),
(2, '1 year', 12, 15000, 1),
(3, '3 years program', 36, 25000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `payable_amount` int(11) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `reference_type` enum('1','2','3') NOT NULL COMMENT '1 for subscription, 2 for general advertiisement and 3 for classified advertisements',
  `vat` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `effective_payable_amount` int(11) NOT NULL,
  `remaining_amount` int(11) NOT NULL,
  `transaction_date` date NOT NULL,
  `client_sponsor_id` int(11) NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`transaction_id`, `payable_amount`, `reference_id`, `reference_type`, `vat`, `discount`, `effective_payable_amount`, `remaining_amount`, `transaction_date`, `client_sponsor_id`) VALUES
(1, 2000, 1, '2', 0, 0, 2000, 0, '2016-09-14', 6);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `salary` float NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `last_login_ip` varchar(16) NOT NULL,
  `department_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `next_to_keen` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `salary`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `last_login_ip`, `department_id`, `designation_id`, `address_1`, `address_2`, `dob`, `next_to_keen`) VALUES
(1, 0, '127.0.0.1', 'administrator', '$2y$08$I/x/RbDb4UYIq1WiL88nsOt.SZJ.pBN.o.tZpeOyhgFXj/oKSX6Mq', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 2016, 1, 'Admin', 'istrator', 'ADMIN', '0', '::1', 0, 0, '', '', '0000-00-00', ''),
(2, 999999, '::1', 'bhusan', '$2y$08$7IG3UyyK07QxMCLf0bd3vuNk4qimbTHreF6NLUvHt3Xtm3zYFVcDC', NULL, 'bhusan@bhusan.com', '2e2c0e1c8152ddfa7a5f3cca4da261a41748c285', NULL, NULL, NULL, 2016, 2016, 1, 'Bhusan', 'Pokharel', NULL, '9807564456', '::1', 10, 5, 'Maitedevi', 'Maitidevi', '2016-02-25', 'Pharsa'),
(5, 10000, '', 'abc@abc.com', '$2y$08$FiGtx5qwAATUK/UtOEQAWO302Zfnh2bNOHzP5OV14eKNja.fx.Qli', NULL, 'abc@abc.com', NULL, NULL, NULL, NULL, 2016, 2016, 1, 'askdfhasjk', 'whaskhfkdasfk', NULL, '987656789', '::1', 10, 5, 'kdadfhkjads', 'dakfhadskfh', '2015-10-14', 'Someone');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(6, 5, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ad_pages_cost`
--
ALTER TABLE `ad_pages_cost`
  ADD CONSTRAINT `ad_pages_cost_ibfk_1` FOREIGN KEY (`sponsor_id`) REFERENCES `ad_sponsors` (`sponsor_id`) ON DELETE SET NULL;

--
-- Constraints for table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`distributor_id`) REFERENCES `distributors` (`distributor_id`) ON DELETE SET NULL;

--
-- Constraints for table `general_advertisements`
--
ALTER TABLE `general_advertisements`
  ADD CONSTRAINT `g_ad_ibfk_1` FOREIGN KEY (`sponsor_id`) REFERENCES `ad_sponsors` (`sponsor_id`) ON DELETE SET NULL;

--
-- Constraints for table `gvs`
--
ALTER TABLE `gvs`
  ADD CONSTRAINT `gvs_ibfk_1` FOREIGN KEY (`district_id`) REFERENCES `districts` (`district_id`) ON DELETE CASCADE;

--
-- Constraints for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD CONSTRAINT `subscriptions_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`client_id`) ON DELETE SET NULL;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
