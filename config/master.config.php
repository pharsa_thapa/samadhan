<?php

if (!defined('INC_SOURCE')) {
   die('Ooops..Direct access not allowed!');
}
if (!defined('BASEPATH')) {
   die('Ooops.. Direct access not allowed!');
}
// for application/config/config.php for both admin and frontend section

$servers = array(
   'localserver' => array('localhost'),
   'testingserver' => array('192.168.1.119'),
   'liveserver' => array('http://92661bab.ngrok.io'),
);
if (in_array($_SERVER['SERVER_NAME'], $servers['localserver'])) {
   ## localserver config
   ## admin configuration settings
   define('ADMIN_BASE_URL', 'http://localhost/samadhan/samadhan');
   ## frontend configuration settings
   define('FRONT_BASE_URL', 'http://localhost/samadhan/samadhan');
   ## user configuration settings
//    define('USER_BASE_URL', 'http://localhost/id-suspect/user');

   ## root path
   define('ROOT_PATH', __DIR__ . '/../');

   ## db settings
   define('DBHOST', 'localhost');
   define('DBUSER', 'root');
   define('DBPASS', '');
   define('DBNAME', 'db_samadhan');
//    die("hjfygygu");
} else if (in_array($_SERVER['SERVER_NAME'], $servers['testingserver']) ) {
   ## staging server config
   ## admin configuration settings
   define('ADMIN_BASE_URL', 'http://192.168.1.119/metro-payroll');
   ## frontend configuraiton settings
   define('FRONT_BASE_URL', 'http://192.168.1.119/metro-payroll');
   ## frontend configuraiton settings
//    define('USER_BASE_URL', 'http://metro-vibes.com/metro-payroll');

   if (!defined('ROOT_PATH'))
       define('ROOT_PATH', './');

   ## db settings
   define('DBHOST', 'localhost');
   define('DBUSER', 'root');
   define('DBPASS', '');
   define('DBNAME', 'db_camstreet');
}
else {
   ## for production server
   ## admin configuration settings
   ## frontend configuraiton settings

   define('ADMIN_BASE_URL', 'http://92661bab.ngrok.io');
   ## frontend configuration settings
   define('FRONT_BASE_URL', 'http://92661bab.ngrok.io');

   ## db settings

    define('DBHOST', 'localhost');
   define('DBUSER', 'root');
   define('DBPASS', '');
   define('DBNAME', 'db_samadhan');
}

## application specific settings
define('APP_PFIX', 'MAPP_'); // application prefix

define('SITE_TITLE', 'Metro Payroll');